package by.epam.bookrating.entity;
import java.sql.Date;

/**
 * Class-bean representing comment in the system<br/>
 * @author Anna Buklis
 */
public class Comment {
    private long commentId;
    private long bookId;
    private long userId;
    private Date commentDate;
    private String commentText;

    public Comment() {
        super();
    }

    public Comment(long commentId, long bookId, long userId, Date commentDate, String commentText) {
        this.commentId = commentId;
        this.bookId = bookId;
        this.userId = userId;
        this.commentDate = commentDate;
        this.commentText = commentText;
    }

    public Comment(long bookId, long userId, Date commentDate, String commentText) {
        this.bookId = bookId;
        this.userId = userId;
        this.commentDate = commentDate;
        this.commentText = commentText;
    }


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "bookId=" + bookId +
                ", commentId=" + commentId +
                ", commentDate=" + commentDate +
                ", commentText='" + commentText + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment = (Comment) o;

        return commentId == comment.commentId
                && bookId == comment.bookId && userId == comment.userId;

    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + (int) (bookId ^ (bookId >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        return result;
    }
}
