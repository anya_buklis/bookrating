package by.epam.bookrating.entity;

/**
 * Class-bean representing user-statistics in the system<br/>
 * @author Anna Buklis
 */
public class Statistics {
    private long userId;
    private String name;
    private String login;
    private int age;
    private String role;
    private int commentsAmount;
    private int ratingsAmount;

    public Statistics() {
    }

    public Statistics(long userId, String name, String login, int age,
                      String role, int commentsAmount, int ratingsAmount) {
        this.ratingsAmount = ratingsAmount;
        this.commentsAmount = commentsAmount;
        this.role = role;
        this.age = age;
        this.login = login;
        this.name = name;
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getCommentsAmount() {
        return commentsAmount;
    }

    public void setCommentsAmount(int commentsAmount) {
        this.commentsAmount = commentsAmount;
    }

    public int getRatingsAmount() {
        return ratingsAmount;
    }

    public void setRatingsAmount(int ratingsAmount) {
        this.ratingsAmount = ratingsAmount;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", age=" + age +
                ", role='" + role + '\'' +
                ", commentsAmount=" + commentsAmount +
                ", ratingsAmount=" + ratingsAmount +
                '}';
    }
}
