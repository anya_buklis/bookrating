package by.epam.bookrating.exception;

/**
 * Exception-wrapper of all exceptions thrown in command-layer<br/>
 * Extends {@link Exception}
 * @author Anna Buklis
 */
public class CommandException extends Exception {
    public CommandException() {
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

}
