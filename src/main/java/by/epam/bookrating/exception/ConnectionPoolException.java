package by.epam.bookrating.exception;

/**
 * Exception-wrapper of all exceptions thrown in {@link by.epam.bookrating.pool.ConnectionPool}<br/>
 * Extends {@link Exception}
 * @author Anna Buklis
 */
public class ConnectionPoolException extends Exception {
    public ConnectionPoolException() {
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
