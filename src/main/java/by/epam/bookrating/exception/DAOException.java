package by.epam.bookrating.exception;

/**
 * Exception-wrapper of all exceptions thrown in dao-layer<br/>
 * Extends {@link Exception}
 * @author Anna Buklis
 */
public class DAOException extends Exception {
    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

}
