package by.epam.bookrating.exception;

/**
 * Exception-wrapper of all exceptions thrown in service-layer<br/>
 * Extends {@link Exception}
 * @author Anna Buklis
 */
public class ServiceException extends Exception {
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
