package by.epam.bookrating.service;

import by.epam.bookrating.dao.AuthorDAO;
import by.epam.bookrating.dao.impl.MySQLAuthorDAO;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.util.ImageLogic;
import org.apache.log4j.Logger;

import javax.servlet.http.Part;
import java.util.List;

/**
 * <p> Class contains methods for for processing data
 * received from controller layer, concerning with authors. </p>
 *  @author Anna Buklis
 */
public class AuthorService {
    private static final String IMG_DIRECTORY_PATH = "img/";
    private static final int AUTHOR_NOT_FOUND = 0;
    private static final AuthorDAO DAO = new MySQLAuthorDAO();
    private static Logger logger = Logger.getLogger(AuthorService.class);

    /**
     * Checks if author with such name exists. If no,
     * creates author instance filled with method parameters
     * and send in to the dao layer and returns true.
     * If such author exists, return false
     * @param fullName full name of an author
     * @param birthYear birth year of an author
     * @param birthCountry birth country of an author
     * @param biography of an author
     * @param imagePart representing image
     * @param absoluteDiskPath path for uploading image
     * @return true, if author new and added, false if author exists
     * @throws ServiceException
     */
    public static boolean addAuthor(String fullName, int birthYear, String birthCountry, String biography,
                             Part imagePart, String absoluteDiskPath) throws ServiceException {
        try {
            logger.info("Full name = " + fullName + ", birth year " + birthYear + ", birth country " +
                    birthCountry + "\n biography = " + biography);

            logger.info("Checking if author with such name already exists...");
            if (findAuthorIdByFullName(fullName) == AUTHOR_NOT_FOUND){
                logger.info("Author not found. Adding...");
                String imageUrl = ImageLogic.addImage(imagePart, absoluteDiskPath);
                Author author = new Author(fullName, birthYear, birthCountry, biography, imageUrl);
                DAO.addAuthor(author);
                return true;
            } else {
                logger.info("Author with such name already exists.");
                return false;
            }
        } catch (DAOException e) {
            throw new ServiceException("Exception occurred in addAuthor method.", e);
        }
    }

    /**
     * Adds author's in db only with full name
     * @param fullName full name of an author
     * @return id generated after adding author in db
     * @throws ServiceException
     */
    public static long addBasicAuthorInfo(String fullName) throws ServiceException {
        long id = 0;
        try {
            logger.info("Adding author only with full name = " + fullName);
            id =  DAO.addBasicAuthorInfo(fullName);
            logger.info("Generated id = " + id);
        } catch (DAOException e) {
            throw new ServiceException("Exception occurred in addBasicAuthorInfo method.", e);
        }
        return id;
    }

    /**
     * Finds author by id and sends received from db object to a comand.
     * @param  authorId id of author in the db
     * @throws ServiceException
     * @return Appropriate {@link by.epam.bookrating.entity.Author} instance
     */
    public static Author findAuthorById(long authorId) throws ServiceException {
        Author currentAuthor;
        try{
            logger.info("AuthorId = " + authorId);
            currentAuthor = DAO.findAuthorById(authorId);
            logger.info("Found author: "+ currentAuthor );
        } catch (DAOException e){
            throw new ServiceException("Exception occurred in this method.", e);
        }
        return currentAuthor;
    }

    public static int findAmountOfBooksByAuthor(long authorId) throws ServiceException{
        int result;
        try{
            result = DAO.findAmountOfBooksByAuthor(authorId);
            logger.info(result + " books found by author with id " + authorId);
        } catch (DAOException e){
            throw new ServiceException("Exception occurred in this method.", e);
        }
        return result;
    }

    /**
     * Calculates number of pages for pagination tag
     * @param totalAmount amount of authors
     * @param recordsPerPage amount of authors which are showed on the single page
     * @return number of pages
     */
    public static int calculatePagesAmount(int totalAmount, int recordsPerPage){
        return (int) Math.ceil(totalAmount * 1.0 / recordsPerPage);
    }

    /**
     * Edits author in the db with the method's parameters
     * @param authorId id of author which should be updated
     * @param fullName full name of an author
     * @param birthYear birth year of an author
     * @param birthCountry birth country of an author
     * @param biography of an author
     * @param imgPart representing image
     * @param absoluteDiskPath path for uploading image
     * @return true, if author updated, false if author with such name exists
     * @throws ServiceException
     */

    public static boolean editAuthor(long authorId, String fullName,String oldName, int birthYear, String birthCountry, String biography,
                           String oldImgUrl, Part imgPart, String absoluteDiskPath) throws ServiceException {
        try {
            if (oldName.equalsIgnoreCase(fullName) ||
                    (!oldName.equalsIgnoreCase(fullName) && findAuthorIdByFullName(fullName) == AUTHOR_NOT_FOUND)){
                logger.info("Full name = " + fullName + ", birth year " + birthYear + ", birth country " +
                        birthCountry + "\n biography = " + biography + "\n oldImageUrl " + oldImgUrl);
                String fileName = ImageLogic.getFileName(imgPart);
                String realImageUrl = IMG_DIRECTORY_PATH + fileName;
                String imageUrl;
                if (!realImageUrl.equals(IMG_DIRECTORY_PATH)) {
                    imageUrl = ImageLogic.addImage(imgPart, absoluteDiskPath);
                } else {
                    imageUrl = oldImgUrl;
                }
                logger.info("New image url " + imageUrl);
                logger.info("Trying to update author with id = " + authorId);
                Author author = new Author(authorId, fullName, birthYear, birthCountry, biography, imageUrl);
                DAO.updateAuthorInfo(author);
                return true;
            }  else {
                return false;
            }
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
    }

    /**
     * Finds author by full name, returns his id
     * and sends this list to a command
     * @param  fullName full name of author we are looking for
     * @return id of author, 0 if author wasn't found
     * @throws ServiceException
     */
    public static long findAuthorIdByFullName(String fullName) throws ServiceException {
        Author currentAuthor;
        try{
            currentAuthor = DAO.isAuthorExists(fullName);
            if (currentAuthor.getAuthorId() != AUTHOR_NOT_FOUND){
                return currentAuthor.getAuthorId();
            } else{
                logger.info("Author not found.");
                return AUTHOR_NOT_FOUND;
            }
        } catch (DAOException e){
            throw new ServiceException("Exception is occurred in ths method.", e);
        }
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Author} who wrote
     * book with bookId parameter and sends this list to a command
     * @param  bookId id of book in the db authors of which are looking for
     * @return List<Author> of authors
     * @throws ServiceException
     */
    public static List<Author> findAuthorsByBookId(long bookId) throws ServiceException {
        try{
            return DAO.findAuthorsByBookId(bookId);
        } catch (DAOException e){
            throw new ServiceException("Exception in findAuthorsListByBookId, Service layer",e);
        }
    }

    /**
     * Checks if author exists, if no, add basic info about him
     * into the db, and then add this author to a book
     * @param author full name of an author
     * @param bookId id of a book in the db for which author should be added
     * @throws ServiceException
     */
    public static void addOrUpdate(String author, long bookId) throws ServiceException {
        long authorId;
        try{
            logger.info("Check, if '" + author + "' exists.");
            if (AuthorService.findAuthorIdByFullName(author) == AUTHOR_NOT_FOUND){
                logger.info("There is no such author in the author's base. Adding...");
                authorId = AuthorService.addBasicAuthorInfo(author);
                BookService.addAuthorToBook(authorId, bookId);
            } else {
                logger.info("Author exists.");
                authorId = AuthorService.findAuthorIdByFullName(author);
                BookService.addAuthorToBook(authorId, bookId);
            }
        } catch (ServiceException e){
            throw new ServiceException("Exception occurred during checking author.", e);
        }
    }

    /**
     * Deletes authors for a certain book in the db
     * @param  bookId the identifier of the book whose authors we want to remove
     * @throws ServiceException
     */
    public static void deleteAllAuthorsForBook(long bookId) throws ServiceException {
        try{
            DAO.deleteAllAuthorsForBook(bookId);
        } catch (DAOException e){
            throw new ServiceException("Exception in findAuthorsListByBookId, Service layer",e);
        }
    }

    /**
     * Deletes all books for a certain author in the db
     * @param  authorId the identifier of the author whose books should be removed
     * @throws ServiceException
     */
    public static void deleteAllBooksByAuthor(long authorId) throws ServiceException{
        try{
            logger.info("Deleting all books for authors" + authorId);
            DAO.deleteAllBooksByAuthor(authorId);
        } catch (DAOException e){
            throw new ServiceException("Exception in findAuthorsListByBookId, Service layer",e);
        }
    }

    /**
     * Deletes certain author from the db
     * @param  authorId the identifier of the author to remove
     * @throws ServiceException
     */
    public static void deleteAuthorById(long authorId) throws ServiceException {
        try{
            logger.info("Received authorId = " + authorId);
            DAO.deleteAuthorById(authorId);
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in this method.", e);
        }
    }

    /**
     * Returns list of all authors in the db and sends this list to a command
     * @param from number representing start index
     * @param to number representing end index
     * @throws ServiceException
     * @return List<Author> list of {@link by.epam.bookrating.entity.Author} instances
     */
    public static List<Author> findAllAuthors(int from, int to) throws ServiceException{
        try{
            return DAO.findAllAuthors(from, to);
        } catch (DAOException e){
            throw new ServiceException("ServiceException is occurred during finding all authors.", e);
        }
    }

    /**
     * Returns total amount of authors in the db
     * and sends this list to a command
     * @throws ServiceException
     * @return total authors amount
     */
    public static int findAmountOfAuthors() throws ServiceException{
        try{
            return DAO.findAmountOfAuthors();
        } catch (DAOException e){
            throw new ServiceException("ServiceException is occurred during finding all authors.", e);
        }
    }
}
