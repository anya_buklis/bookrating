package by.epam.bookrating.service;
import by.epam.bookrating.dao.BookDAO;
import by.epam.bookrating.dao.impl.MySQLBookDAO;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.util.ImageLogic;
import org.apache.log4j.Logger;

import javax.servlet.http.Part;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p> Class contains methods for for processing data
 * received from controller layer, concerning with books. </p>
 *  @author Anna Buklis
 */
public class BookService {
    private static final BookDAO DAO = new MySQLBookDAO();
    private static Logger logger = Logger.getLogger(BookService.class);
    private static final String IMG_DIRECTORY_PATH = "img/";
    private static final int BOOK_NOT_FOUND = 0;

    /**
     * Creates book instance from method's parameters and sends it to a dao
     * for adding in the db
     * @param title of a book
     * @param publishingYear year of publishing
     * @param authors array of the full names of the authors
     * @param imagePart representing image
     * @param description little description of a book
     * @param absoluteDiskPath needed for uploading images
     * @param genres array of the identifiers of the genres
     * @return true if book added, false if book with such title already exists
     * @throws ServiceException
     */
    public static boolean addBook(String title, int publishingYear, String[] authors,
            String description, Part imagePart, String absoluteDiskPath, String[] genres)
            throws ServiceException {
        try {
            logger.info("Title = " + title + ", publishingYear = " + publishingYear +
            ", authors " + Arrays.toString(authors) + "\n description = " + description +
            ", genres = " + Arrays.toString(genres));

            if (findBookIdByTitle(title) == BOOK_NOT_FOUND) {
                String fileName = ImageLogic.getFileName(imagePart);
                String realImageUrl = IMG_DIRECTORY_PATH + fileName;
                String imageUrl = null;
                if (!realImageUrl.equals(IMG_DIRECTORY_PATH)) {
                    imageUrl = ImageLogic.addImage(imagePart, absoluteDiskPath);
                }
                Book book = new Book(title, description, publishingYear, imageUrl);
                long bookId = DAO.addBasicBookInfo(book);

                addGenresToTheBook(genres, bookId);

                for (String author : authors) {
                    AuthorService.addOrUpdate(author.trim(), bookId);
                }
                return true;
            } else {
                logger.info("Cannot add book because book with such title already exists.");
                return false;
            }
           } catch (DAOException e) {
                throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
    }

    /**
     * Returns total amount of books in the db
     * @throws ServiceException
     * @return total books amount
     */
    public static int getAmountOfBooks() throws ServiceException{
        int totalAmount;
        try{
             totalAmount = DAO.findAmountOfBooks();
            logger.info("Total amount of books = " + totalAmount);
        } catch (DAOException e){
            throw new ServiceException("Exception is occurred during finding total amount of books.", e);
        }
        return totalAmount;
    }

    /**
     * Deletes certain book from the db
     * @param  bookId the identifier of the book to remove
     * @throws ServiceException
     */
    public static void deleteBook(long bookId) throws ServiceException {
        try{
            logger.info("Received bookId = " + bookId);
            DAO.deleteBookById(bookId);
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
    }

    /**
     * Returns total amount of books as
     * a result of searching by title
     * @param title by which search should be done
     * @throws ServiceException
     * @return books amount with such title
     */
    public static int findAmountOfBooksByTitle(String title) throws ServiceException{
        int result;
        try {
            result = DAO.findAmountOfBooksByTitle(title.trim());
            logger.info(result + " books found by title " + title.trim());
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
        return result;
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * as a result of searching by the title
     * @param from number representing start index
     * @param to number representing end index
     * @param title title by which we are searching
     * @return List<Book> list of books with title like this
     * @throws ServiceException
     */
    public static List<Book> findBooksByTitle(int from, int to, String title) throws ServiceException{
        List<Book> books;
        try{
            logger.info("Title to search = " + title);
            books = DAO.findBooksByTitle(from, to, title.trim());
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in this method.", e);
        }
        return books;
    }

    /**
     * Calculates number of pages for pagination tag
     * @param totalAmount amount of authors
     * @param recordsPerPage amount of authors which are showed on the single page
     * @return number of pages
     */
    public static int calculatePagesAmount(int totalAmount, int recordsPerPage){
        return (int) Math.ceil(totalAmount * 1.0 / recordsPerPage);
    }

    /**
     * Creates book instance from method's parameters
     * and sends it to a dao for updating book row in the db.
     * Adds genres and authors to a book.
     * @param title of a book
     * @param publishingYear year of publishing
     * @param authors array of the full names of the authors
     * @param imagePart representing image4
     * @param oldImageUrl needed for updating image
     * @param description little description of a book
     * @param absoluteDiskPath needed for uploading images
     * @param genres array of the identifiers of the genres
     * @param bookId id of a book to update
     * @return true, if book updated, false if book with such title exists
     * @throws ServiceException
     */
    public static boolean editBook(long bookId, String title, String oldTitle,
                                   int publishingYear, String[] authors, String description,
                         String oldImageUrl, Part imagePart, String absoluteDiskPath, String[] genres)
            throws ServiceException {
        try {
            if (oldTitle.equalsIgnoreCase(title) ||
                    (!oldTitle.equalsIgnoreCase(title) && findBookIdByTitle(title) == BOOK_NOT_FOUND)) {
                logger.info("Title = " + title + ", publishingYear = " + publishingYear +
                        ", authors " + Arrays.toString(authors) + "\n description = " + description +
                        ", genres = " + Arrays.toString(genres));
                String fileName = ImageLogic.getFileName(imagePart);
                String realImageUrl = IMG_DIRECTORY_PATH + fileName;
                String imageUrl;
                if (!realImageUrl.equals(IMG_DIRECTORY_PATH)) {
                    imageUrl = ImageLogic.addImage(imagePart, absoluteDiskPath);
                } else {
                    imageUrl = oldImageUrl;
                }

                Book book = new Book(bookId, title, description, publishingYear, imageUrl);
                //Adding genres to a book

                GenreService.deleteAllGenresForBook(bookId);
                addGenresToTheBook(genres, bookId);

                //Adding authors to a book
                AuthorService.deleteAllAuthorsForBook(bookId);
                for (String author : authors) {
                    AuthorService.addOrUpdate(author.trim(), bookId);
                }

                logger.info("New image url " + imageUrl);
                DAO.updateBookItem(book);
                logger.info("Book is successfully updated.");
                return true;
            } else {
                return false;
            }
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added in db by a certain genre
     * @param from number representing start index
     * @param to number representing end index
     * @param genreId id of genre in the db
     * @return List<Book> list of books which are by given genre
     * @throws ServiceException
     */
    public static List<Book> findBooksByGenre(int from, int to, int genreId) throws ServiceException {
        logger.info("Genre id " + genreId);
        List<Book> books;
        try {
            books = DAO.findBooksByGenre(from, to, genreId);
            logger.info(books.size() + " books found.");
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
        return books;
    }

    /**
     * Returns total amount of books as
     * a result of searching by genre
     * @param genreId by which search should be done
     * @throws ServiceException
     * @return books amount with such title
     */
    public static int findAmountOfBooksByGenre(int genreId) throws ServiceException{
        int result;
        try {
            result = DAO.findAmountOfBooksByGenre(genreId);
            logger.info(result + " books found by genre " + genreId);
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred.", e);
        }
        return result;
    }

    /**
     * Returns total amount of books in read user's list
     * @param userId by which search should be done
     * @throws ServiceException
     * @return books amount in read user's list
     */
    public static int findAmountOfReadBooks(long userId) throws ServiceException{
        int result;
        try {
            result = DAO.findAmountOfReadBooks(userId);
            logger.info(result + " read books by user with id " + userId);
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred.", e);
        }
        return result;
    }

    /**
     * Returns total amount of books in favorite user's list
     * @param userId by which search should be done
     * @throws ServiceException
     * @return books amount in favorite user's list
     */
    public static int findAmountOfFavoriteBooks(long userId) throws ServiceException{
        int result;
        try {
            result = DAO.findAmountOfFavoriteBooks(userId);
            logger.info(result + " favorite books by user with id " + userId);
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred.", e);
        }
        return result;
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added in db for a certain author
     * @param from number representing start index
     * @param to number representing end index
     * @param authorId id of author in the db
     * @return List<Book> list of books which are written by a certain author
     * @throws ServiceException
     */
    public static List<Book> findBooksByAuthorId(int from, int to, long authorId) throws ServiceException {
        List<Book> books;
        try {
            books = DAO.findBooksByAuthor(from, to, authorId);
            logger.info("Books found by author with id '" + authorId + "': " + books.size());
        } catch (DAOException e) {
            throw new ServiceException("Exception occurred during finding books by authorId", e);
        }
        return books;
    }

    /**
     * Returns a certain part of book list in a db by a given interval
     * @param from number representing start index
     * @param to number representing end index
     * @return List<Book> list of books in this part
     * @throws ServiceException
     */
    public static List<Book> findAllBooks(int from, int to) throws ServiceException {
        List<Book> books;
        try{
            books = DAO.findAllBooks(from, to);
            logger.info(books.size() + " books at all.");
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in this method.", e);
        }
        return books;
    }

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Book} as key and
     * average rating as value representing books with highest ratings in the db
     * if they are exists. Otherwise returns empty map.
     * @return Map<Book,Double> map of books and rating left by users
     * @throws ServiceException
     */
    public static Map<Book, Double> findBooksWithHighRating() throws ServiceException {
        Map<Book, Double> booksWithRatings;
        try{
            booksWithRatings = DAO.findBooksWithHighRating();
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in this method.", e);
        }
        return booksWithRatings;
    }

    /**
     * Returns an book instance that can then be passed as a
     * parameter and displayed on a page.
     * @param  bookId id of book
     * @throws ServiceException
     * @return Appropriate {@link by.epam.bookrating.entity.Book} instance
     */
    public static Book findBookById(long bookId) throws ServiceException {
        logger.info("Received bookId = " + bookId);
        Book currentBook;
        try{
            currentBook = DAO.findBookById(bookId);
            currentBook.setAuthors(AuthorService.findAuthorsByBookId(bookId));
            currentBook.setGenres(GenreService.findGenresByBookId(bookId));
            logger.info(currentBook);
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
        return currentBook;
    }

    /**
     * Finds book id by a title
     * @param title of book
     * @throws ServiceException
     * @return bookId if book exists, 0 otherwise
     */
    public static long findBookIdByTitle(String title) throws ServiceException {
        Book currentBook;
        try{
            currentBook = DAO.isBookExists(title);
            if (currentBook != null){
                return currentBook.getBookId();
            } else{
                logger.info("Book not found.");
                return BOOK_NOT_FOUND;
            }
        } catch (DAOException e){
            throw new ServiceException("Exception is occurred in ths method.", e);
        }
    }

    /**
     * Adds a certain genres for a book
     * @param genres array of genres id that should be added for a book
     * @param bookId id of a book in the db for which genre should be added
     * @throws ServiceException
     */
    public static void addGenresToTheBook(String[] genres, long bookId) throws ServiceException {
        try{
            for (String genre: genres){
                DAO.addGenreToBook(Integer.parseInt(genre), bookId);
            }
        } catch (DAOException e){
            throw new ServiceException("Exception in addGenreToTheBook, Service layer",e);
        }
    }

    /**
     * Adds a certain author for a book
     * @param authorId id of a author in the db which should be added
     * @param bookId id of a book in the db for which author should be added
     * @throws ServiceException
     */
    public static void addAuthorToBook(long authorId, long bookId) throws ServiceException {
        try{
            if (DAO.isBookByThisAuthorAlreadyExists(authorId, bookId)){
                logger.info("Book with such name by this author already exists. Do nothing");
            } else {
                DAO.addAuthorToBook(authorId, bookId);
                logger.info("Author with id = "+authorId + " added to a book "+ bookId);
            }
        } catch (DAOException e){
            throw new ServiceException("Exception occurred during adding author to a book.", e);
        }
    }
}
