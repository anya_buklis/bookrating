package by.epam.bookrating.service;

import by.epam.bookrating.dao.BookDAO;
import by.epam.bookrating.dao.UserDAO;
import by.epam.bookrating.dao.impl.MySQLBookDAO;
import by.epam.bookrating.dao.impl.MySQLUserDAO;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.Statistics;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.util.HashLogic;
import by.epam.bookrating.util.ImageLogic;
import org.apache.log4j.Logger;

import javax.servlet.http.Part;
import java.util.List;
import java.util.Map;


/**
 * <p> Class contains methods for for processing data
 * received from controller layer, concerning with users. </p>
 *  @author Anna Buklis
 */
public class UserService {
    private static final UserDAO USER_DAO = new MySQLUserDAO();
    private static final BookDAO BOOK_DAO = new MySQLBookDAO();
    private static final String IMG_DIR_PATH = "img/";
    private static Logger logger = Logger.getLogger(UserService.class);

    /**
     * Updates user's information url in the db with the methods parameters
     * @param name user's name
     * @param age user's age
     * @param info user's autobiography
     * @param userId id of the user to update
     * @return user with updated information
     * @throws ServiceException
     */
    public static User editProfile(long userId, String name, int age, String info) throws ServiceException {
        User currentUser;
        try {
            logger.info("userId " + userId + ", name " + name + ", age " + age + ", info " + info);
            USER_DAO.updateProfileInformation(name, age, info.trim(), userId);
            currentUser = USER_DAO.findUserByUserId(userId);
        } catch (DAOException e){
            throw new ServiceException("Service Exception in EditBookService", e);
        }
        return currentUser;
    }

    /**
     * Updates user's avatar url in the db with the methods parameters
     * @param imagePart representing image
     * @param absoluteDiskPath path to uploading image
     * @param oldImage url of old avatar
     * @param userId id of the user to update
     * @throws ServiceException
     */
    public static void editAvatar(Part imagePart, String absoluteDiskPath, String oldImage, long userId)
            throws ServiceException {
        String fileName = ImageLogic.getFileName(imagePart);
        String realImageUrl= IMG_DIR_PATH + fileName;
        String imageUrl;

        imageUrl = ImageLogic.addImage(imagePart, absoluteDiskPath);
        if (!realImageUrl.equals(IMG_DIR_PATH) && !realImageUrl.equals(oldImage)){
            try {
                USER_DAO.updateAvatar(userId, imageUrl);
            } catch (DAOException e) {
                throw new ServiceException("Exception in editAvatar method", e);
            }
        } else {
            logger.warn("Picture wasn't change.");
        }
        logger.info("New avatar url " + imageUrl);
    }

    /**
     * Enters user in the system if such exists
     * @param login unique user's login
     * @param password hashed password
     * @return true, if user entered, false otherwise
     * @throws ServiceException
     */
    public static boolean login(String login, String password) throws ServiceException {
        try {
            logger.info("Login " + login);
            String hashPassword = HashLogic.hashPassword(password);
            if (USER_DAO.isLoginExists(login)) {
                User user = USER_DAO.findUserByLogin(login);
                if (hashPassword.equals(user.getPassword())) {
                    logger.info("User entered the system.");
                    return true;
                }
            } else {
                logger.info("User with such login doesn't exists.");
            }
        } catch (DAOException e) {
            throw new ServiceException("Exception in LoginService.", e);
        }
        return false;
    }

    /**
     * Calculates number of pages for pagination tag
     * @param totalAmount amount of users
     * @param recordsPerPage amount of users which are showed on the single page
     * @return number of pages
     */
    public static int calculatePagesAmount(int totalAmount, int recordsPerPage){
        return (int) Math.ceil(totalAmount * 1.0 / recordsPerPage);
    }

    /**
     * Adds an user's row in the db and fills it with the method's parameter
     * @param login user's login
     * @param password hashed user's password
     * @param name user's name
     * @return true if user successfully added, false if
     * author with such login exists
     * @throws ServiceException
     */
    public static boolean registerUser(String name, String login, String password) throws ServiceException {
        try{
            logger.info("Name = " + name + ", login = " + login);
            if (!USER_DAO.isLoginExists(login)){
                String hashPassword = HashLogic.hashPassword(password);
                USER_DAO.addUser(login, hashPassword, name);
                logger.info("User with login "+ login + " successfully registered.");
                return true;
            } else {
                logger.info("User with such login exists.");
            }
        } catch (DAOException e){
            throw new ServiceException("Exception in occurred during registration", e);
        }
        return false;
    }

    /**
     * Returns a certain user object that can then be passed as a
     * parameter and displayed on a page.
     * @param  userId id of the user in the db
     * @throws ServiceException
     * @return Appropriate {@link by.epam.bookrating.entity.User} instance
     */
    public static User findUserById(long userId) throws ServiceException {
        User currentUser;
        try{
            logger.info("Received userId " + userId);
            currentUser = USER_DAO.findUserByUserId(userId);
            logger.info("User found "+ currentUser);
        } catch ( DAOException e){
            throw new ServiceException("Exception in ViewSingleService", e);
        }
        return currentUser;
    }

    /**
     * Returns a certain user object that can then be passed as a
     * parameter and displayed on a page.
     * @param  login login of the user we are searching for
     * @return Appropriate {@link by.epam.bookrating.entity.User} instance
     * @throws ServiceException
     */
    public static User findUserByLogin(String login) throws ServiceException {
        User currentUser;
        try{
            currentUser = USER_DAO.findUserByLogin(login);
            logger.info("User found by login " + login + " : " + currentUser);
        } catch ( DAOException e){
            throw new ServiceException("Exception in ViewSingleService", e);
        }
        return currentUser;
    }

    /**
     * Bans certain user in the system by setting banned role
     * @param userId id of the user to ban
     * @throws ServiceException
     */
    public static void banUser(long userId) throws ServiceException {
        try {
            logger.info("Ban user with id " + userId);
            USER_DAO.banUser(userId);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in this method.", e);
        }
    }

    /**
     * Unlocks certain user in the system by setting regular role
     * @param userId id of the user to unlock
     * @throws ServiceException
     */
    public static void unlockUser(long userId) throws ServiceException {
        try {
            logger.info("Unlock user with id " + userId);
            USER_DAO.unlockUser(userId);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in this method.", e);
        }
    }

    /**
     * Returns total amount of users in the db
     * without admins
     * @throws ServiceException
     * @return total users amount
     */
    public static int findAmountOfUsers() throws ServiceException{
        int totalAmount;
        try{
            totalAmount = USER_DAO.findAmountOfUsers();
            logger.info(totalAmount + " users (not admins!) found.");
        } catch (DAOException e){
            throw new ServiceException("ServiceException is occurred during finding all authors.", e);
        }
        return totalAmount;
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added to a favorite list by a certain user
     * @param userId id of user in the db for whom books are looking for
     * @return List<Book> list of books which are in favorite list
     * @throws ServiceException
     */
    public static List<Book> findFavoriteBooks(int from, int to, long userId) throws ServiceException{
        List<Book> favoriteBooks;
        try {
            logger.info("Fining read books of user with id = " + userId);
            favoriteBooks = BOOK_DAO.findFavoriteBooks(from, to, userId);
            logger.info(favoriteBooks.size() + " favorite books found for this user.");
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
        return favoriteBooks;
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added to a read list by a certain user
     * @param userId id of user in the db for whom books are looking for
     * @return List<Book> list of books which are in read list
     * @throws ServiceException
     */
    public static List<Book> findReadBooks(int from, int to, long userId) throws ServiceException {
        List<Book> readBooks;
        try {
            logger.info("Fining read books of user with id = " + userId);
            readBooks = BOOK_DAO.findReadBooks(from, to, userId);
            logger.info(readBooks.size() + " read books found for this user.");
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in ths method.", e);
        }
        return readBooks;
    }

    /**
     * Deletes certain book from the user's favorite list
     * @param  bookId the identifier of the book to remove
     * @param userId id of user from which list the book should be removed
     * @throws ServiceException
     */
    public static void deleteBookFromFavorite(long userId, long bookId) throws ServiceException{
        try {
            logger.info("bookId = " + bookId + ", userId = " + userId);
            BOOK_DAO.deleteBookFromFavorite(userId, bookId);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in deleteBookFromFavorite method.", e);
        }
    }

    /**
     * Deletes certain book from the user's read list
     * @param  bookId the identifier of the book to remove
     * @param userId id of user from which list the book should be removed
     * @throws ServiceException
     */
    public static void deleteBookFromRead(long userId, long bookId) throws ServiceException{
        try {
            logger.info("bookId = " + bookId + ", userId = " + userId);
            BOOK_DAO.deleteBookFromRead(userId, bookId);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in deleteBookFromRead method.", e);
        }
    }

    /**
     * Adds certain book in user's favorite list
     * @param  bookId the identifier of the book to add
     * @param userId id of user for whom the book should be added in the list
     * @throws ServiceException
     */
    public static void addBookToFavorite(long userId, long bookId) throws ServiceException{
        try {
            logger.info("Adding book with id " + bookId + " to favorite list.");
            BOOK_DAO.addToFavorite(userId, bookId);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in addBookToFavorite method.", e);
        }
    }

    /**
     * Adds certain book in user's read list
     * @param  bookId the identifier of the book to add
     * @param userId id of user for whom the book should be added in the list
     * @throws ServiceException
     */
    public static void addBookToRead(long userId, long bookId) throws ServiceException{
        try {
            logger.info("Adding book with id " + bookId + " to read list.");
            BOOK_DAO.addToRead(userId, bookId);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in addBookToFavorite method.", e);
        }
    }

    /**
     * Checks if certain book is in user's favorite list
     * @param  bookId the identifier of the book to check
     * @param userId id of user for whom the book should be checked
     * @return true, if this book in favorite list, false otherwise
     * @throws ServiceException
     */
    public static boolean isBookInFavorite(long userId, long bookId) throws ServiceException{
        boolean result;
        try {
            result = BOOK_DAO.isBookInFavorite(userId, bookId);
            logger.info("Book in favorite = " + result);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in isBookInFavorite method.", e);
        }
        return result;
    }

    /**
     * Checks if certain book is in user's read list
     * @param  bookId the identifier of the book to check
     * @param userId id of user for whom the book should be checked
     * @return true, if this book in read list, false otherwise
     * @throws ServiceException
     */
    public static boolean isBookInRead(long userId, long bookId) throws ServiceException{
        boolean result;
        try {
            result = BOOK_DAO.isBookInRead(userId, bookId);
            logger.info("Book in read = " + result);
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in isBookInRead method.", e);
        }
        return result;
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Statistics} instances
     * for all users in the system (except admin)
     * @param from number representing start index
     * @param to number representing end index
     * @return List<Statistics> list of statistics for all users
     * @throws ServiceException
     */
    public static List<Statistics> findUserStatistics(int from, int to) throws ServiceException {
        List<Statistics> statisticsList;
        try {
            statisticsList = USER_DAO.findUsersStatics(from, to);
            logger.info(statisticsList.size() + " users rows received.");
        } catch (DAOException e) {
            throw new ServiceException("Service Exception is occurred in isBookInRead method.", e);
        }
        return statisticsList;
    }

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Book} as key and
     * {@link by.epam.bookrating.entity.Comment} as value for a certain user
     * if they are exists. Otherwise returns empty map.
     * @param  userId id of a certain user whose comments are looking for
     * @param from number representing start index
     * @param to number representing end index
     * @return Map<Book,Comment> map of books and comments left by user
     * @throws ServiceException
     */
    public static Map<Comment, Book> findAllCommentsByUserId(int from, int to, long userId) throws ServiceException{
        Map<Comment, Book> comments;
        try {
            comments = USER_DAO.findAllCommentsByUserId(from, to, userId);
            logger.info(comments.size() + " comments found by user.");
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in findAllCommentsByUserId method.", e);
        }
        return comments;
    }
}
