package by.epam.bookrating.service;
import by.epam.bookrating.dao.CommentDAO;
import by.epam.bookrating.dao.impl.MySQLCommentDAO;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.exception.ServiceException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p> Class contains methods for for processing data
 * received from controller layer, concerning with comments. </p>
 *  @author Anna Buklis
 */
public class  CommentService {
    private static final CommentDAO DAO = new MySQLCommentDAO();
    private static Logger logger = Logger.getLogger(CommentService.class);

    /**
     * Adds a comment in the db and fills it with the fields of method's parameter
     * @param bookId id of book for which comment left
     * @param userId id of user who left comment
     * @param commentText text
     * @throws ServiceException
     */
    public static void leaveComment(long bookId, long userId, String commentText)
            throws ServiceException {
        try {
            Date date = getCurrentDate();
            logger.info("bookId = " + bookId + ", userId = " + userId +
                    ", date = " + date + ", commentText = " + commentText);
            Comment comment = new Comment(bookId, userId, date, commentText);
            DAO.addComment(comment);
        } catch (DAOException e) {
            throw new ServiceException("Exception in CommentService, findAllCommentsByBookId method", e);
        }
    }

    /**
     * Deletes certain comment from the db
     * @param  commentId the identifier of the comment to remove
     * @throws ServiceException
     */
    public static void deleteComment(long commentId) throws ServiceException {
        try {
            logger.info("commentId = " + commentId);
            DAO.deleteComment(commentId);
        } catch (DAOException e) {
            throw new ServiceException("Exception in CommentService, findAllCommentsByBookId method", e);
        }
    }

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Comment} as key and
     * {@link by.epam.bookrating.entity.User} as value for a certain book
     * if they are exists. Otherwise returns empty map.
     * @param  bookId id of a book whose comments are looking for
     * @return Map<Comment,User> map of comments and users who left comments earlier
     * @throws ServiceException
     */
    public static Map<Comment, User> findCommentsByBookId(long bookId) throws ServiceException {
        Map<Comment, User> comments;
        try {
            comments = DAO.findAllCommentsByBookId(bookId);
            logger.info(comments.size() + " comments found for this book.");
        } catch (DAOException e) {
            throw new ServiceException("Exception in CommentService, findCommentsByBookId method", e);
        }
        return comments;
    }

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Comment} as key and
     * login as value for a certain date if they are exists. Otherwise returns empty map.
     * @return Map<Comment,String> map of comments and user's login
     * @throws ServiceException
     */
    public static Map<Comment, String> viewTodayComments() throws ServiceException{
        Map<Comment, String> comments;
        Date currentDate = getCurrentDate();
        logger.info("Current date = " + currentDate);
        try {
            comments = DAO.findTodayComments(currentDate);
            logger.info(comments.size() + " today comments.");
        } catch (DAOException e) {
            throw new ServiceException("Exception in CommentService, viewTodayComments method", e);
        }
        return comments;
    }

    /**
     * Returns the list of comments for a certain user
     * if they are exists. Otherwise returns empty list.
     * @param  userId id of user whose comments are looking for
     * @return  List<Comment> list of comments
     * @throws ServiceException
     */
    public static List<Comment> findCommentsByUserId(long userId) throws ServiceException{
        logger.info("User id = " + userId);
        List<Comment> comments;
        try {
            comments = DAO.findCommentsByUserId(userId);
            logger.info(comments.size() + " user comments.");
        } catch (DAOException e) {
            throw new ServiceException("Exception in findCommentsByUserId method", e);
        }
        return comments;
    }

    /**
     * Returns total amount of comment
     * left by certain user in the db
     * @param userId id of user by which search should be done
     * @throws ServiceException
     * @return total comments amount
     */
    public static int findAmountOfCommentsByUserId(long userId) throws ServiceException{
        int result;
        try {
            result = DAO.findCommentsAmountByUser(userId);
            logger.info(result + " comments found by user.");
        } catch (DAOException e){
            throw new ServiceException("Service Exception is occurred in findAllCommentsByUserId method.", e);
        }
        return result;
    }

    /**
     * Returns current date
     * @return  date
     */
    private static Date getCurrentDate(){
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDate localDate = currentTime.toLocalDate();
        return Date.valueOf(localDate);
    }
}
