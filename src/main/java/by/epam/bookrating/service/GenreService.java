package by.epam.bookrating.service;

import by.epam.bookrating.dao.GenreDAO;
import by.epam.bookrating.dao.impl.MySQLGenreDAO;
import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p> Class contains methods for for processing data
 * received from controller layer, concerning with genres. </p>
 *  @author Anna Buklis
 */
public class GenreService {
    private static final GenreDAO DAO = new MySQLGenreDAO();
    private static Logger logger = Logger.getLogger(GenreService.class);

    /**
     * Returns the list of genres from db
     * @return List<Genre> the list of genres
     * @throws ServiceException
     */
    public static List<Genre> findAllGenres() throws ServiceException {
        try{
            return DAO.findAllGenres();
        } catch (DAOException e){
            throw new ServiceException("Exception in findAllGenres in Service.", e);
        }
    }

    /**
     * Returns the certain {@link by.epam.bookrating.entity.Genre} genre instance
     * @param  genreId id of genre in the db that we are looking for
     * @return {@link by.epam.bookrating.entity.Genre} instance
     * @throws ServiceException
     */
    public static Genre findGenreById(int genreId) throws ServiceException{
        Genre genre;
        try{
            genre = DAO.findGenreById(genreId);
            logger.info("Found genre " + genre);
        } catch (DAOException e){
            throw new ServiceException("Exception .", e);
        }
        return genre;
    }

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Genre} that
     * are selected for a certain book
     * @param  bookId id of book in the db genres of which are looking for
     * @return List<Genre> the list of genres for a certain book
     * @throws ServiceException
     */
    public static List<Genre> findGenresByBookId(long bookId) throws ServiceException {
        try{
            return DAO.findGenresByBookId(bookId);
        } catch (DAOException e){
            throw new ServiceException("Exception in findGenresByBookId, Service layer",e);
        }
    }

    /**
     * Deletes all genres for a certain book in the db, that are selected for this book
     * @param  bookId the identifier of the book whose genres we want to remove
     * @throws ServiceException
     */
    public static void deleteAllGenresForBook(long bookId) throws ServiceException {
        try {
            DAO.deleteAllGenresForBook(bookId);
        } catch (DAOException e) {
            throw new ServiceException("Exception in deleteAllGenresForBook, Service layer",e);
        }
    }
}
