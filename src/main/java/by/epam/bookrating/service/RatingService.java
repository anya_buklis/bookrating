package by.epam.bookrating.service;
import by.epam.bookrating.dao.RatingDAO;
import by.epam.bookrating.dao.impl.MySQLRatingDAO;
import by.epam.bookrating.entity.Rating;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.exception.ServiceException;
import org.apache.log4j.Logger;

/**
 * <p> Class contains methods for for processing data
 * received from controller layer, concerning with ratings. </p>
 *  @author Anna Buklis
 */
public class RatingService {
    private static final RatingDAO DAO = new MySQLRatingDAO();
    private static Logger logger = Logger.getLogger(RatingService.class);

    /**
     * Adds an rating's row to the db and fills it with the method's parameters
     * @param userId the id of user who left the rating
     * @param bookId the id of book for which rating was left
     * @param rating the number representing 1-5 start left for a certain book
     * @throws ServiceException
     */
    public static void leaveRating(long userId, long bookId, int rating) throws ServiceException {
        try {
            logger.info("Leaving " + rating + " stars on the book with id = "+ bookId + " by user "+ userId);
            DAO.addRating(userId, bookId, rating);
        } catch (DAOException e) {
            throw new ServiceException("Exception in RatingService.", e);
        }
    }

    /**
     * Checks if rating by certain user for certain book exists.
     * If exists, returns this rating.
     * @param  bookId the identifier of the book for which rating are searching
     * @param  userId the identifier of the user
     * @return rating left by user of 0, if rating doesn't exit
     * @throws ServiceException
     */
    public static int checkRating(long bookId, long userId) throws ServiceException {
        int rating = 0;
        if(isRatingExists(bookId, userId)){
            rating = findRatingByUserIdAndBookId(bookId, userId).getRating();
        }
        logger.info(rating + " rating left by "+ userId + " for this book.");
        return rating;
    }

    /**
     * Returns average rating for a certain book
     * @param  bookId the identifier of the book for which avg rating should be calculated
     * @return average rating for this book
     * @throws ServiceException
     */
    public static double findAvgRatingByBookId(long bookId) throws ServiceException {
        double rating;
        try {
            rating = DAO.findAvgRatingByBookId(bookId);
            logger.info("Avg rating for this book " + rating);
        } catch (DAOException e) {
            throw new ServiceException("Exception in RatingService.", e);
        }
        return rating;
    }

    /**
     * Deletes certain rating from the db
     * @param  userId the identifier of the user who left the rating to remove
     * @param  bookId the identifier of the book for which user left the rating to remove
     * @throws ServiceException
     */
    public static void deleteRating(long userId, long bookId) throws ServiceException {
        try {
            logger.info("bookId=" + bookId + ", userId=" + userId);
            DAO.deleteRating(userId, bookId);
        } catch (DAOException e) {
            throw new ServiceException("Exception in RatingService.", e);
        }
    }

    /**
     * Returns true, if rating by certain user for certain book exists.
     * False otherwise.
     * @param  bookId the identifier of the book for which rating are searching
     * @param  userId the identifier of the user
     * @return true, if rating exits
     * @throws ServiceException
     */
    private static boolean isRatingExists(long bookId, long userId) throws ServiceException {
        try {
            return DAO.isRatingByThisUserExists(userId, bookId);
        } catch (DAOException e) {
            throw new ServiceException("Exception in RatingService.", e);
        }
    }

    /**
     * Returns certain {@link by.epam.bookrating.entity.Rating} instance
     * representing rating received from the db
     * @param  bookId the identifier of the book for which rating was left
     * @param  userId the identifier of the user who left this rating
     * @return {@link by.epam.bookrating.entity.Rating} instance
     * @throws ServiceException
     */
    private static Rating findRatingByUserIdAndBookId(long bookId, long userId) throws ServiceException {
        Rating rating;
        try {
            rating = DAO.findRatingByUserIdAndBookId(bookId, userId);
            logger.info("Rating left by user " + rating.getRating());
        } catch (DAOException e) {
            throw new ServiceException("Exception in RatingService.", e);
        }
        return rating;
    }


}
