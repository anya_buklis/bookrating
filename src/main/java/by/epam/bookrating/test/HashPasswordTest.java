package by.epam.bookrating.test;

import by.epam.bookrating.util.HashLogic;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test to check if password hashing returns correct results
 * @author Anna Buklis
 */
public class HashPasswordTest {
    private String password;

    @Before
    public void setUp() throws Exception {
        password = "123456";
    }

    @Test
    public void testExistingOfResourceBundle() throws Exception {
        String result1 = HashLogic.hashPassword(password);
        String result2 = HashLogic.hashPassword(password);
        Assert.assertEquals(result1, result2);
    }

    @After
    public void tearDown() throws Exception {
        password = null;
    }
}
