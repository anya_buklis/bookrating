package by.epam.bookrating.test;
import by.epam.bookrating.pool.ConnectionPool;
import org.junit.Test;
import java.sql.Connection;

/**
 * Test to check if takeConnection() method of ConnectionPool class
 * returns connection
 * @author Anna Buklis
 */
public class TakeConnectionTest {
    @Test
    public void testTakeConnection() throws Exception {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        assert connection!=null;
    }
}
