package by.epam.bookrating.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ResourceBundle;

/**
 * Test to check if a ResourceBundle with a specific name exists
 * @author Anna Buklis
 */
public class ResourceBundleTest {
    private String resourceBundleName;
    private String keyToCheck;

    @Before
    public void setUp() throws Exception {
        resourceBundleName = "db";
        keyToCheck = "DB_DRIVER_CLASS";
    }

    @Test
    public void testExistingOfResourceBundle() throws Exception {
        ResourceBundle bundle = ResourceBundle.getBundle(resourceBundleName);
        boolean actualResult = bundle.containsKey(keyToCheck);
        assert actualResult;
    }

    @After
    public void tearDown() throws Exception {
        resourceBundleName = null;
        keyToCheck = null;
    }
}
