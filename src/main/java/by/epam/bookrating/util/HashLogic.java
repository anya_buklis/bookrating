package by.epam.bookrating.util;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Class contains of method for hashing password using sha256 encryption with salt.
 * @author anyab
 */
public class HashLogic {
    private static final String SALT = "Salt12@$@4&#%^$*";

    /**
     * Returns hashed user's password
     * @param  password string representing user's password
     * @return hashed password
     */
    public static String hashPassword(String password) {
        return DigestUtils.sha256Hex(password + SALT);
    }
}
