package by.epam.bookrating.util;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;

/**
 * Custom tag for creating pagination links in jsp<br/>
 * Implements {@link Tag}
 * @author Anna Buklis
 */
public class PaginationTag implements Tag {
    private PageContext pageContext;
    private Tag parentTag;
    private String action;
    private int totalPageAmount;
    private int viewPageCount = 1;
    private int startIndex = 1;
    private static final String PAGE = "page";
    private static final String NEXT = ">";
    private static final String NEXT_lAST = ">>";
    private static final String PREVIOUS = "<";
    private static final String PREVIOUS_LAST = "<<";
    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag parentTag) {
        this.parentTag = parentTag;
    }

    public void setTotalPageAmount(int pageCount) {
        this.totalPageAmount = pageCount;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setViewPageCount(int minPages) throws JspException {
        this.viewPageCount = minPages;
    }

    public int doStartTag() throws JspException {
        if(viewPageCount < 1) {
            throw new JspException("minimum page count should be greater than zero");
        }
        return Tag.SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        if(this.totalPageAmount <= 1) {
            return Tag.SKIP_PAGE;
        }
        ServletRequest request = pageContext.getRequest();
        String page = request.getParameter(PAGE);
        if (page == null){
            page = "1";
        }
        if((Integer.parseInt(page) > totalPageAmount || Integer.parseInt(page) <= 0)) {
            throw new JspException("Page Number : " + page + " wrong requested page, " +
                    "requested page must be greater than 0 and less than or equal to total page count");
        }
        JspWriter out = pageContext.getOut();
        int endIndex = this.startIndex + this.viewPageCount - 1;
        try {
            if(Integer.parseInt(page) > endIndex) {
                if(Integer.parseInt(page)==totalPageAmount) {
                    startIndex = totalPageAmount-viewPageCount+1;
                } else {
                    startIndex = startIndex + 1;
                }
                endIndex = Integer.parseInt(page);
            }

            if(Integer.parseInt(page) < startIndex) {
                startIndex = Integer.parseInt(page);
                if(Integer.parseInt(page)==1) {
                    endIndex = viewPageCount;
                } else {
                    endIndex = endIndex -1;
                }
            }

            if(startIndex > 1) {
                out.write(getLink(this.action, 1, false, PREVIOUS_LAST));
                out.write(getLink(this.action, (Integer.parseInt(page)-1), false, PREVIOUS));
            }

            for(int i = startIndex; i <= endIndex; i++) {
                if((i==1) || (i==(Integer.parseInt(page)))) {
                    out.write(getLink(this.action, i, true, String.valueOf(i)));
                } else {
                    out.write(getLink(this.action, i, false, String.valueOf(i)));
                }
            }

            if(endIndex < this.totalPageAmount) {
                out.write(getLink(this.action, (Integer.parseInt(page)+1), false, NEXT));
                out.write(getLink(this.action, this.totalPageAmount, false, NEXT_lAST));
            }
            out.flush();
        } catch (IOException e) {
            throw new JspException("IOException is occurred during writing a tag", e);
        }
        return Tag.EVAL_PAGE;
    }

    private String getLink(final String action, final int page, final boolean isCurrentPage, final String desc) {
        return "<a href='" + action + "&page=" + page + "'><span class='" +
                (isCurrentPage ? "current-page" : "available-page") + "'>" + desc + "</span></a>&nbsp;";
    }

    public Tag getParent() {
        return null;
    }

    public void release() {
    }
}
