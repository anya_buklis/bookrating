package by.epam.bookrating.util;
import org.apache.log4j.Logger;

import javax.servlet.http.Part;
import java.io.*;

/**
 * Class contains methods needed for uploading images.
 * @author anyab
 */

public class ImageLogic {
    private static Logger logger = Logger.getLogger(ImageLogic.class);
    private static final String DIRECTORY_NAME =  "img";
    private static final int BYTE_SIZE = 1024;
    private static final String SEPARATOR = "/";
    private static final String FILE_NAME = "filename";
    private static final String DISPOSITION = "content-disposition";
    private static final char EQUAL = '=';
    private static final String SEMI_COLUMN = ";";
    private static final String EMPTY_STRING = "";

    /**
     * Gets real file name of an image
     * @param part representing image
     * @return file name
     */
    public static String getFileName(final Part part) {
        for (String content : part.getHeader(DISPOSITION).split(SEMI_COLUMN)) {
            if (content.trim().startsWith(FILE_NAME)) {
                return content.substring(
                        content.indexOf(EQUAL) + 1).trim().replace("\"", EMPTY_STRING);
            }
        }
        return null;
    }

    /**
     * Loads image into the system
     * @param  imgPart representing image
     * @param  absolutePath absolute path needed for uploading
     * @return imageUrl which is url of an image n a app
     */
    public static String addImage(Part imgPart, String absolutePath) {
        String fileName = getFileName(imgPart);
        String pathToImgDir = absolutePath + DIRECTORY_NAME;
        File directory = new File(pathToImgDir);
        if (!directory.exists()){
            directory.mkdir();
        }
        String imageUrl = DIRECTORY_NAME + File.separator + fileName;
        try (OutputStream out = new FileOutputStream(new File(pathToImgDir + SEPARATOR + fileName));
             InputStream fileContent = imgPart.getInputStream()) {
            logger.info("Try to copy image to a file " + pathToImgDir + SEPARATOR + fileName);
            int read;
            final byte[] bytes = new byte[BYTE_SIZE];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            logger.info("Image uploaded.");
        } catch (IOException e){
            logger.warn("Exception is occurred during writing image.");
        }
        return imageUrl;
    }
}
