package by.epam.bookrating.util;

import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class consists of methods for validating data<br/>
 * @author Anna Buklis
 */
public class Validator {
    private static Logger logger = Logger.getLogger(Validator.class);
    private static final String LOGIN_REGEXP = "[\\w\\-]{5,10}$";
    private static final String PASSWORD_REGEXP = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,10}";
    private static final String TAG_EXISTING_REGEXP = "[^<>]*";
    private static final String LETTERS_REGEXP = "[^0-9!@#$%^&*():}{>?<.,`~]*";

    private static final int MIN_NAME_LENGTH = 2;
    private static final int MAX_NAME_LENGTH = 30;
    private static final int MIN_LOGIN_LENGTH = 5;
    private static final int MAX_LOGIN_LENGTH = 10;
    private static final int MIN_PASS_LENGTH = 6;
    private static final int MAX_PASS_LENGTH = 10;
    private static final int MIN_BIRTH_YEAR = 1000;
    private static final int MIN_PUBLISHING_YEAR = 1400;
    private static final int MIN_BIOGRAPHY_LENGTH = 60;
    private static final int MIN_DESCRIPTION_LENGTH = 30;
    private static final int MAX_BIOGRAPHY_LENGTH = 1500;
    private static final int MAX_USER_INFO_LENGTH = 300;
    private static final int MAX_AUTHOR_NAME_LENGTH = 100;
    private static final int MAX_COUNTRY_LENGTH = 60;
    private static final int MIN_COUNTRY_LENGTH = 3;
    private static final int MIN_GENRES_LENGTH = 1;
    private static final int MIN_AUTHOR_NAME_LENGTH = 5;
    private static final int MAX_GENRES_LENGTH = 10;
    private static final int MIN_TITLE_LENGTH = 1;
    private static final int MAX_TITLE_LENGTH = 200;
    private static final int MIN_AUTHOR_AGE = 18;
    private static final int MIN_AGE = 12;
    private static final int MAX_AGE = 100;
    private static final int MIN_COMMENT_LENGTH = 10;
    private static final int MAX_COMMENT_LENGTH = 2000;
    private static final int CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);

    public static boolean isUserNameValid(String userName){
        boolean result = MIN_NAME_LENGTH <= userName.length()&&
                userName.length() <= MAX_NAME_LENGTH;
        if (!result){
            logger.warn("UserName field is invalid.");
        } else {
            logger.info("UserName field is valid.");
        }
        return result;
    }

    public static boolean isLoginValid(String login){
        boolean result = login.length() >= MIN_LOGIN_LENGTH &&
                login.length() <= MAX_LOGIN_LENGTH &&
                validateByRegexp(login, LOGIN_REGEXP);
        if (!result){
            logger.warn("Login field is invalid.");
        } else {
            logger.info("Login field is valid.");
        }
        return result;
    }

    public static boolean isPasswordValid(String password){
        boolean result = password.length() >= MIN_PASS_LENGTH &&
                password.length() <= MAX_PASS_LENGTH &&
                validateByRegexp(password, PASSWORD_REGEXP);
        if (!result){
            logger.warn("Password field is invalid.");
        } else {
            logger.info("Password field is valid.");
        }
        return result;
    }

    public static boolean isBirthYearValid(String birthYear){
        boolean result;
        try{
            int birthYearInt = Integer.parseInt(birthYear);
            int maxBirthYear = CURRENT_YEAR  - MIN_AUTHOR_AGE;

            result = birthYearInt >= MIN_BIRTH_YEAR && birthYearInt <= maxBirthYear;
            if (!result){
                logger.warn("BirthYear field is invalid.");
            } else {
                logger.info("BirthYear field is valid.");
            }
            return result;
        } catch (NumberFormatException e){
            logger.warn("Wrong type of birth year field received.");
            return false;
        }
    }

    public static boolean isBiographyValid(String biography){
        int realLength = biography.trim().length();
        boolean result = MIN_BIOGRAPHY_LENGTH <= realLength &&
                realLength <= MAX_BIOGRAPHY_LENGTH &&
                validateByRegexp(biography, TAG_EXISTING_REGEXP);
        if (!result){
            logger.warn("Biography field is invalid.");
        } else {
            logger.info("Biography field is valid.");
        }
        return result;
    }

    public static boolean isBirthCountryValid(String birthCountry){
        int realLength = birthCountry.trim().length();
        boolean result = realLength >= MIN_COUNTRY_LENGTH &&
                realLength <=  MAX_COUNTRY_LENGTH &&
                validateByRegexp(birthCountry, TAG_EXISTING_REGEXP) &&
                validateByRegexp(birthCountry, LETTERS_REGEXP);
        if (!result){
            logger.warn("Birth Country field is invalid.");
        } else {
            logger.info("Birth Country field is valid.");
        }
        return result;
    }

    public static boolean isFullNameValid(String fullName){
        int realLength = fullName.trim().length();
        boolean result = realLength >= MIN_AUTHOR_NAME_LENGTH &&
                realLength <= MAX_AUTHOR_NAME_LENGTH &&
                validateByRegexp(fullName, TAG_EXISTING_REGEXP) &&
                validateByRegexp(fullName, LETTERS_REGEXP);
        if (!result){
            logger.warn("FullName field is invalid.");
        } else {
            logger.info("FullName field is valid.");
        }
        return result;

    }

    public static boolean isPublishingYearValid(String publishingYear){
        boolean result;
        try{
            int publishingYearInt = Integer.parseInt(publishingYear);
            result = publishingYearInt > MIN_PUBLISHING_YEAR &&
                    publishingYearInt <= CURRENT_YEAR;
            if (!result){
                logger.warn("PublishingYear field is invalid.");
            } else {
                logger.info("PublishingYear field is valid.");
            }
            return result;
        } catch (NumberFormatException e){
            logger.warn("Wrong type of publishing year received.");
            return false;
        }
    }

    public static boolean isTitleValid(String title){
        int realLength = title.trim().length();
        boolean result = realLength >= MIN_TITLE_LENGTH &&
                realLength <= MAX_TITLE_LENGTH &&
                validateByRegexp(title, TAG_EXISTING_REGEXP);
        if (!result){
            logger.warn("Title field is invalid.");
        } else {
            logger.info("Title field is valid.");
        }
        return result;
    }

    public static boolean isAuthorsValid(String[] authors){
        boolean result = true;
        if (authors != null && authors.length != 0){
            for (String author: authors) {
                if (!isFullNameValid(author)) {
                    logger.warn(author + " invalid.");
                    result = false;
                    break;
                }
            }
        } else {
            logger.warn("Authors not specified.");
            result = false;
        }
        return result;
    }

    public static boolean isAgeValid(String age){
        boolean result;
        try{
            int ageInt = Integer.parseInt(age);
            result = MIN_AGE <= ageInt && ageInt <= MAX_AGE;
            if (!result){
                logger.warn("Age field is invalid.");
            } else {
                logger.info("Age field is valid.");
            }
        } catch (NumberFormatException e){
            logger.warn("Wrong type of age field received.");
            return false;
        }
        return result;
    }

    public static boolean isGenresValid(String[] genres){
        boolean result = genres != null &&
                genres.length >= MIN_GENRES_LENGTH
                && genres.length <= MAX_GENRES_LENGTH;
        if (!result){
            logger.warn("Genres field is invalid.");
        } else {
            logger.info("Genres field is valid.");
        }
        return result;
    }

    public static boolean isDescriptionValid(String description){
        int realLength = description.trim().length();
        boolean result = MIN_DESCRIPTION_LENGTH <= realLength &&
                realLength <= MAX_BIOGRAPHY_LENGTH
                && validateByRegexp(description, TAG_EXISTING_REGEXP);
        if (!result){
            logger.warn("Description field is invalid.");
        } else {
            logger.info("Description field is valid.");
        }
        return result;
    }

    public static boolean isUserInfoValid(String info){
        int realLength = info.trim().length();
        boolean result = realLength <= MAX_USER_INFO_LENGTH &&
                validateByRegexp(info, TAG_EXISTING_REGEXP);
        if (!result){
            logger.warn("UserInfo field is invalid.");
        } else {
            logger.info("UserInfo field is valid.");
        }
        return result;
    }

    public static boolean isCommentTextValid(String commentText){
        int realLength = commentText.trim().length();
        boolean result = realLength >= MIN_COMMENT_LENGTH
                && realLength <= MAX_COMMENT_LENGTH
                && validateByRegexp(commentText, TAG_EXISTING_REGEXP);
        if (!result){
            logger.warn("CommentText field is invalid.");
        } else {
            logger.info("CommentText field is valid.");
        }
        return result;
    }

    private static boolean validateByRegexp(String stringToCheck, String pattern){
        Pattern nameRegexp = Pattern.compile(pattern);
        Matcher matcher = nameRegexp.matcher(stringToCheck);
        return matcher.matches();
    }
}
