package by.epam.bookrating.dao;

import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.DAOException;

import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * Interface consisting of methods to deal with comments<br/>
 * Implements {@link by.epam.bookrating.dao.IDAO}
 * @author Anna Buklis
 */
public interface CommentDAO extends IDAO {

    /**
     * Adds a comment's row in the db and fills it with the fields of method's parameter
     * @param comment {@link by.epam.bookrating.entity.Comment} instance consists of comment's info
     * @throws DAOException
     */
    void addComment(Comment comment) throws DAOException;

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Comment} as key and
     * {@link by.epam.bookrating.entity.User} as value for a certain book
     * if they are exists. Otherwise returns empty map.
     * @param  bookId id of a book whose comments are looking for
     * @return Map<Comment,User> map of comments and users who left comments earlier
     * @throws DAOException
     */
    Map<Comment, User> findAllCommentsByBookId(long bookId) throws DAOException;

    /**
     * Deletes certain comment from the db
     * @param  commentId the identifier of the comment to remove
     * @throws DAOException
     */
    void deleteComment(long commentId) throws DAOException;

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Comment} as key and
     * login as value for a certain date if they are exists. Otherwise returns empty map.
     * @param  currentDate current date
     * @return Map<Comment,String> map of comments and user's login
     * @throws DAOException
     */
    Map<Comment, String> findTodayComments(Date currentDate) throws DAOException;

    /**
     * Returns the list of comments for a certain user
     * if they are exists. Otherwise returns empty list.
     * @param  userId id of user whose comments are looking for
     * @return  List<Comment> list of comments
     * @throws DAOException
     */
    List<Comment> findCommentsByUserId(long userId) throws DAOException;

    /**
     * Returns total amount of comment
     * left by certain user in the db
     * @param userId id of user by which search should be done
     * @throws DAOException
     * @return total comments amount
     */
    int findCommentsAmountByUser(long userId) throws DAOException;
}
