package by.epam.bookrating.dao;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * Interface consisting of methods to deal with books<br/>
 * Implements {@link by.epam.bookrating.dao.IDAO}
 * @author Anna Buklis
 */
public interface BookDAO extends IDAO {
    /**
     * Adds an book's row in the db and fills it with the fields of method parameter
     * @param book {@link by.epam.bookrating.entity.Book} instance consists of book's info
     * @return bookId generated during adding in the db
     * @throws DAOException
     */
    long addBasicBookInfo(Book book) throws DAOException;

    /**
     * Updates the book's info in the db with the method's parameter fields
     * @param  book {@link by.epam.bookrating.entity.Book} object,
     * consists of fields to update
     * @throws DAOException
     */
    void updateBookItem(Book book) throws DAOException;

    /**
     * Returns total amount of books in the db
     * @throws DAOException
     * @return total books amount
     */
    int findAmountOfBooks() throws DAOException;

    /**
     * Returns amount of books in the db
     * with such genre
     * @throws DAOException
     * @return total books amount
     */
    int findAmountOfBooksByGenre(int genreId) throws DAOException;

    /**
     * Returns total amount of books as
     * a result of searching by title
     * @param title by which search should be done
     * @throws DAOException
     * @return books amount with such title
     */
    int findAmountOfBooksByTitle(String title) throws DAOException;

    /**
     * Returns total amount of books in read user's list
     * @param userId by which search should be done
     * @throws DAOException
     * @return books amount in read user's list
     */
    int findAmountOfReadBooks(long userId) throws DAOException;

    /**
     * Returns total amount of books in favorite user's list
     * @param userId by which search should be done
     * @throws DAOException
     * @return books amount in favorite user's list
     */
    int findAmountOfFavoriteBooks(long userId) throws DAOException;

    /**
     * Returns an book instance that can then be passed as a
     * parameter and displayed on a page.
     * @param  bookId id of book
     * @throws DAOException
     * @return Appropriate {@link by.epam.bookrating.entity.Book} instance
     */
    Book findBookById(long bookId) throws DAOException;

    /**
     * Returns a certain part of book list in a db by a given interval
     * @param from number representing start index
     * @param to number representing end index
     * @return List<Book> list of books in this part
     * @throws DAOException
     */
    List<Book> findAllBooks(int from, int to) throws DAOException;

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Book} as key and
     * average rating as value representing books with highest ratings in the db
     * if they are exists. Otherwise returns empty map.
     * @return Map<Book,Double> map of books and rating left by users
     * @throws DAOException
     */
    Map<Book, Double> findBooksWithHighRating() throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added to a read list by a certain user
     * @param userId id of user in the db for whom books are looking for
     * @param from number representing start index
     * @param to number representing end index
     * @return List<Book> list of books which are in read list
     * @throws DAOException
     */
    List<Book> findReadBooks(int from, int to, long userId) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added to a favorite list by a certain user
     * @param userId id of user in the db for whom books are looking for
     * @param from number representing start index
     * @param to number representing end index
     * @return List<Book> list of books which are in favorite list
     * @throws DAOException
     */
    List<Book> findFavoriteBooks(int from, int to, long userId) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added in db by a certain genre
     * @param from number representing start index
     * @param to number representing end index
     * @param genreId id of genre in the db
     * @return List<Book> list of books which are by given genre
     * @throws DAOException
     */
    List<Book> findBooksByGenre(int from, int to, int genreId) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * as a result of searching by the title
     * @param from number representing start index
     * @param to number representing end index
     * @param title title by which we are searching
     * @return List<Book> list of books with title like this
     * @throws DAOException
     */
    List<Book> findBooksByTitle(int from, int to, String title) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Book} instances
     * which was added in db for a certain author
     * @param from number representing start index
     * @param to number representing end index
     * @param authorId id of author in the db
     * @return List<Book> list of books which are written by a certain author
     * @throws DAOException
     */
    List<Book> findBooksByAuthor(int from, int to, long authorId) throws DAOException;

    /**
     * Adds certain book in user's favorite list
     * @param  bookId the identifier of the book to add
     * @param userId id of user for whom the book should be added in the list
     * @throws DAOException
     */
    void addToFavorite(long userId, long bookId) throws DAOException;

    /**
     * Adds certain book in user's read list
     * @param  bookId the identifier of the book to add
     * @param userId id of user for whom the book should be added in the list
     * @throws DAOException
     */
    void addToRead(long userId, long bookId) throws DAOException;

    /**
     * Adds a certain genre for a book
     * @param genreId id of a genre in the db which should be added
     * @param bookId id of a book in the db for which genre should be added
     * @throws DAOException
     */
    void addGenreToBook(int genreId, long bookId) throws DAOException;

    /**
     * Adds a certain author for a book
     * @param authorId id of a author in the db which should be added
     * @param bookId id of a book in the db for which author should be added
     * @throws DAOException
     */
    void addAuthorToBook(long authorId, long bookId) throws DAOException;

    /**
     * Checks if certain book is in user's favorite list
     * @param  bookId the identifier of the book to check
     * @param userId id of user for whom the book should be checked
     * @return true, if this book in favorite list, false otherwise
     * @throws DAOException
     */
    boolean isBookInFavorite(long userId, long bookId) throws DAOException;

    /**
     * Checks if certain book is in user's read list
     * @param  bookId the identifier of the book to check
     * @param userId id of user for whom the book should be checked
     * @return true, if this book in read list, false otherwise
     * @throws DAOException
     */
    boolean isBookInRead(long userId, long bookId) throws DAOException;

    /**
     * Returns the result of checking existing of a certain book for a certain author
     * @param authorId id of a author in the db for whom book is looking for
     * @param bookId id of a book in the db for which is looking for
     * @return true, if book by this author already exists, false otherwise
     * @throws DAOException
     */
    boolean isBookByThisAuthorAlreadyExists(long authorId, long bookId) throws DAOException;

    /**
     * Returns the result of checking existing of a certain book.
     * If book exists, returns this book, otherwise empty object
     * @param title title of a book in the db which are searching
     * @return {@link by.epam.bookrating.entity.Book} instance of appropriate book
     * @throws DAOException
     */
    Book isBookExists(String title) throws DAOException;

    /**
     * Deletes certain book from the user's read list
     * @param  bookId the identifier of the book to remove
     * @param userId id of user from which list the book should be removed
     * @throws DAOException
     */
    void deleteBookFromRead(long userId, long bookId) throws DAOException;

    /**
     * Deletes certain book from the user's favorite list
     * @param  bookId the identifier of the book to remove
     * @param userId id of user from which list the book should be removed
     * @throws DAOException
     */
    void deleteBookFromFavorite(long userId, long bookId) throws DAOException;

    /**
     * Deletes certain book from the db
     * @param  bookId the identifier of the book to remove
     * @throws DAOException
     */
    void deleteBookById(long bookId) throws DAOException;
}
