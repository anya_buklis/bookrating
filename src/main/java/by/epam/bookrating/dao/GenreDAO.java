package by.epam.bookrating.dao;

import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.exception.DAOException;

import java.util.List;

/**
 * Interface consisting of methods to deal with genres<br/>
 * Implements {@link by.epam.bookrating.dao.IDAO}
 * @author Anna Buklis
 */
public interface GenreDAO extends IDAO {

    /**
     * Returns the list of genres from db
     * @return List<Genre> the list of genres
     * @throws DAOException
     */
    List<Genre> findAllGenres() throws DAOException;

    /**
     * Returns the certain {@link by.epam.bookrating.entity.Genre} genre instance
     * @param  genreId id of genre in the db that we are looking for
     * @return {@link by.epam.bookrating.entity.Genre} instance
     * @throws DAOException
     */
    Genre findGenreById(int genreId) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Genre} that
     * are selected for a certain book
     * @param  bookId id of book in the db genres of which are looking for
     * @return List<Genre> the list of genres for a certain book
     * @throws DAOException
     */
    List<Genre> findGenresByBookId(long bookId) throws DAOException;

    /**
     * Deletes all genres for a certain book in the db, that are selected for this book
     * @param  bookId the identifier of the book whose genres we want to remove
     * @throws DAOException
     */
    void deleteAllGenresForBook(long bookId) throws DAOException;
}
