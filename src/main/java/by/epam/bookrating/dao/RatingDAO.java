package by.epam.bookrating.dao;
import by.epam.bookrating.entity.Rating;
import by.epam.bookrating.exception.DAOException;

/**
 * Interface consisting of methods to deal with ratings<br/>
 * Implements {@link by.epam.bookrating.dao.IDAO}
 * @author Anna Buklis
 */
public interface RatingDAO extends IDAO {

    /**
     * Adds an rating's row to the db and fills it with the method's parameters
     * @param userId the id of user who left the rating
     * @param bookId the id of book for which rating was left
     * @param rating the number representing 1-5 start left for a certain book
     * @throws DAOException
     */
    void addRating(long userId, long bookId, int rating) throws DAOException;

    /**
     * Returns true, if rating by certain user for certain book exists.
     * False otherwise.
     * @param  bookId the identifier of the book for which rating are searching
     * @param  userId the identifier of the user
     * @return true, if rating exits
     * @throws DAOException
     */
    boolean isRatingByThisUserExists(long userId, long bookId) throws DAOException;

    /**
     * Returns certain {@link by.epam.bookrating.entity.Rating} instance
     * representing rating received from the db
     * @param  bookId the identifier of the book for which rating was left
     * @param  userId the identifier of the user who left this rating
     * @return {@link by.epam.bookrating.entity.Rating} instance
     * @throws DAOException
     */
    Rating findRatingByUserIdAndBookId(long bookId, long userId) throws DAOException;

    /**
     * Returns average rating for a certain book
     * @param  bookId the identifier of the book for which avg rating should be calculated
     * @return average rating for this book
     * @throws DAOException
     */
    double findAvgRatingByBookId(long bookId) throws DAOException;

    /**
     * Deletes certain rating from the db
     * @param  userId the identifier of the user who left the rating to remove
     * @param  bookId the identifier of the book for which user left the rating to remove
     * @throws DAOException
     */
    void deleteRating(long userId, long bookId) throws DAOException;
}
