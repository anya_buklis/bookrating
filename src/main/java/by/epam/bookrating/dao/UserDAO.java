package by.epam.bookrating.dao;

import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.Statistics;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * Interface consisting of methods to deal with users<br/>
 * Implements {@link by.epam.bookrating.dao.IDAO}
 * @author Anna Buklis
 */
public interface UserDAO extends IDAO {

    /**
     * Adds an user's row in the db and fills it with the method's parameter
     * @param login user's login
     * @param password hashed user's password
     * @param name user's name
     * @throws DAOException
     */
    void addUser(String login, String password, String name) throws DAOException;

    /**
     * Returns a certain user object that can then be passed as a
     * parameter and displayed on a page.
     * @param  userId id of the user in the db
     * @throws DAOException
     * @return Appropriate {@link by.epam.bookrating.entity.User} instance
     */
    User findUserByUserId(long userId) throws DAOException;

    /**
     * Returns total amount of users in the db
     * without admins
     * @throws DAOException
     * @return total users amount
     */
    int findAmountOfUsers() throws DAOException;

    /**
     * Returns true, if user with certain login exists. False otherwise.
     * @param  login the login of the user we are searching for
     * @return true, if user with such login exits
     * @throws DAOException
     */
    boolean isLoginExists(String login) throws DAOException;

    /**
     * Updates user information url in the db with the methods parameters
     * @param name user's name
     * @param age user's age
     * @param info user's autobiography
     * @param userId id of the user to update
     * @throws DAOException
     */
    void updateProfileInformation(String name, int age, String info, long userId) throws DAOException;

    /**
     * Updates avatar url in the db for a certain user
     * @param userId id of the user to update
     * @param imageUrl url of new avatar
     * @throws DAOException
     */
    void updateAvatar(long userId, String imageUrl) throws DAOException;

    /**
     * Unlocks certain user in the system by setting regular role
     * @param userId id of the user to unlock
     * @throws DAOException
     */
    void unlockUser(long userId) throws DAOException;

    /**
     * Bans certain user in the system by setting banned role
     * @param userId id of the user to ban
     * @throws DAOException
     */
    void banUser(long userId) throws DAOException;

    /**
     * Returns a certain user object that can then be passed as a
     * parameter and displayed on a page.
     * @param  login login of the user we are searching for
     * @return Appropriate {@link by.epam.bookrating.entity.User} instance
     * @throws DAOException
     */
    User findUserByLogin(String login) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Statistics} instances
     * for all users in the system (except admin)
     * @param from number representing start index
     * @param to number representing end index
     * @return List<Statistics> list of statistics for all users
     * @throws DAOException
     */
    List<Statistics> findUsersStatics(int from, int to) throws DAOException;

    /**
     * Returns the map of {@link by.epam.bookrating.entity.Book} as key and
     * {@link by.epam.bookrating.entity.Comment} as value for a certain user
     * if they are exists. Otherwise returns empty map.
     * @param from number representing start index
     * @param to number representing end index
     * @param  userId id of a certain user whose comments are looking for
     * @return Map<Book,Comment> map of books and comments left by user
     * @throws DAOException
     */
    Map<Comment, Book> findAllCommentsByUserId(int from, int to, long userId) throws DAOException;
}
