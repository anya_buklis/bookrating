package by.epam.bookrating.dao;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Basic DAO Interface consist of methods to deal with
 * jdbc elements (Connection, Statement)
 * return current command
 * @author Anna Buklis
 */

interface IDAO {
    Logger logger = Logger.getLogger(IDAO.class);

    /**
     * Base method. Do some action (deleting/updating) by id
     * @param  id id of entity for which action should be done
     * @param query sql query of action
     * @throws DAOException
     */
    default void doActionById(long id, String query) throws DAOException{
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
     }

    /**
     * Base method. Checks existing of certain entity for a user
     * @param userId id of user for which checking should be done
     * @param entityId id of entity for which action should be done
     * @param query sql query for checking
     * @return true, if entity for user exists
     * @throws DAOException
     */
    default boolean isEntityForUserExists(long userId, long entityId, String query) throws DAOException{
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, entityId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    /**
     * Base method. Finds total amount of certain entities
     * @param query sql query for finding entities in the db
     * @return total amount of entities
     * @throws DAOException
     */
    default int findTotalAmount(String query) throws DAOException{
        Connection connection = ConnectionPool.getInstance().takeConnection();
        int totalAmount = 0;
        try(Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()){
                totalAmount = resultSet.getInt(1);
            }
        } catch (SQLException e){
            throw new DAOException("Exception is occurred during finding total amount.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return totalAmount;
    }

    /**
     * Base method. Finds total amount of certain entities by certain id
     * @param query sql query for finding entities in the db
     * @param parameter needed for query
     * @return total amount of entities
     * @throws DAOException
     */
    default int findTotalAmountByParameter(String query, Object parameter) throws DAOException{
        Connection connection = ConnectionPool.getInstance().takeConnection();
        int totalAmount = 0;
        try(PreparedStatement statement = connection.prepareStatement(query)){
            statement.setObject(1, parameter);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                totalAmount = resultSet.getInt(1);
            }
        } catch (SQLException e){
            throw new DAOException("Exception is occurred during finding total amount.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return totalAmount;
    }

    /**
     * Returns connection to the pool if
     * it's not null
     * @param connection to return
     */
    default void returnConnectionToThePool(Connection connection){
        if (connection != null){
            ConnectionPool.getInstance().returnConnection(connection);
        }
    }
}
