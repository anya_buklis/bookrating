package by.epam.bookrating.dao;

import by.epam.bookrating.entity.Author;
import by.epam.bookrating.exception.DAOException;

import java.util.List;

/**
 * Interface consisting of methods to deal with authors<br/>
 * Implements {@link by.epam.bookrating.dao.IDAO}
 * @author Anna Buklis
 */
public interface AuthorDAO extends IDAO {

    /**
     * Adds an author's row in the db and fills it with the fields of method parameter
     * @param author {@link by.epam.bookrating.entity.Author} instance consists of author's info
     * @throws DAOException
     */
    void addAuthor(Author author) throws DAOException;

    /**
     * Adds an author's row in the db and fills it only with the full name
     * @param fullName full name of author to add
     * @return id of added author
     * @throws DAOException
     */
    long addBasicAuthorInfo(String fullName) throws DAOException;

    /**
     * Returns an author object that can then be passed as a
     * parameter and displayed on a page.
     * @param  authorId id of author in the db
     * @throws DAOException
     * @return Appropriate {@link by.epam.bookrating.entity.Author} instance
     */
    Author findAuthorById(long authorId) throws DAOException;

    /**
     * Returns list of all authors in the db
     * @param from number representing start index
     * @param to number representing end index
     * @throws DAOException
     * @return List<Author> list of {@link by.epam.bookrating.entity.Author} instances
     */
    List<Author> findAllAuthors(int from, int to) throws DAOException;

    /**
     * Returns total amount of books by author
     * @param authorId by which search should be done
     * @throws DAOException
     * @return books amount
     */
    int findAmountOfBooksByAuthor(long authorId) throws DAOException;

    /**
     * Returns total amount of authors in the db
     * @throws DAOException
     * @return total authors amount
     */
    int findAmountOfAuthors() throws DAOException;
    /**
     * Returns an author object that can then be passed as a
     * parameter and displayed on a page only if author with such name exists.
     * Otherwise method returns "empty" author object
     * @param  fullName full name of author in the db
     * @throws DAOException
     * @return Appropriate {@link by.epam.bookrating.entity.Author} instance
     */
    Author isAuthorExists(String fullName) throws DAOException;

    /**
     * Updates the author's info in the db with the author parameter fields
     * @param  author {@link by.epam.bookrating.entity.Author} object,
     * consists of fields to update
     * @throws DAOException
     */
    void updateAuthorInfo(Author author) throws DAOException;

    /**
     * Returns the list of {@link by.epam.bookrating.entity.Author} who wrote book with bookId parameter
     * @param  bookId id of book in the db authors of which are looking for
     * @return List<Author> of authors
     * @throws DAOException
     */
    List<Author> findAuthorsByBookId(long bookId) throws DAOException;

    /**
     * Deletes authors for a certain book in the db
     * @param  bookId the identifier of the book whose authors we want to remove
     * @throws DAOException
     */
    void deleteAllAuthorsForBook(long bookId) throws DAOException;

    /**
     * Deletes certain author from the db
     * @param  authorId the identifier of the author to remove
     * @throws DAOException
     */
    void deleteAuthorById(long authorId) throws DAOException;

    /**
     * Deletes all books for a certain author in the db
     * @param  authorId the identifier of the author whose books should be removed
     * @throws DAOException
     */
    void deleteAllBooksByAuthor(long authorId) throws DAOException;
}
