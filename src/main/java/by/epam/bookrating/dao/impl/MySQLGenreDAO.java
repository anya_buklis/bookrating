package by.epam.bookrating.dao.impl;

import by.epam.bookrating.dao.GenreDAO;
import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class consisting of realisation of methods to deal with genres<br/>
 * Implements {@link by.epam.bookrating.dao.GenreDAO}
 * @author Anna Buklis
 */
public class MySQLGenreDAO implements GenreDAO {
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private static final String SQL_FIND_ALL_GENRES =  "SELECT genre_id, genre_name FROM genres";

    private static final String SQL_FIND_GENRE_BY_ID =
            "SELECT genre_id, genre_name FROM genres WHERE genre_id = ?";

    private static final String SQL_GENRES_FOR_BOOK = "SELECT genres.genre_id, genres.genre_name " +
            "FROM genres JOIN genres_books ON genres_books.genre_id = genres.genre_id " +
            "WHERE genres_books.book_id = ?";

    private static final String SQL_DELETE_ALL_GENRES_FOR_BOOK =
            "DELETE FROM genres_books WHERE book_id = ?";

    public List<Genre> findAllGenres() throws DAOException {
        List<Genre> genres = new ArrayList<>();
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_GENRES)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                genres.add(new Genre(resultSet.getInt(1), resultSet.getString(2)));
            }
        } catch (SQLException e){
            throw new DAOException("Exception in method findAllGenres, DAO layer",e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return genres;
    }

    public Genre findGenreById(int genreId) throws DAOException {
        Genre genre = new Genre();
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_GENRE_BY_ID)){
            preparedStatement.setInt(1, genreId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                genre.setGenreId(resultSet.getInt(1));
                genre.setGenreName(resultSet.getString(2));
            }
        } catch (SQLException e){
            throw new DAOException("Exception in method findAllGenres, DAO layer",e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return genre;
    }

    public List<Genre> findGenresByBookId(long bookId) throws DAOException {
        List<Genre> genres = new ArrayList<>();
        Connection connection =  pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_GENRES_FOR_BOOK)){
            preparedStatement.setLong(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                genres.add(new Genre(resultSet.getInt(1), resultSet.getString(2)));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception occurred during finding genres to a book.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return genres;
    }

    public void deleteAllGenresForBook(long bookId) throws DAOException {
        doActionById(bookId, SQL_DELETE_ALL_GENRES_FOR_BOOK);
    }

}
