package by.epam.bookrating.dao.impl;

import by.epam.bookrating.dao.AuthorDAO;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class consisting of realisation of methods to deal with authors<br/>
 * Implements {@link by.epam.bookrating.dao.AuthorDAO}
 * @author Anna Buklis
 */
public class MySQLAuthorDAO implements AuthorDAO {
    private static Logger logger = Logger.getLogger(MySQLAuthorDAO.class);
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private static final String SQL_EDIT_AUTHOR_INFO =
            "UPDATE authors SET full_name=? , birth_year=? , birth_country=?, " +
                    "biography=?, image_url =? WHERE author_id=?";

    private static final String SQL_FIND_AUTHORS_FOR_BOOK = "SELECT authors.author_id, authors.full_name, " +
            "authors.birth_year, authors.birth_country, authors.biography, authors.image_url " +
            "FROM authors JOIN authors_books ON authors_books.author_id = authors.author_id " +
            "WHERE authors_books.book_id = ?";

    private static final String SQL_ADD_AUTHOR =
            "INSERT INTO authors(full_name, birth_year, birth_country, biography, image_url) VALUES (?,?,?,?, ?)";

    private static final String SQL_FIND_AUTHOR_BY_ID = "SELECT author_id, full_name, birth_year, birth_country, " +
            "biography, image_url FROM authors WHERE author_id = ?";

    private static final String SQL_FIND_AUTHOR_BY_FULLNAME = "SELECT author_id, full_name, birth_year, birth_country, " +
            "biography, image_url FROM authors WHERE UPPER(full_name) LIKE UPPER(?)";

    private static final String SQL_ADD_BASIC_AUTHOR_INFO = "INSERT INTO authors(full_name) VALUES (?)";

    private static final String SQL_DELETE_ALL_AUTHORS_FOR_BOOK =
            "DELETE FROM authors_books WHERE BOOK_ID = ?";

    private static final String SQL_FIND_AUTHORS_PORTION = "SELECT author_id, full_name, birth_year, birth_country " +
            " FROM authors LIMIT ? , ?";

    private static final String SQL_DELETE_AUTHOR_BY_ID = "DELETE FROM authors WHERE author_id = ?";

    private static final String SQL_DELETE_BOOKS_BY_AUTHOR = "DELETE FROM books where book_id in (" +
            "select book_id from authors_books where author_id = ?);";

    private static final String SQL_FIND_AMOUNT_OF_AUTHORS = "SELECT count(*) from authors";

    private static final String SQL_FIND_BOOKS_AMOUNT_FOR_AUTHOR = "SELECT count(*) FROM books JOIN authors_books" +
            " ON books.book_id=authors_books.book_id " +
            "WHERE authors_books.author_id = ?";

    public void addAuthor(Author author) throws DAOException {
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_AUTHOR)){
            preparedStatement.setString(1, author.getFullName());
            preparedStatement.setInt(2, author.getBirthYear());
            preparedStatement.setString(3, author.getBirthCountry());
            preparedStatement.setString(4, author.getBiography());
            preparedStatement.setString(5, author.getImageUrl());
            preparedStatement.executeUpdate();
            logger.info("Author successfully added.");
        } catch (SQLException e) {
            throw new DAOException("Exception occurred during adding author.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public long addBasicAuthorInfo(String fullName) throws DAOException {
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SQL_ADD_BASIC_AUTHOR_INFO, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1, fullName);
            preparedStatement.executeUpdate();
            logger.info("Author with name " + fullName+" added to a database.");
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            return rs.getLong(1);
        } catch (SQLException e) {
            throw new DAOException("Exception occurred during adding author.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public Author findAuthorById(long authorId) throws DAOException {
        Connection connection = pool.takeConnection();
        Author author = new Author();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_AUTHOR_BY_ID)) {
            logger.info("Finding author with id =" + authorId + " in db.");
            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                author = new Author(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getInt(3), resultSet.getString(4),
                        resultSet.getString(5), resultSet.getString(6));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding author by id.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return author;
    }

    public List<Author> findAllAuthors(int from, int to) throws DAOException {
        List<Author> authors = new ArrayList<>();
        Author author;
        Connection connection = pool.takeConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_AUTHORS_PORTION)){
            statement.setInt(1, from);
            statement.setInt(2, to);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                author = new Author(resultSet.getLong(1), resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getString(4));
                authors.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in method findAllBooks", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return authors;
    }

    public int findAmountOfAuthors() throws DAOException {
        return findTotalAmount(SQL_FIND_AMOUNT_OF_AUTHORS);
    }

    public Author isAuthorExists(String fullName) throws DAOException {
        Connection connection = pool.takeConnection();
        Author author = new Author();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_AUTHOR_BY_FULLNAME)){
            preparedStatement.setString(1, fullName);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                author = new Author(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getInt(3), resultSet.getString(4),
                        resultSet.getString(5), resultSet.getString(6));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding author by name.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return author;
    }

    public List<Author> findAuthorsByBookId(long bookId) throws DAOException {
        Connection connection = pool.takeConnection();
        List<Author> authors = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_AUTHORS_FOR_BOOK)){
            preparedStatement.setLong(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                long authorId = resultSet.getLong(1);
                authors.add(new Author(authorId, resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getString(4), resultSet.getString(5), resultSet.getString(6)));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception occurred during findAuthorIdByBookId to a book.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return authors;
    }

    public void updateAuthorInfo(Author author) throws DAOException {
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_EDIT_AUTHOR_INFO)){
            preparedStatement.setString(1, author.getFullName());
            preparedStatement.setInt(2, author.getBirthYear());
            preparedStatement.setString(3, author.getBirthCountry());
            preparedStatement.setString(4, author.getBiography());
            preparedStatement.setString(5, author.getImageUrl());
            preparedStatement.setLong(6, author.getAuthorId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception occurred during updating author.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public void deleteAllAuthorsForBook(long bookId) throws DAOException {
        doActionById(bookId, SQL_DELETE_ALL_AUTHORS_FOR_BOOK);
    }

    public void deleteAuthorById(long authorId) throws DAOException{
        doActionById(authorId, SQL_DELETE_AUTHOR_BY_ID);
    }

    public void deleteAllBooksByAuthor(long authorId) throws DAOException{
        doActionById(authorId, SQL_DELETE_BOOKS_BY_AUTHOR);
    }

    public int findAmountOfBooksByAuthor(long authorId) throws DAOException{
        return findTotalAmountByParameter(SQL_FIND_BOOKS_AMOUNT_FOR_AUTHOR, authorId);
    }
}
