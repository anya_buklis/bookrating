package by.epam.bookrating.dao.impl;

import by.epam.bookrating.dao.AuthorDAO;
import by.epam.bookrating.dao.BookDAO;
import by.epam.bookrating.dao.GenreDAO;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class consisting of realisation of methods to deal with books<br/>
 * Implements {@link by.epam.bookrating.dao.BookDAO}
 * @author Anna Buklis
 */

public class MySQLBookDAO implements BookDAO {
    private static Logger logger = Logger.getLogger(MySQLBookDAO.class);
    private static final ConnectionPool pool = ConnectionPool.getInstance();
    private static final String PERCENT = "%";
    private static final AuthorDAO AUTHOR_DAO = new MySQLAuthorDAO();
    private static final GenreDAO GENRE_DAO = new MySQLGenreDAO();

    private static final String SQL_FIND_AMOUNT_OF_BOOKS =
            "SELECT count(*) from books";

    private static final String SQL_DELETE_BOOK_FROM_FAVORITE =
            "DELETE FROM favorite_books WHERE user_id=? AND book_id=?";

    private static final String SQL_DELETE_BOOK_FROM_READ =
            "DELETE FROM read_books WHERE user_id=? AND book_id=?";

    private static final String SQL_ADD_BASIC_BOOK_INFO =
            "INSERT INTO books(title, description, publishing_year, image_url) values (?,?,?,?)";

    private static final String SQL_FIND_BOOK_BY_ID = "SELECT book_id, title, description, publishing_year, " +
            " image_url FROM books WHERE book_id = ?";

    private static final String SQL_FIND_POPULAR_BOOKS =
            "SELECT rating.book_id, ROUND((select avg(r2.rating) from rating as `r2`" +
                    "where rating.book_id=r2.book_id), 1) as avgRating " +
                    "FROM rating GROUP by rating.book_id " +
                    "ORDER by avgRating DESC limit 3";

    private static final String SQL_FIND_BOOKS_BY_AUTHOR_ID =
            "SELECT books.book_id, books.title, books.description, books.publishing_year, " +
                    "books.image_url FROM books JOIN authors_books ON books.book_id=authors_books.book_id " +
                    "WHERE authors_books.author_id =? LIMIT ? , ?";

    private static final String SQL_FIND_BOOKS_BY_GENRE_ID =
            "SELECT books.book_id, books.title, books.description, books.publishing_year, " +
                    "books.image_url FROM books JOIN genres_books ON books.book_id=genres_books.BOOK_ID " +
                    "WHERE genres_books.genre_id =? LIMIT  ? , ?";

    private static final String SQL_FIND_BOOKS_BY_TITLE =
            "SELECT books.book_id, books.title, books.description, books.publishing_year, " +
                    "books.image_url FROM books WHERE UPPER(books.title) LIKE UPPER(?) LIMIT ? , ?";

    private static final String SQL_FIND_AMOUNT_OF_BOOKS_BY_TITLE =
            "SELECT COUNT(*) FROM books WHERE UPPER(books.title) LIKE UPPER(?)";

    private static final String SQL_FIND_AMOUNT_OF_BOOKS_BY_GENRE =
            "SELECT count(*) FROM books JOIN genres_books ON books.book_id=genres_books.book_id " +
                    "WHERE genres_books.genre_id =?";

    private static final String SQL_DELETE_BOOK_BY_ID = "DELETE FROM books WHERE book_id = ?";

    private static final String SQL_UPDATE_BOOK_ITEM =
            "UPDATE books SET title=? , description=? , publishing_year=?, image_url =? WHERE book_id=?";

    private static final String SQL_IS_BOOK_BY_THIS_AUTHOR_EXISTS =
            "SELECT book_id, author_id FROM authors_books WHERE author_id = ? AND book_id=?";

    private static final String SQL_FIND_LIST_OF_FAVORITE_BOOKS =
            "SELECT books.book_id, books.title, books.description, books.publishing_year, books.image_url " +
                    "FROM books JOIN favorite_books ON books.book_id=favorite_books.book_id " +
                    "WHERE favorite_books.user_id = ? LIMIT ? , ?";

    private static final String SQL_FIND_LIST_OF_READ_BOOKS =
            "SELECT books.book_id, books.title, books.description, books.publishing_year, " +
                    "books.image_url FROM books JOIN read_books ON books.book_id=read_books.book_id " +
                    "WHERE read_books.user_id = ? LIMIT ? , ?";

    private static final String SQL_FIND_AMOUNT_OF_READ_BOOKS =
            "SELECT COUNT(*) FROM books JOIN read_books ON books.book_id=read_books.book_id " +
                    "WHERE read_books.user_id = ? ";

    private static final String SQL_FIND_AMOUNT_OF_FAVORITE_BOOKS =
            "SELECT COUNT(*) FROM books JOIN favorite_books ON books.book_id=favorite_books.book_id"+
                " WHERE favorite_books.user_id = ?";

    private static final String SQL_ADD_BOOK_TO_FAVORITE =
            "INSERT INTO favorite_books(user_id, book_id) VALUES (?,?)";

    private static final String SQL_ADD_BOOK_TO_READ =
            "INSERT INTO read_books(user_id, book_id) VALUES (?,?)";

    private static final String SQL_IS_BOOK_IS_IN_READ =
            "SELECT user_id, book_id FROM read_books WHERE user_id=? AND book_id=?";

    private static final String SQL_IS_BOOK_IS_IN_FAVORITE =
            "SELECT user_id, book_id FROM favorite_books WHERE user_id=? AND book_id=?";

    private static final String SQL_ADD_GENRE_TO_THE_BOOK =
            "INSERT INTO genres_books(genre_id, book_id) VALUES (?,?)";

    private static final String SQL_ADD_AUTHOR_TO_BOOK =
            "INSERT INTO authors_books(author_id, book_id) VALUES (?,?)";

    private static final String SQL_FIND_BOOKS_PORTION = "SELECT book_id, title, description, publishing_year, " +
            "image_url FROM books LIMIT ? , ?";

    private static final String SQL_FIND_BOOK_BY_TITLE = "SELECT book_id, title, description, publishing_year, " +
            " image_url FROM books WHERE UPPER(title) LIKE UPPER(?) ";

    private static final int POPULAR_BOOKS_SIZE = 3;

    public List<Book> findAllBooks(int from, int to) throws DAOException {
        List<Book> books = new ArrayList<>();
        Book book;
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BOOKS_PORTION)){
            preparedStatement.setInt(1, from);
            preparedStatement.setInt(2, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                book = new Book(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getInt(4),resultSet.getString(5));
                book.setAuthors(new MySQLAuthorDAO().findAuthorsByBookId(resultSet.getLong(1)));
                book.setGenres(new MySQLGenreDAO().findGenresByBookId(resultSet.getLong(1)));
                books.add(book);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in method findAllBooks", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return books;
    }

    public int findAmountOfBooks() throws DAOException {
        return findTotalAmount(SQL_FIND_AMOUNT_OF_BOOKS);
    }

    public long addBasicBookInfo(Book book) throws DAOException{
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_BASIC_BOOK_INFO,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getDescription());
            preparedStatement.setInt(3, book.getPublishingYear());
            preparedStatement.setString(4, book.getImageUrl());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            long bookId = rs.getLong(1);
            logger.info("Basic book info successfully added. Id = " + bookId);
            return bookId;
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during adding book.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public Book isBookExists(String title) throws DAOException {
        Book book = new Book();
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BOOK_BY_TITLE)) {
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                book = new Book(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getInt(4), resultSet.getString(5));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding book by title.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return book;
    }

    public Book findBookById(long bookId) throws DAOException {
        Book book = new Book();
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BOOK_BY_ID)){
            preparedStatement.setLong(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                book = new Book(resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getInt(4),
                        resultSet.getString(5));
                book.setAuthors(AUTHOR_DAO.findAuthorsByBookId(bookId));
                book.setGenres(GENRE_DAO.findGenresByBookId(bookId));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding book.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return book;
    }

    public void deleteBookById(long bookId) throws DAOException{
        doActionById(bookId, SQL_DELETE_BOOK_BY_ID);
    }

    public int findAmountOfBooksByTitle(String title) throws DAOException{
        return findTotalAmountByParameter(SQL_FIND_AMOUNT_OF_BOOKS_BY_TITLE,
                PERCENT + title + PERCENT);
    }

    public List<Book> findBooksByTitle(int from, int to, String title) throws DAOException {
        Connection connection = pool.takeConnection();
        List<Book> books = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BOOKS_BY_TITLE)){
            preparedStatement.setString(1, PERCENT + title + PERCENT);
            preparedStatement.setInt(2, from);
            preparedStatement.setInt(3, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                long bookId = resultSet.getLong(1);
                String bookTitle = resultSet.getString(2);
                String description = resultSet.getString(3);
                int publishingYear = resultSet.getInt(4);
                String imageUrl = resultSet.getString(5);

                Book book = new Book(bookId, bookTitle, description, publishingYear, imageUrl);
                List<Author> authors = new MySQLAuthorDAO().findAuthorsByBookId(bookId);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (SQLException e){
            throw new DAOException("Exception is occurred during finding books by title.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return books;
    }


    public void updateBookItem(Book book) throws DAOException{
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_BOOK_ITEM)){
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getDescription());
            preparedStatement.setInt(3, book.getPublishingYear());
            preparedStatement.setString(4, book.getImageUrl());
            preparedStatement.setLong(5, book.getBookId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during updating book.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public Map<Book, Double> findBooksWithHighRating() throws DAOException {
        Connection connection = pool.takeConnection();
        Map<Book,Double> booksWithAvgRatings = new LinkedHashMap<>(POPULAR_BOOKS_SIZE);
        logger.info("size " + booksWithAvgRatings.size());
        try(Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(SQL_FIND_POPULAR_BOOKS);
            while (resultSet.next()){
                long bookId = resultSet.getLong(1);
                double avgRating = resultSet.getDouble(2);
                booksWithAvgRatings.put(findBookById(bookId), avgRating);
            }
        } catch (SQLException| DAOException e){
            throw new DAOException("Exception is occurred in addGenreToBook method.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return booksWithAvgRatings;
    }

    public List<Book> findBooksByGenre(int from, int to, int genreId) throws DAOException {
        Connection connection = pool.takeConnection();
        List<Book> books = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BOOKS_BY_GENRE_ID)){
            preparedStatement.setInt(1, genreId);
            preparedStatement.setInt(2, from);
            preparedStatement.setInt(3, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                long bookId = resultSet.getLong(1);
                String bookTitle = resultSet.getString(2);
                String description = resultSet.getString(3);
                int publishingYear = resultSet.getInt(4);
                String imageUrl = resultSet.getString(5);

                Book book = new Book(bookId, bookTitle, description, publishingYear, imageUrl);
                List<Author> authors = new MySQLAuthorDAO().findAuthorsByBookId(bookId);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (SQLException e){
            throw new DAOException("Exception is occurred during finding books by title.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return books;
    }

    public int findAmountOfBooksByGenre(int genreId) throws DAOException{
        return findTotalAmountByParameter(SQL_FIND_AMOUNT_OF_BOOKS_BY_GENRE,
                genreId);
    }

    public List<Book> findBooksByAuthor(int from, int to, long authorId) throws DAOException {
        Connection connection = pool.takeConnection();
        List<Book> books = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BOOKS_BY_AUTHOR_ID)){
            preparedStatement.setLong(1, authorId);
            preparedStatement.setLong(2, from);
            preparedStatement.setLong(3, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Book book = new Book(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getInt(4),resultSet.getString(5));
                book.setAuthors(AUTHOR_DAO.findAuthorsByBookId(resultSet.getLong(1)));
                book.setGenres(GENRE_DAO.findGenresByBookId(resultSet.getLong(1)));
                books.add(book);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding books by parameter", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return books;
    }

    public void addToFavorite(long userId, long bookId) throws DAOException {
        addBookTo(userId, bookId,SQL_ADD_BOOK_TO_FAVORITE);
    }

    public void addToRead(long userId, long bookId) throws DAOException {
        addBookTo(userId, bookId, SQL_ADD_BOOK_TO_READ);
    }

    public boolean isBookInFavorite(long userId, long bookId) throws DAOException{
        return isBookHasAlreadyAdded(userId, bookId, SQL_IS_BOOK_IS_IN_FAVORITE);
    }

    public boolean isBookInRead(long userId, long bookId) throws DAOException{
        return isBookHasAlreadyAdded(userId, bookId, SQL_IS_BOOK_IS_IN_READ);
    }

    public int findAmountOfReadBooks(long userId) throws DAOException{
        int amount = findTotalAmountByParameter(SQL_FIND_AMOUNT_OF_READ_BOOKS,
                userId);
        logger.info(amount + " read books found.");
        return amount;
    }

    public List<Book> findReadBooks(int from, int to, long userId) throws DAOException {
        logger.info("Finding read books...");
        return findBooksForUser(from, to, userId, SQL_FIND_LIST_OF_READ_BOOKS);
    }

    public int findAmountOfFavoriteBooks(long userId) throws DAOException{
        int amount = findTotalAmountByParameter(SQL_FIND_AMOUNT_OF_FAVORITE_BOOKS,
                userId);
        logger.info(amount + " favorite books found.");
        return amount;
    }

    public List<Book> findFavoriteBooks(int from, int to, long userId) throws DAOException {
        logger.info("Finding favorite books...");
        return findBooksForUser(from, to, userId, SQL_FIND_LIST_OF_FAVORITE_BOOKS);
    }

    public void addAuthorToBook(long authorId, long bookId) throws DAOException{
        addGenreOrAuthorToBook(authorId, bookId, SQL_ADD_AUTHOR_TO_BOOK);
    }

    public void addGenreToBook(int genreId, long bookId) throws DAOException{
        addGenreOrAuthorToBook(genreId, bookId, SQL_ADD_GENRE_TO_THE_BOOK);
    }

    public void deleteBookFromRead(long userId, long bookId) throws DAOException {
        try {
            deleteBookFrom(userId, bookId, SQL_DELETE_BOOK_FROM_READ);
        } catch (DAOException e) {
            throw new DAOException("Exception in method deleteBookFromRead", e);
        }
    }

    public void deleteBookFromFavorite(long userId, long bookId) throws DAOException {
        try {
            deleteBookFrom(userId, bookId, SQL_DELETE_BOOK_FROM_FAVORITE);
        } catch (DAOException e) {
            throw new DAOException("Exception in method deleteBookFromFavorite", e);
        }
    }

    public boolean isBookByThisAuthorAlreadyExists(long authorId, long bookId) throws DAOException{
        return isBookHasAlreadyAdded(authorId, bookId,SQL_IS_BOOK_BY_THIS_AUTHOR_EXISTS);
    }

    private void addGenreOrAuthorToBook(long genreOrAuthorId, long bookId, String query) throws DAOException {
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setLong(1, genreOrAuthorId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.executeUpdate();
            logger.info("Author " + genreOrAuthorId + " added to a book");
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred in addGenreToBook method.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    private List<Book> findBooksForUser(int from, int to, long id, String query) throws DAOException {
        Connection connection = pool.takeConnection();
        List<Book> books = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setLong(1, id);
            preparedStatement.setLong(2, from);
            preparedStatement.setLong(3, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Book book = new Book(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getInt(4),resultSet.getString(5));
                book.setAuthors(AUTHOR_DAO.findAuthorsByBookId(resultSet.getLong(1)));
                book.setGenres(GENRE_DAO.findGenresByBookId(resultSet.getLong(1)));
                books.add(book);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding books by parameter", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return books;
    }

    private void deleteBookFrom(long userId, long bookId, String query) throws DAOException {
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.executeUpdate();
            logger.info("Book with id = " + bookId + " deleted from favorite/read by user "+ userId);
        } catch (SQLException e){
            throw new DAOException("Exception in deleteFrom method.", e);
        }
        finally {
            returnConnectionToThePool(connection);
        }
    }

    private void addBookTo(long userId, long bookId, String addingQuery) throws DAOException {
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(addingQuery)){
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.executeUpdate();
            logger.info("Added.");
        } catch (SQLException e){
            throw new DAOException("Exception is occurred during adding to favorite in DAO layer.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    private boolean isBookHasAlreadyAdded(long userId, long bookId, String checkQuery) throws DAOException {
        return isEntityForUserExists(userId, bookId, checkQuery);
    }
}
