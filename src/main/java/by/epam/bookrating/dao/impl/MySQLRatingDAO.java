package by.epam.bookrating.dao.impl;

import by.epam.bookrating.dao.RatingDAO;
import by.epam.bookrating.entity.Rating;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class consisting of realisation of methods to deal with ratings<br/>
 * Implements {@link by.epam.bookrating.dao.RatingDAO}
 * @author Anna Buklis
 */
public class MySQLRatingDAO implements RatingDAO {
    private static Logger logger = Logger.getLogger(MySQLRatingDAO.class);
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private static final String SQL_LEAVE_RATING =
            "INSERT INTO rating(user_id, book_id, rating) VALUES (?,?,?)";

    private static final String SQL_FIND_AVG_STAR =
            "SELECT ROUND(AVG(rating),1) FROM rating WHERE book_id=?";

    private static final String SQL_DELETE_RATING =
            "DELETE FROM rating WHERE user_id=? and book_id =?";

    private static final String SQL_IS_USER_LEFT_RATING =
            "SELECT user_id, book_id, rating FROM rating WHERE user_id= ? AND book_id= ?";

    private static final String SQL_FIND_RATING_BY_USERID_AND_BOOKID =
            "SELECT rating_id, user_id, book_id, rating FROM RATING " +
                    "WHERE user_id= ? AND book_id= ?";

    public void addRating(long userId, long bookId, int rating) throws DAOException {
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LEAVE_RATING)){
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.setInt(3, rating);
            preparedStatement.executeUpdate();
            logger.info("Rating successfully added to a db.");
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during leaving rating.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public boolean isRatingByThisUserExists(long userId, long bookId) throws DAOException {
        return isEntityForUserExists(userId, bookId, SQL_IS_USER_LEFT_RATING);
    }

    public Rating findRatingByUserIdAndBookId(long bookId, long userId) throws DAOException {
        Connection connection = pool.takeConnection();
        Rating rating = new Rating();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_RATING_BY_USERID_AND_BOOKID)){
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                rating.setRatingId(resultSet.getLong(1));
                rating.setUserId(resultSet.getLong(2));
                rating.setBookId(resultSet.getLong(3));
                rating.setRating(resultSet.getInt(4));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during leaving rating.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return rating;
    }

    public double findAvgRatingByBookId(long bookId) throws DAOException {
        Connection connection = pool.takeConnection();
        double avgRating = 0;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_AVG_STAR)){
            preparedStatement.setLong(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                avgRating = resultSet.getDouble(1);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during leaving rating.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return avgRating;
    }

    public void deleteRating(long userId, long bookId) throws DAOException {
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_RATING)){
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.executeUpdate();
            logger.info("Rating with userId" + userId + ", bookId=" + bookId + " deleted.");
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during leaving rating.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }
}
