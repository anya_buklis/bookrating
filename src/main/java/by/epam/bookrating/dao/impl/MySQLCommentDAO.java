package by.epam.bookrating.dao.impl;

import by.epam.bookrating.dao.CommentDAO;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class consisting of realisation of methods to deal with comments<br/>
 * Implements {@link by.epam.bookrating.dao.CommentDAO}
 * @author Anna Buklis
 */
public class MySQLCommentDAO implements CommentDAO {
    private static Logger logger = Logger.getLogger(MySQLCommentDAO.class);
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private static final String SQL_ADD_COMMENT_TO_THE_BOOK =
            "INSERT INTO comments(book_id, user_id, comment_date, comment_text) VALUES (?,?,?,?)";

    private static final String SQL_FIND_ALL_COMMENTS_BY_BOOKID =
            "SELECT comments.comment_id, comments.book_id, comments.user_id, comments.comment_date, " +
                    "comments.comment_text, users.login , users.name, users.age, users.info, users.avatar, " +
                    "role FROM comments JOIN users on users.user_id = comments.user_id " +
                    "WHERE comments.book_id = ?";

    private static final String SQL_FIND_COMMENTS_BY_DATE =
            "SELECT comments.comment_id, comments.book_id, comments.user_id, comments.comment_date, " +
                    "comments.comment_text, users.login as login FROM comments JOIN users" +
                    " on comments.user_id = users.user_id " +
                    "WHERE comments.comment_date = ?";

    private static final String SQL_FIND_COMMENTS_BY_USER_ID =
            "SELECT comment_id, book_id, user_id, comment_date, comment_text " +
                    "FROM comments WHERE user_id = ? ";

    private static final String SQL_FIND_AMOUNT_OF_COMMENTS_BY_USER_ID =
            "SELECT COUNT(*) FROM comments WHERE user_id = ? ";

    private static final String SQL_DELETE_COMMENT_BY_ID =
            "DELETE FROM comments WHERE comment_id = ?";

    public void addComment(Comment comment)
            throws DAOException {
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_COMMENT_TO_THE_BOOK)){
            preparedStatement.setLong(1, comment.getBookId());
            preparedStatement.setLong(2, comment.getUserId());
            preparedStatement.setDate(3, comment.getCommentDate());
            preparedStatement.setString(4, comment.getCommentText());
            preparedStatement.executeUpdate();
            logger.info("Comment added to a db.");
            } catch (SQLException e) {
            throw new DAOException("Exception is occurred during adding comment.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public Map<Comment, User> findAllCommentsByBookId(long bookId) throws DAOException {
        Map<Comment, User> comments = new LinkedHashMap<>();
        Connection connection = pool.takeConnection();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_COMMENTS_BY_BOOKID);
            preparedStatement.setLong(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long userId = resultSet.getLong(3);
                Date date = Date.valueOf(resultSet.getString(4));
                Comment comment = new Comment(resultSet.getLong(1),resultSet.getLong(2),
                        userId, date, resultSet.getString(5));
                User user = new User(userId, resultSet.getString(6), resultSet.getString(7),
                        resultSet.getInt(8), resultSet.getString(9));
                comments.put(comment, user);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in method findAllBooks", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return comments;
    }

    public void deleteComment(long commentId) throws DAOException{
        doActionById(commentId, SQL_DELETE_COMMENT_BY_ID);
    }

    public Map<Comment, String> findTodayComments(Date currentDate) throws DAOException {
        Map<Comment, String> commentsWithLogin = new LinkedHashMap<>();
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_COMMENTS_BY_DATE)){
            preparedStatement.setString(1, currentDate.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Date date = Date.valueOf(resultSet.getString(4));
                Comment comment = new Comment(resultSet.getLong(1),resultSet.getLong(2),
                        resultSet.getLong(3), date, resultSet.getString(5));
                String login = resultSet.getString(6);
                commentsWithLogin.put(comment, login);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding today's comments.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return commentsWithLogin;
    }

    public List<Comment> findCommentsByUserId(long userId) throws DAOException {
        List<Comment> comments = new ArrayList<>();
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_COMMENTS_BY_USER_ID)) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Date date = Date.valueOf(resultSet.getString(4));
                Comment comment = new Comment(resultSet.getLong(1),resultSet.getLong(2),
                        resultSet.getLong(3), date, resultSet.getString(5));
                comments.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding user comments", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return comments;
    }

    public int findCommentsAmountByUser(long userId) throws DAOException{
        return findTotalAmountByParameter(SQL_FIND_AMOUNT_OF_COMMENTS_BY_USER_ID, userId);
    }
}
