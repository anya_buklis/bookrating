package by.epam.bookrating.dao.impl;

import by.epam.bookrating.dao.UserDAO;
import by.epam.bookrating.entity.*;
import by.epam.bookrating.exception.DAOException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class consisting of realisation of methods to deal with users<br/>
 * Implements {@link by.epam.bookrating.dao.UserDAO}
 * @author Anna Buklis
 */
public class MySQLUserDAO implements UserDAO {
    private static Logger logger = Logger.getLogger(MySQLUserDAO.class);
    private static final ConnectionPool pool = ConnectionPool.getInstance();

    private static final String SQL_FIND_COMMENTS_AND_BOOKS_BY_USER_ID =
            "SELECT comments.comment_id, comments.book_id, comments.comment_date, comments.comment_text," +
                    " books.title, books.publishing_year, books.description, books.image_url " +
                    "FROM comments JOIN books on books.book_id=comments.book_id " +
                    "WHERE comments.user_id = ? LIMIT ?, ?";

    private static final String SQL_ADD_USER =
            "INSERT INTO users (login, password, name) VALUES (?,?,?)";

    private static final String SQL_IF_USER_EXISTS =
            "SELECT login FROM users WHERE login = ?";

    private static final String SQL_UPDATE_PROFILE =
            "UPDATE users SET name=? , age=? , info=? WHERE user_id = ?";

    private static final String SQL_UPDATE_AVATAR =
            "UPDATE users SET avatar = ? WHERE user_id =?";

    private static final String SQL_BAN_USER = "UPDATE users SET role = 'banned' WHERE user_id = ?";

    private static final String SQL_UNLOCK_USER = "UPDATE users SET role = 'user' WHERE user_id = ?";

    private static final String SQL_FIND_USER_INFO_BY_USERID =
            "SELECT user_id, login , password, name, age, info, avatar, role FROM users WHERE user_id = ?";

    private static final String SQL_FIND_USER_INFO_BY_LOGIN =
            "SELECT user_id, login , password, name, age, info, avatar, role FROM users WHERE login = ?";

    private static final String SQL_FIND_USER_STATISTICS =
            "SELECT users.user_id, users.name, users.login, users.age, users.role," +
                    "(SELECT count(*) from comments where user_id=users.user_id) as 'commentsAmount'," +
                    "(SELECT count(*) from rating where user_id=users.user_id) as 'ratingAmount'" +
                    "FROM users WHERE users.role!='admin' LIMIT ?, ? ";

    private static final String SQL_FIND_AMOUNT_OF_USERS = "SELECT count(*) from users WHERE users.role != 'admin'";

    public void addUser(String login, String password, String name) throws DAOException {
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_USER)){
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, name);
            preparedStatement.executeUpdate();
            logger.info("User successfully added to a db.");
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during adding user to a db.", e);
        }finally {
            returnConnectionToThePool(connection);
        }
    }

    public User findUserByUserId(long userId) throws DAOException{
        return findUserByParameter(userId, SQL_FIND_USER_INFO_BY_USERID);
    }

    public User findUserByLogin(String login) throws DAOException{
        return findUserByParameter(login, SQL_FIND_USER_INFO_BY_LOGIN);
    }

    public List<Statistics> findUsersStatics(int from, int to) throws DAOException {
        List<Statistics> statisticsList = new ArrayList<>();
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_USER_STATISTICS)){
            preparedStatement.setInt(1, from);
            preparedStatement.setInt(2, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Statistics statistics = new Statistics(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getInt(6),
                        resultSet.getInt(7));
                statisticsList.add(statistics);
            }
        } catch (SQLException e){
            throw new DAOException("Exception is occurred in method isLoginExists in UserDAO.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return statisticsList;
    }

    public boolean isLoginExists(String login) throws DAOException{
        Connection connection = pool.takeConnection();
        boolean ifUserExists = false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_IF_USER_EXISTS)){
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                ifUserExists = true;
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred in method isLoginExists in UserDAO.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return ifUserExists;
    }

    public Map<Comment, Book> findAllCommentsByUserId(int from, int to, long userId) throws DAOException{
        Map<Comment, Book> comments = new LinkedHashMap<>();
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_COMMENTS_AND_BOOKS_BY_USER_ID)){
            preparedStatement.setLong(1, userId);
            preparedStatement.setInt(2, from);
            preparedStatement.setInt(3, to);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                long commentId = resultSet.getLong(1);
                long bookId = resultSet.getLong(2);
                String commentText = resultSet.getString(4);
                String title = resultSet.getString(5);
                int publishingYear = resultSet.getInt(6);
                String description = resultSet.getString(7);
                String imageUrl = resultSet.getString(8);
                Book book = new Book(bookId, title, description, publishingYear, imageUrl);
                List<Author> authors = new MySQLAuthorDAO().findAuthorsByBookId(bookId);
                book.setAuthors(authors);
                Comment comment = new Comment(commentId, bookId, userId, resultSet.getDate(3), commentText);
                comments.put(comment, book);
            }
        } catch (SQLException e){
            throw new DAOException("Exception is occurred during finding user comments in dao layer.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return comments;
    }


    public void updateProfileInformation(String name, int age, String info, long userId)
            throws DAOException{
        Connection connection = pool.takeConnection();;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PROFILE)){
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, age);
            preparedStatement.setString(3, info);
            preparedStatement.setLong(4, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during updating profile in dao layer.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    public void updateAvatar(long userId, String imageUrl) throws DAOException {
        updateUserInfoElement(imageUrl, userId, SQL_UPDATE_AVATAR);
    }

    public void unlockUser(long userId) throws DAOException{
        lockOrUnlockUser(userId, SQL_UNLOCK_USER);
    }

    public void banUser(long userId) throws DAOException{
       lockOrUnlockUser(userId, SQL_BAN_USER);
    }

    public int findAmountOfUsers() throws DAOException {
        return findTotalAmount(SQL_FIND_AMOUNT_OF_USERS);
    }

    private void updateUserInfoElement(String value, long userId, String query) throws DAOException{
        Connection connection = pool.takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setString(1, value);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
            logger.info("User's info element updated.");
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during updating user info element.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
    }

    private void lockOrUnlockUser(long userId, String query) throws DAOException{
        doActionById(userId, query);
    }

    private User findUserByParameter(Object parameter, String query) throws DAOException{
        User user = new User();
        Connection connection = pool.takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setObject(1, parameter);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                user.setUserId(resultSet.getLong(1));
                user.setLogin(resultSet.getString(2));
                user.setPassword(resultSet.getString(3));
                user.setName(resultSet.getString(4));
                user.setAge(resultSet.getInt(5));
                user.setInfo(resultSet.getString(6));
                user.setAvatar(resultSet.getString(7));
                user.setRole(resultSet.getString(8));
            }
        } catch (SQLException e) {
            throw new DAOException("Exception is occurred during finding user.", e);
        } finally {
            returnConnectionToThePool(connection);
        }
        return user;
    }
}
