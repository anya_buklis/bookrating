package by.epam.bookrating.command;

/**
 * Class consists of names of attributes to send/to get
 * from jsp and jsp-paths
 * @author Anna Buklis
 */

public class Constant {
    public static final String PAGES_AMOUNT = "pagesAmount";
    public static final String AUTHOR = "author";
    public static final String TITLE = "title";
    public static final String FULL_NAME = "fullName";
    public static final String BIRTH_YEAR = "birthYear";
    public static final String BIRTH_COUNTRY = "birthCountry";
    public static final String BIOGRAPHY = "biography";
    public static final String BOOK_ID = "bookId";
    public static final String ENTITY = "entity";
    public static final String BOOK = "book";
    public static final String PREVIOUS_PAGE = "previousPage";
    public static final String BOOK_LIST = "books";
    public static final String QUERY = "query";
    public static final String DESCRIPTION = "description";
    public static final String PUBLISHING_YEAR =  "publishingYear";
    public static final String GENRE = "genre";
    public static final String IMAGE_URL = "imageUrl";
    public static final String BOOK_ITEM = "book";
    public static final String NEW_IMAGE_URL = "newImage";
    public static final String OLD_IMAGE_URL = "oldImage";
    public static final String OLD_TITLE = "oldTitle";
    public static final String NAME = "name";
    public static final String AGE = "age";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String INFO = "info";
    public static final String PASSWORD_CONFIRM = "passwordConfirm";
    public static final String USER = "user";
    public static final String COMMENT_TEXT = "commentText";
    public static final String INVALID_COMMENT = "invalidCommentField";
    public static final String COMMENTS = "comments";
    public static final String COMMENT_ID = "commentId";
    public static final String ABSOLUTE_PATH = "/";
    public static final String AVG_RATING = "avgRating";
    public static final String RATING = "rating";
    public static final String AUTHOR_ID = "authorId";
    public static final String USER_EXISTS = "userExists";
    public static final String BOOK_EXISTS = "bookExists";
    public static final String AUTHOR_EXISTS = "authorExists";
    public static final String INVALID_DATA = "invalidData";
    public static final String INCORRECT_LOGIN_DATA = "incorrectLoginData";
    public static final String INVALID_REGISTRATION_DATA = "invalidRegistrationData";
    public static final String ROLE = "role";
    public static final String IN_READ = "inRead";
    public static final String IN_FAVORITE = "inFavorite";
    public static final String FAVORITE_BOOKS_AMOUNT = "favoriteBooksAmount";
    public static final String READ_BOOKS_AMOUNT = "readBooksAmount";
    public static final String COMMENTS_AMOUNT = "commentsAmount";
    public static final String AUTHORS_LIST = "authors";
    public static final String GENRES = "genres";
    public static final String PAGE = "page";
    public static final String LOCALE = "locale";
    public static final String YES =  "Y";
    public static final String USER_ID = "userId";
    public static final String BAN_ACTION = "action";
    public static final String BAN = "ban";
    public static final String UNLOCK = "unlock";
    public static final String POPULAR_BOOKS = "popularBooks";
    public static final String STATISTICS_LIST = "statisticsList";
    public static final String BOOKS_TYPE = "type";
    public static final String READ = "read";
    public static final String FAVORITE = "favorite";
    public static final String LIST = "list";
    public static final String INSUFFICIENT_RIGHTS = "insufficientRights";
    public static final String GENRE_SEARCH = "genreSearch";
    public static final String OLD_NAME = "oldName";
    public static final int NO_RATING = 0;
    public static final int START_PAGE_INDEX = 1;
    public static final int BOOKS_PER_PAGE = 6;
    public static final int COMMENTS_PER_PAGE = 3;
    public static final int AUTHORS_PER_PAGE = 10;
    public static final int USERS_PER_PAGE = 10;
    public static final int ZERO_INDEX = 0;


    public static final String ERROR_PAGE = "/jsp/fail.jsp";
    public static final String BOOK_LIST_PAGE = "/jsp/main.jsp";
    public static final String USER_COMMENTS_PAGE = "/jsp/comments.jsp";
    public static final String ADD_BOOK_PAGE = "/jsp/addBook.jsp";
    public static final String SINGLE_BOOK_PAGE = "/jsp/bookInfo.jsp";
    public static final String MAIN_PAGE = "/index.jsp";
    public static final String EDIT_BOOK_PAGE ="/jsp/editBook.jsp";
    public static final String REGISTRATION_PAGE = "/jsp/registration.jsp";
    public static final String LOGIN_PAGE = "/jsp/login.jsp";
    public static final String VIEW_PROFILE_PAGE = "/jsp/profile.jsp";
    public static final String EDIT_PROFILE_PAGE = "/jsp/editProfile.jsp";
    public static final String VIEW_TODAY_COMMENTS_PAGE = "/jsp/viewTodayComments.jsp";
    public static final String VIEW_USERS_INFO_PAGE = "/jsp/userStatistics.jsp";
    public static final String FAVORITE_BOOKS_PAGE = "/jsp/favoriteBooks.jsp";
    public static final String ADD_AUTHOR_PAGE = "/jsp/addAuthor.jsp";
    public static final String AUTHOR_INFO_PAGE = "/jsp/authorInfo.jsp";
    public static final String EDIT_AUTHOR_PAGE = "/jsp/editAuthor.jsp";
    public static final String READ_BOOKS_PAGE = "/jsp/readBooks.jsp";
    public static final String SEARCH_RESULT_PAGE = "/jsp/searchResult.jsp";
    public static final String VIEW_ALL_AUTHORS_PAGE = "/jsp/authors.jsp";
    public static final String VIEW_ALL_BOOKS_PAGE = "/jsp/books.jsp";
}
