package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.UserService;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view user's profile<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewProfileCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long userId = Long.parseLong(request.getParameter(USER_ID));
        try {
            User user = UserService.findUserById(userId);
            int amountOfFavorite = BookService.findAmountOfFavoriteBooks(userId);
            int amountOfRead = BookService.findAmountOfReadBooks(userId);
            int amountOfComments = CommentService.findAmountOfCommentsByUserId(userId);

            request.setAttribute(USER, user);
            request.setAttribute(FAVORITE_BOOKS_AMOUNT, amountOfFavorite);
            request.setAttribute(READ_BOOKS_AMOUNT, amountOfRead);
            request.setAttribute(COMMENTS_AMOUNT, amountOfComments);
        } catch (ServiceException e) {
            throw new CommandException("Exception in this command.", e);
        }
        return VIEW_PROFILE_PAGE;
    }
}
