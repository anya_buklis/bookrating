package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for searching by title<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class SearchCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String title = request.getParameter(TITLE);
        try{
            int page = START_PAGE_INDEX;
            if (request.getParameter(PAGE) != null) page = Integer.parseInt(request.getParameter(PAGE));
            List<Book> partOfBookList = BookService.findBooksByTitle((page - START_PAGE_INDEX) * BOOKS_PER_PAGE
                    , BOOKS_PER_PAGE, title);

            int totalAmount = BookService.findAmountOfBooksByTitle(title);
            int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

            request.setAttribute(BOOK_LIST, partOfBookList);
            request.setAttribute(QUERY, title);
            request.setAttribute(PAGES_AMOUNT, pagesAmount);
        } catch (ServiceException e){
            throw new CommandException("Exception in ViewAllBooksCommand.", e);
        }
        return SEARCH_RESULT_PAGE;
    }
}
