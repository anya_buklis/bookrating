package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.RatingService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for leaving user rating<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class LeaveRatingCommand implements ICommand {
    private static Logger logger = Logger.getLogger(LeaveRatingCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isUser(request)){
            int rating = Integer.parseInt(request.getParameter(RATING));
            long userId = Long.parseLong(request.getParameter(USER_ID));
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            try {
                RatingService.leaveRating(userId, bookId, rating);
            } catch (ServiceException e) {
                throw new CommandException("Exception in Leave Rating Command ", e);
            }
            return SINGLE_BOOK_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
