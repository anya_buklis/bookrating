package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.CommentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view today's comments<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewTodayCommentsCommand implements ICommand {
    private static Logger logger = Logger.getLogger(ViewTodayCommentsCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)){
            try{
                Map<Comment, String> comments = CommentService.viewTodayComments();
                request.setAttribute(COMMENTS, comments);
            } catch (ServiceException e){
                throw new CommandException("Exception in ViewTodayCommand.", e);
            }
            return VIEW_TODAY_COMMENTS_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
