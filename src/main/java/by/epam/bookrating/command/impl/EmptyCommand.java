package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.ERROR_PAGE;

/**
 * Empty command which runs if no command will be selected<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EmptyCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        return ERROR_PAGE;
    }
}
