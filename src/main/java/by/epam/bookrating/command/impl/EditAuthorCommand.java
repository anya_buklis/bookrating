package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.AuthorService;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for editing author<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EditAuthorCommand implements ICommand {
    private static Logger logger = Logger.getLogger(EditAuthorCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        if (isAdmin(request)) {
            String fullName = request.getParameter(FULL_NAME);
            String birthYear = request.getParameter(BIRTH_YEAR);
            String birthCountry = request.getParameter(BIRTH_COUNTRY);
            String biography = request.getParameter(BIOGRAPHY);
            String oldImageUrl = request.getParameter(OLD_IMAGE_URL);
            String oldName = request.getParameter(OLD_NAME);
            long authorId = Long.parseLong(request.getParameter(AUTHOR_ID));
            try {
                if (Validator.isBiographyValid(biography) && Validator.isBirthYearValid(birthYear) &&
                        Validator.isFullNameValid(fullName) && Validator.isBirthCountryValid(birthCountry)) {
                    int birthYearInt = Integer.parseInt(birthYear);
                    Part imgPart = null;
                    try {
                        imgPart = request.getPart(NEW_IMAGE_URL);
                    } catch (IOException | ServletException e) {
                        logger.warn("Exception occurred during getting image from JSP.");
                    }
                    String absoluteDiskPath = request.getServletContext().getRealPath(ABSOLUTE_PATH);
                    if (AuthorService.editAuthor(authorId, fullName, oldName, birthYearInt, birthCountry,
                            biography, oldImageUrl, imgPart, absoluteDiskPath)) {
                        Author author = AuthorService.findAuthorById(authorId);

                        List<Book> books = BookService.findBooksByAuthorId(ZERO_INDEX, BOOKS_PER_PAGE, authorId);

                        int totalAmount = AuthorService.findAmountOfBooksByAuthor(authorId);
                        int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

                        request.setAttribute(AUTHOR, author);
                        request.setAttribute(PAGES_AMOUNT, pagesAmount);
                        request.setAttribute(BOOK_LIST, books);
                        page = AUTHOR_INFO_PAGE;
                    } else {
                        request.setAttribute(AUTHOR_EXISTS, YES);
                        Author author = AuthorService.findAuthorById(authorId);
                        request.setAttribute(AUTHOR, author);
                        page = EDIT_AUTHOR_PAGE;
                    }
                } else {
                    request.setAttribute(INVALID_DATA, YES);
                    logger.warn("Invalid author data received.");
                    Author author = AuthorService.findAuthorById(authorId);
                    request.setAttribute(AUTHOR, author);
                    page = EDIT_AUTHOR_PAGE;
                }
            } catch (ServiceException e) {
                throw new CommandException("Exception in EditBookCommand.", e);
            }
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
        return page;
    }
}
