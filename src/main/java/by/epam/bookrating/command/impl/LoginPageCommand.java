package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the login page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class LoginPageCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (!isAdmin(request)&& !isUser(request)){
            return LOGIN_PAGE;
        } else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
