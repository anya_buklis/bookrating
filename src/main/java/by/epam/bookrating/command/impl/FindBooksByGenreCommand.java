package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.GenreService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for finding books by single genre<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class FindBooksByGenreCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try {
            int genreId = Integer.parseInt(request.getParameter(GENRE));
            Genre genre = GenreService.findGenreById(genreId);
            String genreName = genre.getGenreName();

            int page = START_PAGE_INDEX;
            if (request.getParameter(PAGE) != null) page = Integer.parseInt(request.getParameter(PAGE));
            List<Book> partOfBookList = BookService.findBooksByGenre((page - START_PAGE_INDEX) * BOOKS_PER_PAGE
                    , BOOKS_PER_PAGE, genreId);

            int totalAmount = BookService.findAmountOfBooksByGenre(genreId);
            int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

            request.setAttribute(BOOK_LIST, partOfBookList);
            request.setAttribute(PAGES_AMOUNT, pagesAmount);
            request.setAttribute(GENRE, genreId);
            request.setAttribute(GENRE_SEARCH, YES);
            request.setAttribute(QUERY, genreName);
        } catch (ServiceException e) {
            throw new CommandException("Exception in Find Books By Genre command", e);
        }
        return SEARCH_RESULT_PAGE;
    }
}
