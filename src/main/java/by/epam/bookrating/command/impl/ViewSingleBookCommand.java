package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.RatingService;
import by.epam.bookrating.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view info about single book<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */

public class ViewSingleBookCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long bookId = Long.parseLong(request.getParameter(BOOK_ID));
        try {
            Book book = BookService.findBookById(bookId);
            double avgRating = RatingService.findAvgRatingByBookId(bookId);
            Map<Comment, User> comments = CommentService.findCommentsByBookId(bookId);

            request.setAttribute(BOOK_ITEM, book);
            request.setAttribute(COMMENTS, comments);
            request.setAttribute(AVG_RATING, avgRating);
            if (isUser(request)) {
                long userId = Long.parseLong(request.getSession().getAttribute(USER_ID).toString());
                int rating = RatingService.checkRating(bookId, userId);

                boolean isBookInRead = UserService.isBookInRead(userId, bookId);
                if (isBookInRead){
                    request.setAttribute(IN_READ, YES);
                }

                boolean isBookInFavorite = UserService.isBookInFavorite(userId, bookId);
                if (isBookInFavorite){
                    request.setAttribute(IN_FAVORITE, YES);
                }
                request.setAttribute(RATING, rating);
            }
            request.getSession().setAttribute(PREVIOUS_PAGE, SINGLE_BOOK_PAGE);
            request.getSession().setAttribute(BOOK_ID, bookId);
        } catch (ServiceException e) {
            throw new CommandException("Exception in LoginCommand.", e);
        }
        return SINGLE_BOOK_PAGE;
    }
}

