package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.AuthorService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for viewing all/part of all authors<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewAuthorsStatisticsCommand implements ICommand{
    private static Logger logger = Logger.getLogger(ViewAuthorsStatisticsCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            try {
                int page = START_PAGE_INDEX;
                if (request.getParameter(PAGE) != null) page = Integer.parseInt(request.getParameter(PAGE));

                List<Author> authorList = AuthorService.findAllAuthors((page - START_PAGE_INDEX) * AUTHORS_PER_PAGE,
                        AUTHORS_PER_PAGE);
                request.setAttribute(AUTHORS_LIST, authorList);

                int totalAmount = AuthorService.findAmountOfAuthors();
                int pagesAmount = AuthorService.calculatePagesAmount(totalAmount, AUTHORS_PER_PAGE);

                request.setAttribute(PAGES_AMOUNT, pagesAmount);
                return VIEW_ALL_AUTHORS_PAGE;
            } catch (ServiceException e){
                throw new CommandException("Exception in ViewAllAuthorsCommand.", e);
            }
        } else {
            logger.warn("Insufficient rights.");
            return ERROR_PAGE;
        }
    }
}
