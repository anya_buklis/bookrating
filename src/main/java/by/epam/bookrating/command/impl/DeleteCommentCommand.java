package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.RatingService;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for deleting comment<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class DeleteCommentCommand implements ICommand {
    private static Logger logger = Logger.getLogger(DeleteCommentCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request) || isUser(request)) {
            String previousPage = request.getParameter(PREVIOUS_PAGE);
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            long commentId = Long.parseLong(request.getParameter(COMMENT_ID));
            logger.info("Previous page = " + previousPage);
            try {
                CommentService.deleteComment(commentId);
                switch (previousPage) {
                    case VIEW_TODAY_COMMENTS_PAGE:
                        Map<Comment, String> commentsWithLogin = CommentService.viewTodayComments();
                        request.setAttribute(COMMENTS, commentsWithLogin);
                        break;
                    case USER_COMMENTS_PAGE:
                        long userId = Long.parseLong(request.getParameter(USER_ID));
                        Map<Comment, Book> commentsWithBook = UserService.findAllCommentsByUserId(ZERO_INDEX,
                                COMMENTS_PER_PAGE, userId);
                        int totalAmount = CommentService.findAmountOfCommentsByUserId(userId);

                        int pagesAmount = BookService.calculatePagesAmount(totalAmount, COMMENTS_PER_PAGE);
                        request.setAttribute(USER_ID, userId);
                        request.setAttribute(PAGES_AMOUNT, pagesAmount);
                        request.setAttribute(COMMENTS, commentsWithBook);
                        break;
                    case SINGLE_BOOK_PAGE:
                        Book currentBook = BookService.findBookById(bookId);
                        double avgRating = RatingService.findAvgRatingByBookId(bookId);
                        Map<Comment, User> comments = CommentService.findCommentsByBookId(bookId);

                        request.setAttribute(BOOK_ITEM, currentBook);
                        request.setAttribute(AVG_RATING, avgRating);
                        request.setAttribute(COMMENTS, comments);
                        break;
                    default:
                        break;
                }
            } catch (ServiceException e) {
                throw new CommandException("Exception is occurred in command.", e);
            }
            return previousPage;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
