package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for viewing all/part of all books<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewAllBooksCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        try{
            int page = START_PAGE_INDEX;
            if(request.getParameter(PAGE)!= null) page = Integer.parseInt(request.getParameter(PAGE));

            List<Book> partOfBookList = BookService.findAllBooks((page - START_PAGE_INDEX) * BOOKS_PER_PAGE,
                    BOOKS_PER_PAGE);
            Map<Book, Double> popularBooks =  BookService.findBooksWithHighRating();
            request.setAttribute(BOOK_LIST, partOfBookList);
            request.setAttribute(POPULAR_BOOKS, popularBooks);

            int totalAmount = BookService.getAmountOfBooks();
            int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

            request.setAttribute(PAGES_AMOUNT, pagesAmount);
        } catch (ServiceException e){
            throw new CommandException("Exception in ViewAllBooksCommand.", e);
        }
        return BOOK_LIST_PAGE;
    }
}
