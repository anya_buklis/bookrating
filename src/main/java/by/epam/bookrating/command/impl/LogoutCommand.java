package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.LOCALE;
import static by.epam.bookrating.command.Constant.LOGIN_PAGE;

/**
 * Command for signing out<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class LogoutCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String locale = (String) request.getSession().getAttribute(LOCALE);
        request.getSession().invalidate();
        request.getSession().setAttribute(LOCALE, locale.toLowerCase());
        return LOGIN_PAGE;
    }
}
