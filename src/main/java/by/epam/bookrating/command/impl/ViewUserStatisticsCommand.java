package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Statistics;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view user statistics (amount of comments, left ratings ans so on)<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewUserStatisticsCommand implements ICommand {
    private static Logger logger = Logger.getLogger(ViewUserStatisticsCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            try {
                int page = START_PAGE_INDEX;
                if (request.getParameter(PAGE) != null) page = Integer.parseInt(request.getParameter(PAGE));

                List<Statistics> partOfStatistics = UserService.findUserStatistics(
                        (page - START_PAGE_INDEX) * USERS_PER_PAGE, USERS_PER_PAGE);
                request.setAttribute(AUTHORS_LIST, partOfStatistics);

                int totalAmount = UserService.findAmountOfUsers();
                int pagesAmount = UserService.calculatePagesAmount(totalAmount, USERS_PER_PAGE);

                request.setAttribute(PAGES_AMOUNT, pagesAmount);
                request.setAttribute(STATISTICS_LIST, partOfStatistics);
            } catch (ServiceException e) {
                throw new CommandException("Exception in command.", e);
            }
            return VIEW_USERS_INFO_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
