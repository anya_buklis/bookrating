package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.GenreService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for adding book<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class AddBookCommand implements ICommand {
    private static Logger logger = Logger.getLogger(AddBookCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        if (isAdmin(request)) {
            String title = request.getParameter(TITLE);
            String publishingYear = request.getParameter(PUBLISHING_YEAR);
            String description = request.getParameter(DESCRIPTION);
            String[] genres = request.getParameterValues(GENRE);
            String[] authors = request.getParameterValues(AUTHOR);
            try {
                if (Validator.isGenresValid(genres) && Validator.isPublishingYearValid(publishingYear) &&
                        Validator.isTitleValid(title) && Validator.isDescriptionValid(description)
                        && Validator.isAuthorsValid(authors)) {

                    String absoluteDiskPath = request.getServletContext().getRealPath(ABSOLUTE_PATH);
                    int publishingYearInt = Integer.parseInt(publishingYear);
                    Part imgPart = null;
                    try {
                        imgPart = request.getPart(IMAGE_URL);
                    } catch (IOException | ServletException e) {
                        logger.warn("Exception during getting image part from jsp.");
                    }

                    if (!BookService.addBook(title, publishingYearInt,
                            authors, description, imgPart, absoluteDiskPath, genres)) {
                        request.setAttribute(BOOK_EXISTS, YES);
                        page = ADD_BOOK_PAGE;
                    } else {
                        long bookId = BookService.findBookIdByTitle(title);
                        Book book = BookService.findBookById(bookId);
                        request.setAttribute(BOOK, book);
                        page = SINGLE_BOOK_PAGE;
                    }
                } else {
                    request.setAttribute(INVALID_DATA, YES);
                    logger.warn("Invalid book data received.");
                    List genresList = GenreService.findAllGenres();
                    request.setAttribute(GENRES, genresList);
                    page = ADD_BOOK_PAGE;
                }
            } catch (ServiceException e){
                throw new CommandException("Exception in AddBookCommand.", e);
            }
        }else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            logger.warn("Insufficient rights to do this action.");
            page = ERROR_PAGE;
        }
        return page;
    }
}
