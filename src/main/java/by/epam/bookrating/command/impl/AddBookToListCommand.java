package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.RatingService;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for adding book to a favorite/read list<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class AddBookToListCommand implements ICommand {
    private static Logger logger = Logger.getLogger(AddBookToListCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isUser(request)){
            HttpSession session = request.getSession();
            String userIdStr = String.valueOf(session.getAttribute(USER_ID));
            long userId = Long.parseLong(userIdStr);
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            String list = request.getParameter(LIST).toLowerCase();
            try {
                switch (list){
                    case READ:
                        UserService.addBookToRead(userId, bookId);
                        break;
                    case FAVORITE:
                        UserService.addBookToFavorite(userId, bookId);
                        break;
                    default: break;
                }
                Book currentBook = BookService.findBookById(bookId);
                Map<Comment, User> comments = CommentService.findCommentsByBookId(bookId);
                double avgRating = RatingService.findAvgRatingByBookId(bookId);
                int userRating = RatingService.checkRating(bookId, userId);

                boolean isBookInRead = UserService.isBookInRead(userId, bookId);
                if (isBookInRead){
                    request.setAttribute(IN_READ, YES);
                }
                boolean isBookInFavorite = UserService.isBookInFavorite(userId, bookId);
                if (isBookInFavorite){
                    request.setAttribute(IN_FAVORITE, YES);
                }
                request.setAttribute(BOOK_ITEM, currentBook);
                request.setAttribute(COMMENTS, comments);
                request.setAttribute(AVG_RATING, avgRating);
                request.setAttribute(RATING, userRating);
            } catch (ServiceException e) {
                throw new CommandException("Exception in AddBookToListCommand.", e);
            }
            return SINGLE_BOOK_PAGE;
        } else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            logger.warn("Insufficient rights to do this action.");
            return ERROR_PAGE;
        }
    }
}
