package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.GenreService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the add book page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class AddBookPageCommand implements ICommand {
    private static Logger logger = Logger.getLogger(AddBookPageCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            try {
                List<Genre> genres = GenreService.findAllGenres();
                request.setAttribute(GENRES, genres);
            } catch (ServiceException e) {
                throw new CommandException("Exception in AddBookPage command", e);
            }
            return ADD_BOOK_PAGE;
        } else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            logger.warn("Insufficient rights to do this action.");
            return ERROR_PAGE;
        }
    }
}
