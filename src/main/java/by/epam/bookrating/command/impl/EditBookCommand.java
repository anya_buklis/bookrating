package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.GenreService;
import by.epam.bookrating.service.RatingService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for editing book<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EditBookCommand implements ICommand {
    private static Logger logger = Logger.getLogger(EditBookCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        if (isAdmin(request)) {
            String title = request.getParameter(TITLE);
            String publishingYear = request.getParameter(PUBLISHING_YEAR);
            String[] authors = request.getParameterValues(AUTHOR);
            String description = request.getParameter(DESCRIPTION);
            String[] genres = request.getParameterValues(GENRE);
            String oldImageUrl = request.getParameter(OLD_IMAGE_URL);
            String oldTitle = request.getParameter(OLD_TITLE);
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            try {
                String absoluteDiskPath = request.getServletContext().getRealPath(ABSOLUTE_PATH);
                if (Validator.isGenresValid(genres) && Validator.isPublishingYearValid(publishingYear) &&
                        Validator.isTitleValid(title) && Validator.isDescriptionValid(description)
                        && Validator.isAuthorsValid(authors)) {
                    Part imgPart = null;
                    try {
                        imgPart = request.getPart(NEW_IMAGE_URL);
                    } catch (IOException | ServletException e) {
                        logger.warn("Exception occurred while getting image part from jsp.");
                    }
                    int publishingYearInt = Integer.parseInt(publishingYear);
                    if (BookService.editBook(bookId, title, oldTitle, publishingYearInt,
                            authors, description, oldImageUrl, imgPart, absoluteDiskPath, genres)) {
                        Book book = BookService.findBookById(bookId);
                        Map<Comment, User> comments = CommentService.findCommentsByBookId(bookId);
                        double avgRating = RatingService.findAvgRatingByBookId(bookId);

                        request.setAttribute(BOOK_ITEM, book);
                        request.setAttribute(COMMENTS, comments);
                        request.setAttribute(AVG_RATING, avgRating);
                        page = SINGLE_BOOK_PAGE;
                    } else {
                        Book book = BookService.findBookById(bookId);
                        List<Genre> genresToEdit = GenreService.findAllGenres();
                        request.setAttribute(BOOK_EXISTS, YES);
                        request.setAttribute(BOOK_ITEM, book);
                        request.setAttribute(GENRES, genresToEdit);
                        page = EDIT_BOOK_PAGE;
                    }
                } else{
                    request.setAttribute(INVALID_DATA, YES);
                    logger.warn("Invalid book data received.");
                    Book book = BookService.findBookById(bookId);
                    request.setAttribute(BOOK, book);
                    page = EDIT_BOOK_PAGE;
                }
            } catch(ServiceException e){
                throw new CommandException("Exception in EditBookCommand.", e);
            }
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
        return page;
    }
}
