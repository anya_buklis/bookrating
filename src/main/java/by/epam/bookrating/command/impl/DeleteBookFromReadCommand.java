package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.RatingService;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for deleting book from read list<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class DeleteBookFromReadCommand implements ICommand {
    private static Logger logger = Logger.getLogger(DeleteBookFromReadCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = ERROR_PAGE;
        HttpSession session = request.getSession();
        if (isUser(request)){
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            long userId = Long.parseLong(session.getAttribute(USER_ID).toString());
            String previous = request.getParameter(PREVIOUS_PAGE);
            try {
                UserService.deleteBookFromRead(userId, bookId);
                if (previous != null && previous.equalsIgnoreCase(SINGLE_BOOK_PAGE)){
                    Book book = BookService.findBookById(bookId);
                    double avgRating = RatingService.findAvgRatingByBookId(bookId);
                    Map<Comment, User> comments = CommentService.findCommentsByBookId(bookId);
                    int rating = RatingService.checkRating(bookId, userId);

                    boolean isBookInFavorite = UserService.isBookInFavorite(userId, bookId);
                    if (isBookInFavorite){
                        request.setAttribute(IN_FAVORITE, YES);
                    }

                    request.setAttribute(RATING, rating);
                    request.setAttribute(BOOK, book);
                    request.setAttribute(COMMENTS, comments);
                    request.setAttribute(AVG_RATING, avgRating);
                    page = SINGLE_BOOK_PAGE;
                } else {
                    List<Book> readBooks = UserService.findReadBooks(ZERO_INDEX, BOOKS_PER_PAGE, userId);
                    int favoriteTotalAmount = BookService.findAmountOfReadBooks(userId);
                    int favoritePagesAmount = BookService.calculatePagesAmount(favoriteTotalAmount, BOOKS_PER_PAGE);

                    request.setAttribute(BOOK_LIST, readBooks);
                    request.setAttribute(PAGES_AMOUNT, favoritePagesAmount);
                    page = READ_BOOKS_PAGE;
                }
            } catch (ServiceException e) {
                throw new CommandException("Exception in this command", e);
            }
            return page;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
        }
        return page;
    }
}
