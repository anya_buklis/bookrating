package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view user's list (favorite/read)<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewUserBooksCommand implements ICommand {
    private static Logger logger = Logger.getLogger(ViewUserBooksCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = ERROR_PAGE;
        if (isSameUser(request)){
        long userId = Long.parseLong(request.getParameter(USER_ID));
        String parameter = request.getParameter(BOOKS_TYPE);
            try{
                switch (parameter){
                    case READ:
                        int pageIndexRead = START_PAGE_INDEX;
                        if(request.getParameter(PAGE)!= null) pageIndexRead = Integer.parseInt(request.getParameter(PAGE));

                        List<Book> readBooks = UserService.findReadBooks((pageIndexRead - START_PAGE_INDEX)
                                * BOOKS_PER_PAGE, BOOKS_PER_PAGE, userId);
                        int readTotalAmount = BookService.findAmountOfReadBooks(userId);
                        int readPagesAmount = BookService.calculatePagesAmount(readTotalAmount, BOOKS_PER_PAGE);

                        request.setAttribute(BOOK_LIST, readBooks);
                        request.setAttribute(PAGES_AMOUNT, readPagesAmount);
                        page = READ_BOOKS_PAGE;
                        break;
                    case FAVORITE:
                        int pageIndex = START_PAGE_INDEX;
                        if(request.getParameter(PAGE)!= null) pageIndex = Integer.parseInt(request.getParameter(PAGE));

                        List<Book> favoriteBooks = UserService.findFavoriteBooks((pageIndex - START_PAGE_INDEX)
                                * BOOKS_PER_PAGE, BOOKS_PER_PAGE, userId);
                        int favoriteTotalAmount = BookService.findAmountOfFavoriteBooks(userId);
                        int favoritePagesAmount = BookService.calculatePagesAmount(favoriteTotalAmount, BOOKS_PER_PAGE);

                        request.setAttribute(BOOK_LIST, favoriteBooks);
                        request.setAttribute(PAGES_AMOUNT, favoritePagesAmount);
                        page = FAVORITE_BOOKS_PAGE;
                        break;
                    default: break;
                }
                request.setAttribute(USER_ID, userId);
            } catch (ServiceException e) {
                throw new CommandException("Exception in ViewUserBooksCommand", e);
            }
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
        return page;
    }
}
