package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Statistics;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for ban/unlock user <br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class BanUserCommand implements ICommand {
    private static Logger logger = Logger.getLogger(BanUserCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            long userId = Long.parseLong(request.getParameter(USER_ID));
            String action = request.getParameter(BAN_ACTION);
            try {
                if (action.equals(BAN)){
                    UserService.banUser(userId);
                } else if (action.equals(UNLOCK)){
                    UserService.unlockUser(userId);
                }
                List<Statistics> statistics = UserService.findUserStatistics(ZERO_INDEX, USERS_PER_PAGE);

                request.setAttribute(STATISTICS_LIST, statistics);
                return VIEW_USERS_INFO_PAGE;
            } catch (ServiceException e) {
                throw new CommandException("Exception in this command.", e);
            }
        } else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            logger.warn("Insufficient rights to do this action.");
            return ERROR_PAGE;
        }
    }
}
