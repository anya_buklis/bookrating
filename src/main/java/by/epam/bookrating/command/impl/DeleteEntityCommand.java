package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.AuthorService;
import by.epam.bookrating.service.BookService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for deleting book<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class DeleteEntityCommand implements ICommand {
    private static Logger logger = Logger.getLogger(DeleteEntityCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String pageToForward = ERROR_PAGE;
        if (isAdmin(request)) {
            String entity = request.getParameter(ENTITY);
            try {
                switch (entity){
                    case BOOK:
                        long bookId = Long.parseLong(request.getParameter(BOOK_ID));
                        BookService.deleteBook(bookId);
                        List<Book> partOfBookList = BookService.findAllBooks(ZERO_INDEX, BOOKS_PER_PAGE);
                        request.setAttribute(BOOK_LIST, partOfBookList);

                        int totalAmount = BookService.getAmountOfBooks();
                        int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

                        request.setAttribute(PAGES_AMOUNT, pagesAmount);
                        request.setAttribute(PAGE, START_PAGE_INDEX);
                        pageToForward = VIEW_ALL_BOOKS_PAGE;
                        break;
                    case AUTHOR:
                        long authorId = Long.parseLong(request.getParameter(AUTHOR_ID));
                        AuthorService.deleteAllBooksByAuthor(authorId);
                        AuthorService.deleteAuthorById(authorId);

                        List<Author> authorList = AuthorService.findAllAuthors(ZERO_INDEX, AUTHORS_PER_PAGE);
                        request.setAttribute(AUTHORS_LIST, authorList);

                        int totalAuthorsAmount = AuthorService.findAmountOfAuthors();
                        int pagesAuthorsAmount = AuthorService.calculatePagesAmount(totalAuthorsAmount,
                                AUTHORS_PER_PAGE);

                        request.setAttribute(PAGES_AMOUNT, pagesAuthorsAmount);
                        request.setAttribute(PAGE, START_PAGE_INDEX);
                        pageToForward = VIEW_ALL_AUTHORS_PAGE;
                        break;
                    default: break;
                }
            } catch (ServiceException e) {
                throw new CommandException("Exception in DeleteBookCommand.", e);
            }
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
        }
        return pageToForward;
    }
}
