package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view comments left by users<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewUserCommentsCommand implements ICommand {
    private static Logger logger = Logger.getLogger(ViewUserCommentsCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long userId = Long.parseLong(request.getParameter(USER_ID));
        if (isSameUser(request)){
            try {
                int pageIndex = START_PAGE_INDEX;
                if(request.getParameter(PAGE)!= null) pageIndex = Integer.parseInt(request.getParameter(PAGE));
                Map<Comment, Book> comments = UserService.findAllCommentsByUserId((pageIndex - START_PAGE_INDEX)
                        * COMMENTS_PER_PAGE, COMMENTS_PER_PAGE, userId);
                int totalAmount = CommentService.findAmountOfCommentsByUserId(userId);

                int pagesAmount = BookService.calculatePagesAmount(totalAmount, COMMENTS_PER_PAGE);
                request.setAttribute(USER_ID, userId);
                request.setAttribute(PAGES_AMOUNT, pagesAmount);
                request.setAttribute(COMMENTS, comments);
            } catch (ServiceException e){
                throw new CommandException("Exception is occurred." , e);
            }
            return USER_COMMENTS_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
