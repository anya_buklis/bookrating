package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Genre;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.GenreService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the edit book page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EditBookPageCommand implements ICommand {
    private static Logger logger = Logger.getLogger(EditBookPageCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            try {
                long bookId = Long.parseLong(request.getParameter(BOOK_ID));
                Book book = BookService.findBookById(bookId);
                List<Genre> genres = GenreService.findAllGenres();

                request.setAttribute(BOOK_ITEM, book);
                request.setAttribute(GENRES, genres);
            } catch (ServiceException e) {
                throw new CommandException("Exception occurred while sending book item to a page.", e);
            }
            return EDIT_BOOK_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
