package by.epam.bookrating.command.impl;
import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for viewing all/part of all books<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewBooksStatisticsCommand implements ICommand {
    private static Logger logger = Logger.getLogger(ViewBooksStatisticsCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            try {
                int page = START_PAGE_INDEX;
                if (request.getParameter(PAGE) != null) page = Integer.parseInt(request.getParameter(PAGE));
                List<Book> partOfBookList = BookService.findAllBooks((page - START_PAGE_INDEX) * BOOKS_PER_PAGE,
                        BOOKS_PER_PAGE);
                request.setAttribute(BOOK_LIST, partOfBookList);

                int totalAmount = BookService.getAmountOfBooks();
                int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

                request.setAttribute(PAGES_AMOUNT, pagesAmount);
                return VIEW_ALL_BOOKS_PAGE;
            } catch (ServiceException e){
                throw new CommandException("Exception in ViewAllAuthorsCommand.", e);
            }
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
