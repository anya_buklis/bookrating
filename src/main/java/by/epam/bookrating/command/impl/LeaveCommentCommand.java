package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for leaving user comments<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class LeaveCommentCommand implements ICommand {
    private static Logger logger = Logger.getLogger(LeaveRatingCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isUser(request)){
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            HttpSession session = request.getSession();
            String userIdStr = String.valueOf(session.getAttribute(USER_ID));
            long userId = Long.parseLong(userIdStr);
            String commentText = request.getParameter(COMMENT_TEXT);
            try {
                if (Validator.isCommentTextValid(commentText)){
                    CommentService.leaveComment(bookId, userId,commentText);
                    request.getSession().removeAttribute(INVALID_COMMENT);
                } else {
                    request.getSession().setAttribute(INVALID_COMMENT, YES);
                }
            } catch (ServiceException e){
                throw new CommandException("Exception in LeaveComment command.",e);
            }
            return SINGLE_BOOK_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
