package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the registration page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */

public class RegistrationPageCommand implements ICommand {
    private static Logger logger = Logger.getLogger(RegistrationPageCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (!isAdmin(request) && !isUser(request)){
            return REGISTRATION_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
