package by.epam.bookrating.command.impl;
import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the edit profile page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EditProfilePageCommand implements ICommand {
    private static Logger logger = Logger.getLogger(EditProfilePageCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isUser(request)){
            long userId = Long.parseLong(request.getParameter(USER_ID));
            try {
                User user = UserService.findUserById(userId);
                request.setAttribute(USER, user);
            } catch (ServiceException e) {
                throw new CommandException("Exception is occurred in command.", e);
            }
            return EDIT_PROFILE_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
