package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.AuthorService;
import by.epam.bookrating.service.BookService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for view author information and his books<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ViewAuthorCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        long authorId = Long.parseLong(request.getParameter(AUTHOR_ID));
        try {
            Author author = AuthorService.findAuthorById(authorId);

            int page = START_PAGE_INDEX;
            if(request.getParameter(PAGE)!= null) page = Integer.parseInt(request.getParameter(PAGE));

            List<Book> books = BookService.findBooksByAuthorId((page - START_PAGE_INDEX) * BOOKS_PER_PAGE,
                    BOOKS_PER_PAGE, authorId);

            int totalAmount = AuthorService.findAmountOfBooksByAuthor(authorId);
            int pagesAmount = BookService.calculatePagesAmount(totalAmount, BOOKS_PER_PAGE);

            request.setAttribute(PAGES_AMOUNT, pagesAmount);
            request.setAttribute(AUTHOR, author);
            request.setAttribute(BOOK_LIST, books);
        } catch (ServiceException e) {
            throw new CommandException("Exception in LoginCommand.", e);
        }
        return AUTHOR_INFO_PAGE;
    }
}
