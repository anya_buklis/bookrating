package by.epam.bookrating.command.impl;
import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the add author page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class AddAuthorPageCommand implements ICommand {
    private static Logger logger = Logger.getLogger(AddAuthorPageCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            return ADD_AUTHOR_PAGE;
        } else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            logger.warn("Insufficient rights to do this action.");
            return ERROR_PAGE;
        }
    }
}
