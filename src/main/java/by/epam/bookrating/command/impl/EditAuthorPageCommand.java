package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.AuthorService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for forwarding to the edit author page<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EditAuthorPageCommand implements ICommand {
    private static Logger logger = Logger.getLogger(EditAuthorPageCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (isAdmin(request)) {
            try {
                long authorId = Long.parseLong(request.getParameter(AUTHOR_ID));
                Author author = AuthorService.findAuthorById(authorId);
                request.setAttribute(AUTHOR, author);
            } catch (ServiceException e) {
                throw new CommandException("Exception in Command Layer", e);
            }
            return EDIT_AUTHOR_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
