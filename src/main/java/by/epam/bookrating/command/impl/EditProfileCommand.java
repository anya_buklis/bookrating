package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.UserService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for editing profile<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class EditProfileCommand implements ICommand {
    private static Logger logger = Logger.getLogger(EditProfileCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        HttpSession session = request.getSession();
        long userId = Long.parseLong(request.getParameter(USER_ID));
        long userIdInSession = Long.parseLong(session.getAttribute(USER_ID).toString());
        if (isUser(request) && (userId == userIdInSession)){
            String name = request.getParameter(NAME);
            String age = request.getParameter(AGE);
            String info = request.getParameter(INFO);
            String oldImageUrl = request.getParameter(OLD_IMAGE_URL);
            try {
                if (Validator.isAgeValid(age) && Validator.isUserNameValid(name) && Validator.isUserInfoValid(info)) {
                    String absoluteDiskPath = request.getServletContext().getRealPath(ABSOLUTE_PATH);
                    int ageInt = Integer.parseInt(age);
                    try {
                        Part imgPart = request.getPart(NEW_IMAGE_URL);
                        if (imgPart != null) {
                            UserService.editAvatar(imgPart, absoluteDiskPath, oldImageUrl, userId);
                        }
                    } catch (IOException | ServletException e) {
                        logger.info("Exception during getting new user avatar.");
                    }
                    int amountOfFavorite = BookService.findAmountOfFavoriteBooks(userId);
                    int amountOfRead = BookService.findAmountOfReadBooks(userId);
                    List<Comment> comments = CommentService.findCommentsByUserId(userId);

                    User user = UserService.editProfile(userId, name, ageInt, info);
                    request.getSession().setAttribute(USER, user);
                    request.setAttribute(FAVORITE_BOOKS_AMOUNT, amountOfFavorite);
                    request.setAttribute(READ_BOOKS_AMOUNT, amountOfRead);
                    request.setAttribute(COMMENTS_AMOUNT, comments.size());
                    return VIEW_PROFILE_PAGE;
                } else {
                    request.setAttribute(INVALID_DATA, YES);
                    logger.warn("Invalid profile data received.");
                    request.setAttribute(USER, UserService.findUserById(userId));
                    page = EDIT_PROFILE_PAGE;
                }
            } catch (ServiceException e){
                throw new CommandException("Exception in EditProfileCommand", e);
            }
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
        return page;
    }
}
