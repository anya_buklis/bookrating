package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Book;
import by.epam.bookrating.entity.Comment;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.BookService;
import by.epam.bookrating.service.CommentService;
import by.epam.bookrating.service.RatingService;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for deleting user's rating<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class DeleteRatingCommand implements ICommand {
    private static Logger logger = Logger.getLogger(DeleteRatingCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        if (isUser(request)) {
            long userId = Long.parseLong(session.getAttribute(USER_ID).toString());
            long bookId = Long.parseLong(request.getParameter(BOOK_ID));
            try {
                RatingService.deleteRating(userId, bookId);
                double avgRating = RatingService.findAvgRatingByBookId(bookId);
                Map<Comment, User> comments = CommentService.findCommentsByBookId(bookId);
                Book book = BookService.findBookById(bookId);

                boolean isBookInRead = UserService.isBookInRead(userId, bookId);
                if (isBookInRead){
                    request.setAttribute(IN_READ, YES);
                }

                boolean isBookInFavorite = UserService.isBookInFavorite(userId, bookId);
                if (isBookInFavorite){
                    request.setAttribute(IN_FAVORITE, YES);
                }
                request.setAttribute(BOOK_ITEM, book);
                request.setAttribute(COMMENTS, comments);
                request.setAttribute(AVG_RATING, avgRating);
                request.setAttribute(RATING, NO_RATING);
            } catch (ServiceException e){
                throw new CommandException("Exception in DeleteRatingCommand.", e);
            }
            return SINGLE_BOOK_PAGE;
        } else {
            logger.warn("Insufficient rights to do this action.");
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
