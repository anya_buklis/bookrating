package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.LOCALE;
import static by.epam.bookrating.command.Constant.MAIN_PAGE;

/**
 * Command for changing locale<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class ChangeLocaleCommand implements ICommand {
    private static Logger logger = Logger.getLogger(ChangeLocaleCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String locale = request.getParameter(LOCALE).toLowerCase();
        request.getSession().setAttribute(LOCALE, locale);
        logger.info("Locale is changed to " + locale);
        return MAIN_PAGE;
    }
}
