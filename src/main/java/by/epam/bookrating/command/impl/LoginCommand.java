package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for signing in<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class LoginCommand implements ICommand {
    private static Logger logger = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (!isAdmin(request) && !isUser(request)) {
            HttpSession session = request.getSession();
            String login = request.getParameter(LOGIN);
            String password = request.getParameter(PASSWORD);
            String page = MAIN_PAGE;
            logger.info("Previous page: " + session.getAttribute(PREVIOUS_PAGE));
            String previousPage = (String) session.getAttribute(PREVIOUS_PAGE);
            try {
                if (session.getAttribute(USER) != null) {
                    String locale = (String) session.getAttribute(LOCALE);
                    session.invalidate();
                    session.setAttribute(LOCALE, locale.toLowerCase());
                }
                if (UserService.login(login, password)) {
                    User user = UserService.findUserByLogin(login);
                    request.getSession(true).setAttribute(LOGIN, user.getLogin());
                    session.setAttribute(USER_ID, user.getUserId());
                    session.setAttribute(ROLE, user.getRole());
                    session.removeAttribute(INCORRECT_LOGIN_DATA);
                    if (previousPage != null && previousPage.equalsIgnoreCase(SINGLE_BOOK_PAGE)) {
                        page = previousPage;
                    }
                } else {
                    page = LOGIN_PAGE;
                }
            } catch (ServiceException e) {
                throw new CommandException("Exception in LoginCommand.", e);
            }
            logger.info("Return to the page " + page);
            return page;
        } else {
            logger.info("Insufficient rights to do this action.");
            request.getSession().setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
