package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.User;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.UserService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for registering user in the system<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class RegistrationCommand implements ICommand {
    private static Logger logger = Logger.getLogger(RegistrationCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (!isAdmin(request) && !isUser(request)) {
            String page = MAIN_PAGE;
            String name = request.getParameter(NAME);
            String login = request.getParameter(LOGIN);
            String password = request.getParameter(PASSWORD);
            String repeatPassword = request.getParameter(PASSWORD_CONFIRM);
            if (Validator.isLoginValid(login) && Validator.isUserNameValid(name) &&
                    Validator.isPasswordValid(password) && password.equals(repeatPassword)) {
                try {
                    if (UserService.registerUser(name, login, password)) {
                        request.getSession(true).setAttribute(LOGIN, login);
                        User user = UserService.findUserByLogin(login);
                        request.getSession().setAttribute(ROLE, USER);
                        request.getSession().setAttribute(USER, user);
                        request.getSession().setAttribute(USER_ID, user.getUserId());
                        request.getSession().removeAttribute(INVALID_REGISTRATION_DATA);
                        request.getSession().removeAttribute(USER_EXISTS);
                    } else {
                        request.getSession().removeAttribute(INVALID_REGISTRATION_DATA);
                        request.getSession().setAttribute(USER_EXISTS, YES);
                        page = REGISTRATION_PAGE;
                    }
                } catch (ServiceException e) {
                    throw new CommandException("Exception in RegistrationCommand", e);
                }
            } else {
                request.getSession().removeAttribute(USER_EXISTS);
                request.getSession().setAttribute(INVALID_REGISTRATION_DATA, YES);
                logger.warn("Invalid registration data received.");
                page = REGISTRATION_PAGE;
            }
            return page;
        } else {
            logger.info("Insufficient rights to do this action.");
            request.getSession().setAttribute(INSUFFICIENT_RIGHTS, YES);
            return ERROR_PAGE;
        }
    }
}
