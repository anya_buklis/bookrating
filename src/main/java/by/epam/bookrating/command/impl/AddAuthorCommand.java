package by.epam.bookrating.command.impl;

import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.entity.Author;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ServiceException;
import by.epam.bookrating.service.AuthorService;
import by.epam.bookrating.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

import static by.epam.bookrating.command.Constant.*;

/**
 * Command for adding author<br/>
 * Implements {@link by.epam.bookrating.command.ICommand}
 * @author Anna Buklis
 */
public class AddAuthorCommand implements ICommand {
    private static Logger logger = Logger.getLogger(AddAuthorCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        if (isAdmin(request)) {
            String fullName = request.getParameter(FULL_NAME);
            String birthYear = request.getParameter(BIRTH_YEAR);
            String birthCountry = request.getParameter(BIRTH_COUNTRY);
            String biography = request.getParameter(BIOGRAPHY);
            if (Validator.isBiographyValid(biography) && Validator.isBirthYearValid(birthYear) &&
                    Validator.isFullNameValid(fullName) && Validator.isBirthCountryValid(birthCountry)){
                String absoluteDiskPath = request.getServletContext().getRealPath(ABSOLUTE_PATH);
                Part imgPart = null;
                try {
                    imgPart = request.getPart(IMAGE_URL);
                } catch (IOException | ServletException e) {
                    logger.warn("Exception during getting image part from jsp.", e);
                }
                int birthYearInt = Integer.parseInt(birthYear);
                try {
                    if (!AuthorService.addAuthor(fullName, birthYearInt, birthCountry,
                            biography, imgPart, absoluteDiskPath)) {
                        page = ADD_AUTHOR_PAGE;
                        request.setAttribute(AUTHOR_EXISTS, YES);
                    } else {
                        long authorId = AuthorService.findAuthorIdByFullName(fullName);
                        Author author = AuthorService.findAuthorById(authorId);
                        request.setAttribute(AUTHOR, author);
                        page = AUTHOR_INFO_PAGE;
                    }
                } catch (ServiceException e) {
                    throw new CommandException("Exception in AddAuthorCommand.", e);
                }
            } else {
                request.setAttribute(INVALID_DATA, YES);
                logger.warn("Invalid author data received.");
                page = ADD_AUTHOR_PAGE;
            }
        } else {
            request.setAttribute(INSUFFICIENT_RIGHTS, YES);
            logger.warn("Insufficient rights.");
            page = ERROR_PAGE;
        }
        return page;
    }
}