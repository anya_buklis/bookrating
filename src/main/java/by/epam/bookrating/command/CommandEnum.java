package by.epam.bookrating.command;

import by.epam.bookrating.command.impl.*;

/**
 * Class-container of commands
 * return current command
 * @author Anna Buklis
 */

public enum CommandEnum {
    LOGIN(new LoginCommand()),
    LOGIN_PAGE(new LoginPageCommand()),
    LOGOUT(new LogoutCommand()),
    VIEW_SINGLE(new ViewSingleBookCommand()),
    ADD_BOOK_PAGE(new AddBookPageCommand()),
    ADD_BOOK(new AddBookCommand()),
    DELETE_ENTITY(new DeleteEntityCommand()),
    VIEW_ALL_BOOKS(new ViewAllBooksCommand()),
    EDIT_BOOK_PAGE(new EditBookPageCommand()),
    EDIT_BOOK(new EditBookCommand()),
    REGISTRATION(new RegistrationCommand()),
    LEAVE_COMMENT(new LeaveCommentCommand()),
    LEAVE_RATING(new LeaveRatingCommand()),
    VIEW_PROFILE(new ViewProfileCommand()),
    EDIT_PROFILE_PAGE(new EditProfilePageCommand()),
    EDIT_PROFILE(new EditProfileCommand()),
    FIND_BOOKS_BY_GENRE(new FindBooksByGenreCommand()),
    VIEW_TODAY_COMMENTS(new ViewTodayCommentsCommand()),
    DELETE_COMMENT(new DeleteCommentCommand()),
    VIEW_USER_STATISTICS(new ViewUserStatisticsCommand()),
    BAN_USER(new BanUserCommand()),
    ADD_BOOK_TO_LIST(new AddBookToListCommand()),
    DELETE_BOOK_FROM_READ(new DeleteBookFromReadCommand()),
    DELETE_BOOK_FROM_FAVORITE(new DeleteBookFromFavoriteCommand()),
    VIEW_USER_BOOKS(new ViewUserBooksCommand()),
    ADD_AUTHOR_PAGE(new AddAuthorPageCommand()),
    ADD_AUTHOR(new AddAuthorCommand()),
    VIEW_AUTHOR(new ViewAuthorCommand()),
    EDIT_AUTHOR_PAGE(new EditAuthorPageCommand()),
    EDIT_AUTHOR(new EditAuthorCommand()),
    CHANGE_LOCALE(new ChangeLocaleCommand()),
    REGISTRATION_PAGE(new RegistrationPageCommand()),
    VIEW_USER_COMMENTS(new ViewUserCommentsCommand()),
    SEARCH(new SearchCommand()),
    DELETE_RATING(new DeleteRatingCommand()),
    VIEW_ALL_AUTHORS(new ViewAuthorsStatisticsCommand()),
    VIEW_BOOK_STATISTICS(new ViewBooksStatisticsCommand());

    private ICommand command;

    CommandEnum(ICommand instance) {
        this.command = instance;
    }

    public ICommand getCurrentCommand() {
        return command;
    }
}