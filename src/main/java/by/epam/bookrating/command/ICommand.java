package by.epam.bookrating.command;

import by.epam.bookrating.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static by.epam.bookrating.command.Constant.ROLE;
import static by.epam.bookrating.command.Constant.USER_ID;

/**
 * Interface {@code ICommand} is the interface that consist of method dealing with
 * HttpRequest and HttpResponse
 * @author Anna Buklis
 */

public interface ICommand {

    /**
     * Receives request, retrieve parameters from it, process them,
     * works with session, and sets attributes
     * @param request HttpServletRequest
     * @return page url of jsp to which forward should be done
     * @throws CommandException
     */
    String execute(HttpServletRequest request) throws CommandException;

    /**
     * Base method. Checks if user in session is admin
     * @param request HttpServletRequest
     * @return true if user is admin, false otherwise
     */
    default boolean isAdmin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(ROLE) != null) {
            String role = String.valueOf(session.getAttribute(ROLE));
            String adminRole = RoleEnum.ADMIN.name().toLowerCase();
            return adminRole.equalsIgnoreCase(role);
        } else return false;
    }

    /**
     * Base method. Checks if user in session is user
     * @param request HttpServletRequest
     * @return true if user is admin, false otherwise
     */
    default boolean isUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(ROLE) != null) {
            String role = String.valueOf(session.getAttribute(ROLE));
            String userRole = RoleEnum.USER.name().toLowerCase();
            String bannedRole = RoleEnum.BANNED.name().toLowerCase();
            return userRole.equalsIgnoreCase(role) || bannedRole.toLowerCase().equalsIgnoreCase(role);
        } else return false;
    }

    default boolean isSameUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(USER_ID) != null) {
            long userIdSession = (long) session.getAttribute(USER_ID);
            long userId = Long.parseLong(request.getParameter(USER_ID));
            return userId == userIdSession;
        } else return false;
    }
}