package by.epam.bookrating.command;

/**
 * Enumeration consists of available
 * roles in application.
 * @author Anna Buklis
 */
public enum RoleEnum {
    ADMIN, USER, BANNED
}
