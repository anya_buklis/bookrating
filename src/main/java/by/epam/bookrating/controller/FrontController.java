package by.epam.bookrating.controller;

import by.epam.bookrating.command.CommandEnum;
import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.exception.CommandException;
import by.epam.bookrating.exception.ConnectionPoolException;
import by.epam.bookrating.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.bookrating.command.Constant.*;


/**
 * Class {@code FrontController} is the class, that extends {@code HttpServlet} class to
 * deal with get and post-requests.
 * @author Anna Buklis
 */

@WebServlet("/controller")
@MultipartConfig
public class FrontController extends HttpServlet {
    private static Logger logger = Logger.getLogger(FrontController.class);
    private static final String PARAMETER_COMMAND = "command";
    private static final String SINGLE_BOOK_REDIRECT_PATH = "/controller?command=view_single&bookId=";
    private static final String DEFAULT_COMMAND_PATH = "/controller?command=view_all_books";
    private static final String LOGIN_REDIRECT_PATH = "/controller?command=login_page";
    private static final String REGISTRATION_REDIRECT_PATH = "/controller?command=registration_page";

    /**
     * Processes GET-requests and forwards to a certain page
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        try {
            ICommand command = CommandHelper.defineCommand(request);
            page = command.execute(request);
        } catch (CommandException e) {
            logger.trace("Error is occurred during processing command. ", e);
            page = ERROR_PAGE;
        }
        logger.info("Forwarding to the page " + page);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    /**
     * Processes POST-requests and forwards to a certain page
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Destroys connection pool
     */
    @Override
    public void destroy(){
        try{
            ConnectionPool.getInstance().destroyConnectionPool();
        } catch (ConnectionPoolException e){
            logger.error("Exception occurred during destroying ConnectionPool in FC", e);
        }
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        try {
            ICommand command = CommandHelper.defineCommand(request);
            String commandAction = request.getParameter(PARAMETER_COMMAND).toUpperCase();
            if (commandAction.equals(CommandEnum.LEAVE_COMMENT.name()) ||
                    commandAction.equals(CommandEnum.LEAVE_RATING.name())) {
                long bookId = Long.parseLong(request.getParameter(BOOK_ID));
                command.execute(request);
                response.sendRedirect(request.getContextPath() + SINGLE_BOOK_REDIRECT_PATH + bookId);
            } else if (commandAction.equals(CommandEnum.LOGIN.name())) {
                page = command.execute(request);
                switch (page) {
                    case ERROR_PAGE:
                        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                        dispatcher.forward(request, response);
                        break;
                    case SINGLE_BOOK_PAGE:
                        long bookId = (Long) request.getSession().getAttribute(BOOK_ID);
                        response.sendRedirect(request.getContextPath() + SINGLE_BOOK_REDIRECT_PATH + bookId);
                        break;
                    case LOGIN_PAGE:
                        HttpSession session = request.getSession();
                        session.setAttribute(INCORRECT_LOGIN_DATA, YES);
                        response.sendRedirect(request.getContextPath() + LOGIN_REDIRECT_PATH);
                        break;
                    default:
                        response.sendRedirect(request.getContextPath() + DEFAULT_COMMAND_PATH);
                        break;
                }
            } else if (commandAction.equals(CommandEnum.REGISTRATION.name())) {
                page = command.execute(request);
                if (request.getSession().getAttribute(INSUFFICIENT_RIGHTS) != null) {
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                    dispatcher.forward(request, response);
                }
                if (request.getSession().getAttribute(INVALID_REGISTRATION_DATA) != null
                        || request.getSession().getAttribute(USER_EXISTS) != null) {
                    response.sendRedirect(request.getContextPath() + REGISTRATION_REDIRECT_PATH);
                } else {
                    response.sendRedirect(request.getContextPath() + DEFAULT_COMMAND_PATH);
                }
            } else {
                page = command.execute(request);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
            }
        } catch (CommandException e) {
            logger.trace("Error is occurred during processing command. ", e);
            page = ERROR_PAGE;
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        }
    }

}
