package by.epam.bookrating.controller;

import by.epam.bookrating.command.CommandEnum;
import by.epam.bookrating.command.ICommand;
import by.epam.bookrating.command.impl.EmptyCommand;
import by.epam.bookrating.exception.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class for defining command received from request
 * @author Anna Buklis
 */
class CommandHelper {
    private static final String PARAMETER_COMMAND = "command";
    private static Logger logger = Logger.getLogger(CommandHelper.class);

    /**
     * Returns appropriate instance that implements {@link by.epam.bookrating.command.ICommand}
     * @param request HttpRequest, from which command parameter should be retrieved
     * @return command appropriate instance, or instance of
     * {@link by.epam.bookrating.command.impl.EmptyCommand} if wrong
     * command parameter received
     * @throws CommandException
     */
    static ICommand defineCommand(HttpServletRequest request) throws CommandException {
        ICommand currentCommand = new EmptyCommand();
        String action = request.getParameter(PARAMETER_COMMAND);
        logger.info("Received action : "+ action);
        if (action == null || action.isEmpty()) {
            return currentCommand;
        }
        try {
            CommandEnum currentAction = CommandEnum.valueOf(action.toUpperCase());
            currentCommand = currentAction.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            throw new CommandException("Wrong command received.", e);
        }
        return currentCommand;
    }
}
