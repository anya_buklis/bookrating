<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.errorText" var="errorText"/>
    <fmt:message bundle="${loc}" key="locale.goToMain" var="goToMain"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.insufficientRights" var="insufficientRightsText"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>
    <div class="container-fluid book-list">
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <div class="text-center blue-text fail-title">
                ${errorText}
                </div>
            </div>
            <div class="col-md-4"></div>
         </div>
        <c:if test="${not empty insufficientRights}">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="text-center">${insufficientRightsText}.</div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </c:if>
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-2">
                <a class="home-back-link" href="controller?command=view_all_books">${goToMain}</a>
            </div>
            <div class="col-md-5"></div>
        </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>