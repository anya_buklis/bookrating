<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.fullName" var="fullName"/>
    <fmt:message bundle="${loc}" key="locale.birthYear" var="birthYear"/>
    <fmt:message bundle="${loc}" key="locale.birthCountry" var="birthCountry"/>
    <fmt:message bundle="${loc}" key="locale.more" var="more"/>
    <fmt:message bundle="${loc}" key="locale.edit" var="edit"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.authorsStatisticsTitle" var="authorsStatisticsTitle"/>
    <fmt:message bundle="${loc}" key="locale.loginText" var="loginText"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.deleteAuthorAndHisBooks" var="deleteAuthorAndHisBooks"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>

<div class="container-fluid book-list">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h3 class="blue-text text-center">${authorsStatisticsTitle}: </h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>${fullName}</th>
                        <th>${birthYear}</th>
                        <th>${birthCountry}</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${authors}" var="author">
                    <tr>
                        <td>${author.fullName}</td>
                        <td>
                            <c:choose>
                                <c:when test="${author.birthYear == 0}">
                                    -
                                </c:when>
                                <c:otherwise>
                                    ${author.birthYear}
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${empty author.birthCountry}">
                                    -
                                </c:when>
                                <c:otherwise>
                                    ${author.birthCountry}
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td><a href="controller?command=view_author&authorId=${author.authorId}" class="authors-statistics-link">${more}</a></td>
                        <td><a href="controller?command=edit_author_page&authorId=${author.authorId}" class="authors-statistics-link">${edit}</a></td>
                        <td><a href="controller?command=delete_entity&entity=author&authorId=${author.authorId}" class="authors-statistics-link">${deleteAuthorAndHisBooks}</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-2"></div>

        <div class="pagination">
            <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="1" action="controller?command=view_all_authors"/>
        </div>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>