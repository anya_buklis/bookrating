<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.comments" var="commentsText"/>
    <fmt:message bundle="${loc}" key="locale.publishingYear" var="publishingYear"/>
    <fmt:message bundle="${loc}" key="locale.genres" var="genresText"/>
    <fmt:message bundle="${loc}" key="locale.description" var="description"/>
    <fmt:message bundle="${loc}" key="locale.inRead" var="inReadText"/>
    <fmt:message bundle="${loc}" key="locale.inFavorite" var="inFavoriteText"/>
    <fmt:message bundle="${loc}" key="locale.toFavorite" var="toFavorite"/>
    <fmt:message bundle="${loc}" key="locale.toRead" var="toRead"/>
    <fmt:message bundle="${loc}" key="locale.noComments" var="noComments"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.toLeaveCommentText" var="toLeave"/>
    <fmt:message bundle="${loc}" key="locale.enter" var="enter"/>
    <fmt:message bundle="${loc}" key="locale.theSystem" var="theSystem"/>
    <fmt:message bundle="${loc}" key="locale.bannedText" var="bannedText"/>
    <fmt:message bundle="${loc}" key="locale.edit" var="edit"/>
    <fmt:message bundle="${loc}" key="locale.leave" var="leave"/>
    <fmt:message bundle="${loc}" key="locale.delete" var="delete"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.commentText" var="commentText"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.commentsAdminText" var="commentsAdminText"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <fmt:message bundle="${loc}" key="locale.invalidCommentData" var="invalidCommentDataText"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="../js/jquery.min.js"></script>

    <script src="../js/angular.min.js"></script>
    <script src="../js/controller.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>

<div class="container-fluid">
    <div class="row book-item-info">
          <div class="col-md-2"></div>
          <div class="col-md-4 col-lg-4 col-xl-4 tm-news-item-img-container">
              <c:choose>
                  <c:when test="${empty book.imageUrl}">
                      <img src="../img/book.png" alt="" class="single-book-img">
                  </c:when>
                  <c:otherwise>
                      <img src="${book.imageUrl}" alt="Image" class="single-book-img">
                  </c:otherwise>
              </c:choose>
            <c:choose>
                <c:when test="${avgRating == 0 or empty avgRating}"></c:when>
                <c:otherwise>
                    <span class="popular-star star-single-book">
                        <img src="img/star.png" alt="" class="popular-star-image">
                        <span class="popular-star-number">${avgRating}</span>
                    </span>
                </c:otherwise>
            </c:choose>
        </div>
<div class="col-md-4 col-lg-4 col-xl-4 tm-news-container">
<h2 class="tm-news-title dark-gray-text">
    <div class="book-item-title-author">
        <c:forEach items="${book.authors}" var="author" varStatus="authorLoop">
           <a href="controller?command=view_author&authorId=${author.authorId}" class="author-link">${author.fullName}</a> ${!authorLoop.last ? ', ' : ''}
        </c:forEach>
    </div>
    <br>"${book.title}"</h2>
<p><span class="dark-text field-title">${publishingYear}: </span>${book.publishingYear}</p>
<p><span class="dark-text field-title">${genresText}: </span>
    <c:forEach items="${book.genres}" var="genre" varStatus="genreLoop">
        <a href="controller?command=find_books_by_genre&genre=${genre.genreId}">${genre.genreName}</a> ${!genreLoop.last ? ', ' : ''}
    </c:forEach>
</p>
<p class="tm-news-text"><span class="dark-text field-title">${description}: </span>${book.description}</p>
    <c:choose>
        <c:when test="${sessionScope.role == userRole}">
            <form name="leaveCommentForm" action="controller" method="POST" >
                <input type="hidden" name="command" value="leave_comment"/>
                <input type="hidden" name="bookId" value="${book.bookId}"/>
                <input type="hidden" name="userId" value="${sessionScope.user.userId}"/>
                <div class="form-group" ng-class="{'has-error' : leaveCommentForm.commentText.$invalid && !leaveCommentForm.commentText.$pristine }">
                    <textarea class="form-control comment-area" rows="3" name ="commentText" ng-model="commentText" ng-minlength="10" ng-maxlength="2000" required></textarea>
                    <div class="reg-error-message">${commentText}</div>
                    <c:if test="${not empty invalidCommentField}">
                        <div class="error-text">${invalidCommentDataText}</div>
                    </c:if>
                </div>
                <input type="submit" class="btn btn-primary button tm-gray-btn  alt leave-comment-btn" ng-disabled="leaveCommentForm.$invalid" value="${leave}"/>
            </form>
        </c:when>
        <c:when test="${sessionScope.role == banned}">
            <div class="comments-title banned-comments">${bannedText}</div>
        </c:when>
    </c:choose>
</div>
<div class="col-md-2">
<div class="row">
        <c:if test="${sessionScope.role == userRole}">
            <c:choose>
              <c:when test="${rating == 0 or empty rating}">
                <form action="controller" method="POST">
                    <input type="hidden" name="command" value="leave_rating"/>
                    <input type="hidden" name="bookId" value="${book.bookId}"/>
                    <input type="hidden" name="userId" value="${sessionScope.userId}"/>
                    <span class="star-cb-group">
                        <input type="radio" id="rating-5" name="rating" value="5" /><label for="rating-5">5</label>
                        <input type="radio" id="rating-4" name="rating" value="4" /><label for="rating-4">4</label>
                        <input type="radio" id="rating-3" name="rating" value="3" /><label for="rating-3">3</label>
                        <input type="radio" id="rating-2" name="rating" value="2" /><label for="rating-2">2</label>
                        <input type="radio" id="rating-1" name="rating" value="1" /><label for="rating-1">1</label>
                        <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" /><label for="rating-0">0</label>
                    </span>
                </form>
              </c:when>
              <c:otherwise>
                <div class="rating-text">${rating}/5 <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                    <a href="controller?command=delete_rating&bookId=${book.bookId}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></div>
              </c:otherwise>
            </c:choose>
        </c:if>
</div>

<c:choose>
    <c:when test="${sessionScope.role == admin}">
        <div class="row">
            <a href="controller?command=edit_book_page&bookId=${book.bookId}" class="btn btn-default tm-normal-btn tm-gray-btn admin-book-btn">${edit}</a>
        </div>
    </c:when>

    <c:when test="${sessionScope.role == userRole}">
        <c:choose>
            <c:when test="${not empty inRead}">
                <div class="row">
                    <span class="in-list-book">
                            ${inReadText}
                        <a href="controller?command=delete_book_from_read&bookId=${book.bookId}&previousPage=${pageContext.request.requestURI}">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </span>
                 </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <a href="controller?command=add_book_to_list&list=read&bookId=${book.bookId}">${toRead}</a>
                </div>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${not empty inFavorite}">
                <div class="row">
                     <span class="in-list-book">
                             ${inFavoriteText}
                          <a href="controller?command=delete_book_from_favorite&bookId=${book.bookId}&previousPage=${pageContext.request.requestURI}">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                          </a>
                     </span>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <a href="controller?command=add_book_to_list&list=favorite&bookId=${book.bookId}">${toFavorite}</a>
                </div>
            </c:otherwise>
        </c:choose>
    </c:when>
</c:choose>
</div>
</div>
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8 comments-section">
<c:choose>
  <c:when test="${comments.size()!=0 and not empty comments}">
      <div class="comments-title">
          ${commentsText} :
      </div>
      <c:forEach items="${comments}" var="comment">
          <article class="box comment-box">
              <a href="controller?command=view_profile&userId=${comment.value.userId}" class="comment-user-link">${comment.value.login}</a>
              <span class="user-comment-date"> ${comment.key.commentDate}</span>
              <c:if test="${sessionScope.role == admin}">
                      <a href="controller?command=delete_comment&commentId=${comment.key.commentId}&bookId=${book.bookId}&previousPage=${pageContext.request.requestURI}" class="delete-comment-cross" >
                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                      </a>
              </c:if>
              <p>${comment.key.commentText}</p>
          </article>
      </c:forEach>
  </c:when>
  <c:otherwise>
      <c:choose>
          <c:when test="${sessionScope.role == admin}">
              <div class="comments-title">${commentsAdminText}</div>
          </c:when>
          <c:when test="${sessionScope.role == userRole}" >
              <div class="comments-title">${noComments} :)</div>
          </c:when>
          <c:when test="${sessionScope.role == banned}"></c:when>
          <c:otherwise>
              <div class="comments-title">
                  <div class="comments-title">${noComments} :)</div>
                  <div class="comments-title"> ${toLeave} <a href="controller?command=login_page" class="enter-link">${enter}</a> ${theSystem}
                  </div>
              </div>
          </c:otherwise>
      </c:choose>
  </c:otherwise>
</c:choose>
</div>
<div class="col-md-2"></div>
</div>
</div>
<%@include file="footer.jsp"%>
<script>
    $('input[type=radio]').on('change', function() {
        $(this).closest("form").submit();
    });
</script>
</body>
</html>