<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.fullName" var="fullName"/>
    <fmt:message bundle="${loc}" key="locale.birthYear" var="birthYear"/>
    <fmt:message bundle="${loc}" key="locale.biography" var="biography"/>
    <fmt:message bundle="${loc}" key="locale.birthCountry" var="birthCountry"/>
    <fmt:message bundle="${loc}" key="locale.image" var="image"/>
    <fmt:message bundle="${loc}" key="locale.imgText" var="imgText"/>
    <fmt:message bundle="${loc}" key="locale.noBooksByAuthor" var="noBooksByAuthor"/>
    <fmt:message bundle="${loc}" key="locale.more" var="more"/>
    <fmt:message bundle="${loc}" key="locale.edit" var="edit"/>
    <fmt:message bundle="${loc}" key="locale.delete" var="delete"/>
    <fmt:message bundle="${loc}" key="locale.booksByAuthor" var="booksByAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.authorInfoNotSpecified" var="authorInfoNotSpecified"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>
    <div class="container-fluid book-list">
        <article class="box post single-book">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <img src="${author.imageUrl}" alt="" class="author-img"/>
                </div>
                <div class="col-md-5">
                    <h2 class="book-author">${author.fullName} </h2>
                    <c:choose>
                        <c:when test="${empty author.biography and empty author.birthCountry and author.birthYear eq 0}">
                            ${authorInfoNotSpecified}
                        </c:when>
                        <c:otherwise>
                            <p class="book-title"> ${author.birthYear} , ${author.birthCountry}</p>
                            <p>${author.biography}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-md-2">
                    <c:choose>
                        <c:when test="${sessionScope.role == admin}">
                            <div class="row">
                                <a href="controller?command=edit_author_page&authorId=${author.authorId}" class="btn btn-default tm-normal-btn tm-gray-btn admin-book-btn">${edit}</a>
                            </div>
                        </c:when>
                        <c:otherwise>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </article>

        <div class="row">
            <c:choose>
                <c:when test="${books.size() != 0 and not empty books}">
                    <h3 class="blue-text left-padding">${booksByAuthor}: </h3>
                    <c:forEach items="${books}" var="bookItem">
                        <div class="col-md-4 margin-bottom-sm-3">
                            <div class="tm-3-col-box gray-bg book-item">
                                <c:choose>
                                    <c:when test="${empty bookItem.imageUrl}">
                                        <img src="../img/book.png" alt="" class="book-img">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${bookItem.imageUrl}" alt="Image" class="book-img">
                                    </c:otherwise>
                                </c:choose>
                                <div class="book-item-title-author">${bookItem.title}</div>
                                <div class="">
                                    <p class="tm-description-text desc-short-text">${bookItem.description}</p>
                                    <div class="book-item-btn-more">
                                        <a href="controller?command=view_single&bookId=${bookItem.bookId}"
                                           class="btn btn-default tm-normal-btn tm-gray-btn add-author-btn">${more}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <h3 class="blue-text text-center">${noBooksByAuthor}</h3>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="pagination">
            <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="1" action="controller?command=view_author&authorId=${author.authorId}"/>
        </div>
    </div>
<%@include file="footer.jsp"%>
</body>
</html>
