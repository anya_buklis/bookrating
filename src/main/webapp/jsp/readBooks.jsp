<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.readBooks" var="readBooks"/>
    <fmt:message bundle="${loc}" key="locale.more" var="more"/>
    <fmt:message bundle="${loc}" key="locale.backToProfile" var="backToProfile"/>
    <fmt:message bundle="${loc}" key="locale.noBooksText" var="noBooks"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">

<%@include file="header.jsp"%>
<div class="container-fluid book-list">
    <div class="row">
        <div class="col-md-3">
            <a href="controller?command=view_profile&userId=${userId}" class="back-to-profile-link">${backToProfile}</a>
        </div>
        <div class="col-md-6">
            <h2 class="blue-text text-center"> ${readBooks}: </h2>
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="row">
        <c:choose>
            <c:when test="${books.size()!=0}">
                <c:forEach items="${books}" var="bookItem">
                    <div class="col-md-4 margin-bottom-sm-3">
                        <div class="tm-3-col-box gray-bg book-item">
                            <c:choose>
                                <c:when test="${empty bookItem.imageUrl}">
                                    <img src="../img/book.png" alt="" class="book-img">
                                </c:when>
                                <c:otherwise>
                                    <img src="${bookItem.imageUrl}" alt="Image" class="book-img">
                                </c:otherwise>
                            </c:choose>
                            <div class="book-title">
                                    ${bookItem.title}
                            </div>
                            <a href="controller?command=delete_book_from_read&bookId=${bookItem.bookId}&previous=${pageContext.request.requestURI}" class="delete-book-from-list">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </a>

                            <div class="book-item-title-author">
                                <c:forEach items="${bookItem.authors}" var="author" varStatus="authorLoop">
                                    <a href="controller?command=view_author&authorId=${author.authorId}">${author.fullName}</a> ${!authorLoop.last ? ', ' : ''}
                                </c:forEach>
                            </div>

                            <div>
                                <p class="tm-description-text desc-short-text">${bookItem.description}</p>
                                <div class="book-item-btn-more">
                                    <a href="controller?command=view_single&bookId=${bookItem.bookId}"  class="btn btn-default tm-normal-btn tm-gray-btn add-author-btn">${more}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="text-center">
                    <h3 class="text-center">${noBooks}</h3>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="pagination">
        <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="1" action="controller?command=view_user_books&userId=${userId}&type=read"/>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>