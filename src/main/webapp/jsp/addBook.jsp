<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.fullNameText" var="fullNameText"/>
    <fmt:message bundle="${loc}" key="locale.image" var="image"/>
    <fmt:message bundle="${loc}" key="locale.authors" var="authors"/>
    <fmt:message bundle="${loc}" key="locale.title" var="title"/>
    <fmt:message bundle="${loc}" key="locale.authors" var="author"/>
    <fmt:message bundle="${loc}" key="locale.description" var="description"/>
    <fmt:message bundle="${loc}" key="locale.publishingYear" var="pubYear"/>
    <fmt:message bundle="${loc}" key="locale.genres" var="genresTitle"/>
    <fmt:message bundle="${loc}" key="locale.imgText" var="imgText"/>
    <fmt:message bundle="${loc}" key="locale.save" var="save"/>
    <fmt:message bundle="${loc}" key="locale.invalidBookData" var="invalidBookData"/>
    <fmt:message bundle="${loc}" key="locale.addBookTitle" var="addBookTitle"/>
    <fmt:message bundle="${loc}" key="locale.pubYearAddText" var="pubYearAddText"/>
    <fmt:message bundle="${loc}" key="locale.descAddText" var="descAddText"/>
    <fmt:message bundle="${loc}" key="locale.titleAddText" var="titleAddText"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.bookExists" var="bookExistsText"/>
    <fmt:message bundle="${loc}" key="locale.genreValidText" var="genreValidText"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.maximumAuthors" var="maximumAuthors"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/addFormEditor.js"></script>
    <script src="../js/validation.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>

<div class="container-fluid">
    <div class="row">

        <section id="tm-section-4" class="tm-section">
            <div class="tm-container">

                <h2 class="blue-text text-center">${addBookTitle}: </h2>

                <c:if test="${not empty bookExists}">
                        <div class="row text-center error-text">${bookExistsText}</div>
                </c:if>

                <c:if test="${not empty invalidData}">
                    <div class="row text-center error-text">${invalidBookData}</div>
                </c:if>

                <form name="addBookForm" action="controller" method="POST" enctype="multipart/form-data" class="tm-contact-form">
                    <input type="hidden" name="command" value="add_book"/>
                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="author">${author}: </label></div>
                        <div class="col-md-6" id="author-field">
                            <input type="text" class="form-control" id="author" name="author" required min="5" max="100" onkeyup="validateAddBookForm()"/>
                            <div class="reg-error-message"> ${fullNameText}, ${maximumAuthors}</div>
                        </div>
                        <div class="col-md-3">
                            <a href="javascript:void(0);" class="addAuthor"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="title">${title}: </label></div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="title" name="title" required min="1" max="200" onkeyup="validateAddBookForm()"/>
                            <div class="reg-error-message">${titleAddText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"> <label for="description">${description}: </label></div>
                        <div class="col-md-6"><textarea class="form-control description-area" rows="3" id="description" name ="description" required
                        maxlength="1500" onkeyup="validateAddBookForm()"></textarea>
                            <div class="reg-error-message">${descAddText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="publishingYear">${pubYear}: </label></div>
                        <div class="col-md-6">
                            <input type="number" class="form-control" id="publishingYear" name="publishingYear" required min="1400" onkeyup="validateAddBookForm()"/>
                            <div class="reg-error-message">${pubYearAddText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="imageUrl">${image}:</label></div>
                        <div class="col-md-6">
                            <input type="file" id="imageUrl" name="imageUrl" accept="image/*">
                            <p class="help-block">${imgText}</p>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view">
                            <label> ${genresTitle}:</label>
                        </div>
                        <div class="col-md-9">
                            <div class="reg-error-message genre-help-text">${genreValidText}</div>
                        </div>
                        <div class="row">
                            <c:forEach items="${genres}" var="genreItem">
                                <div class="col-md-3">
                                    <input type="checkbox" value="${genreItem.genreId}" name="genre" id="${genreItem.genreId}"/>
                                    <label for="${genreItem.genreId}" class="genre-single-label">${genreItem.genreName}</label>
                                </div>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2"><input type="submit" class="btn button tm-green-btn add-author-btn" id="submit-add-book-form" value="${save}"/></div>
                        <div class="col-md-5"></div>
                    </div>
                </form>

            </div>
        </section>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>