<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.banned" var="banned"/>
    <fmt:message bundle="${loc}" key="locale.active" var="active"/>
    <fmt:message bundle="${loc}" key="locale.name" var="name"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.age" var="age"/>
    <fmt:message bundle="${loc}" key="locale.aboutMyself" var="aboutMyself"/>
    <fmt:message bundle="${loc}" key="locale.edit" var="edit"/>
    <fmt:message bundle="${loc}" key="locale.favorite" var="favorite"/>
    <fmt:message bundle="${loc}" key="locale.read" var="read"/>
    <fmt:message bundle="${loc}" key="locale.comments" var="commentsTitle"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>
<div>
    <div class="container-fluid user-profile">
        <div class="row profile-desc">
            <div class="form-group row">
                <div class="col-md-3"></div>
                <div class="col-md-6  user-column">
                    <div class="row profile-item user-title text-center">
                        <span class="user-name">${user.login}</span>
                        <c:choose>
                            <c:when test="${sessionScope.role==banned}">
                                <span class="status banned">${banned}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="status active">${active}</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <c:choose>
                                <c:when test="${not empty user.avatar}">
                                   <img src="${user.avatar}" alt="" class="user-image"/>
                                </c:when>
                                <c:otherwise>
                                   <img src="../img/user.png" alt="" class="user-image"/>
                                </c:otherwise>
                            </c:choose>
                            <div>
                                <c:if test="${user.login == sessionScope.login}">
                                    <a href="controller?command=edit_profile_page&userId=${sessionScope.userId}" class="btn btn-default tm-normal-btn tm-green-btn edit-profile-btn">${edit} <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="row profile-item">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 field-title">${name}:</div>
                        <div class="col-md-6">${user.name}</div>
                        <div class="col-md-2"></div>
                    </div>

                    <div class="row profile-item">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 field-title">${age}:</div>
                        <div class="col-md-6">${user.age}</div>
                        <div class="col-md-2"></div>
                    </div>

                    <div class="row profile-item">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 field-title">${aboutMyself}:</div>
                        <div class="col-md-6">${user.info}</div>
                        <div class="col-md-2"></div>
                    </div>

                    <c:choose>
                        <c:when test="${user.login == sessionScope.login}">
                            <div class="row profile-item user-links-div">
                                <div class="col-md-4">
                                    <a href="controller?command=view_user_books&userId=${user.userId}&type=favorite" class="user-link">${favorite} <span class="rating number">${favoriteBooksAmount}</span></a>
                                </div>
                                <div class="col-md-4">
                                    <a href="controller?command=view_user_books&userId=${user.userId}&type=read" class="user-link">${read} <span class="rating number">${readBooksAmount}</span></a>
                                </div>
                                <div class="col-md-4">
                                    <a href="controller?command=view_user_comments&userId=${user.userId}" class="user-link">${commentsTitle} <span class="rating number">${commentsAmount}</span></a>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="row profile-item user-links-div">
                                <div class="col-md-4">
                                    <span class="user-link">${favorite} <span class="rating number">${favoriteBooksAmount}</span></span>
                                </div>
                                <div class="col-md-4">
                                    <span class="user-link">${read} <span class="rating number">${readBooksAmount}</span></span>
                                </div>
                                <div class="col-md-4">
                                    <span class="user-link">${commentsTitle} <span class="rating number">${commentsAmount}</span></span>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-md-3">
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>