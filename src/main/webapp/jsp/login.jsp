<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.haveNotAccount" var="noAccountText"/>
    <fmt:message bundle="${loc}" key="locale.signup" var="signup"/>
    <fmt:message bundle="${loc}" key="locale.loginErrorText" var="loginErrorText"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.loginText" var="loginText"/>
    <fmt:message bundle="${loc}" key="locale.password" var="password"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="../js/angular.min.js"></script>
    <script src="../js/controller.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>

<div class="container-fluid">
    <div class="row">
        <section id="tm-section-4" class="tm-section">
            <div class="tm-container">
                <c:if test="${not empty incorrectLoginData}">
                    <div class="row text-center error-text">${loginErrorText}.</div>
                </c:if>
                <div class="col-md-4 col-lg-4 col-xl-4"></div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h2 class="blue-text text-center">${login}</h2>
                    <form name="loginForm" action="controller" method="POST" class="tm-contact-form">
                        <input type="hidden" name="command" value="login"/>
                        <div class="form-group" ng-class="{ 'has-error' : loginForm.login.$invalid && !loginForm.login.$pristine }">
                            <input type="text" id="login" name="login" class="form-control" placeholder="${loginText}" ng-model="login" ng-minlength="1" required/>
                        </div>
                        <div class="form-group" ng-class="{ 'has-error' : loginForm.password.$invalid && !loginForm.password.$pristine }">
                            <input type="password" id="password" name="password" class="form-control" placeholder="${password}" ng-model="password" ng-minlength="1" required/>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn tm-green-btn pull-xs-right login-button" ng-disabled="loginForm.$invalid">${login}</button>
                        </div>
                        <div class="row login-link-text">
                            ${noAccountText} <a href="controller?command=registration_page" class="register-link">${signup}!</a>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-lg-4 col-xl-4"></div>
            </div>
        </section>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>