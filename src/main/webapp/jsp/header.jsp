<c:set var="userRole" value="user"/>
<c:set var="banned" value="banned"/>
<c:set var="admin" value="admin"/>

<div class="tm-navbar-container-dark">
    <nav class="navbar navbar-full bg-inverse">
        <ul class="navbar-toggleable-sm" id="tmNavbar">
            <a class="home-link" href="controller?command=view_all_books">${bookrating}</a>
            <c:choose>
                <%--For admin--%>
                <c:when test="${sessionScope.role == admin}">
                    <ul class="nav navbar-nav header-nav">
                        <li class="nav-item">
                            <a href="controller?command=view_book_statistics" class="nav-link">${allBooks}</a></li>
                        <li class="nav-item">
                            <a href="controller?command=view_all_authors" class="nav-link">${allAuthors}</a></li>
                        <li class="nav-item">
                            <a href="controller?command=view_user_statistics" class="nav-link">${userInfo}</a></li>
                        <li class="nav-item">
                            <a href="controller?command=view_today_comments" class="nav-link">${todayComments}</a></li>
                        <li class="nav-item">
                            <a href="controller?command=add_author_page" class="nav-link">${addAuthor}</a></li>
                        <li class="nav-item">
                            <a href="controller?command=add_book_page" class="nav-link">${addBook}</a></li>
                        <li class="nav-item">
                            <a href="controller?command=logout" class="nav-link">${logout}</a>
                        </li>
                    </ul>
                </c:when>

                <c:when test="${sessionScope.role == userRole or sessionScope.role == banned}">
                    <div class="welcome-nav">
                        <span class="welcome-text">${welcomeText}, </span><a href="controller?command=view_Profile&userId=${sessionScope.userId}" class="nav-link login-link">${sessionScope.login}</a>
                        <a href="controller?command=logout" class="nav-link logout-link">${logout}</a>
                    </div>

                    <div class="search">
                        <form method="get" action="controller" name="searchForm">
                            <input type="hidden" name="command" value="search"/>
                            <span ng-class="{'has-error' : searchForm.title.$invalid && !searchForm.title.$pristine }">
                                <input type="text" name="title" placeholder="Search" class="title-input" ng-model="title" ng-minlength="1" required/>
                            </span>
                            <span class="reg-error-message search-title">${searchByTitle}: </span>
                            <input type="submit" class="btn btn-primary button alt search-btn" ng-disabled="searchForm.$invalid" value="${search}">
                        </form>
                    </div>
                </c:when>

                <c:otherwise>
                    <ul class="nav navbar-nav header-nav">
                        <li class="nav-item">
                           <a href="controller?command=registration_page" class="nav-link">${registration}</a>
                        </li>
                        <li class="nav-item">
                           <a href="controller?command=login_page" class="nav-link">${login}</a>
                        </li>
                    </ul>

                    <div class="search">
                        <form method="get" action="controller" name="searchForm">
                            <input type="hidden" name="command" value="search"/>
                            <span ng-class="{'has-error' : searchForm.title.$invalid && !searchForm.title.$pristine }">
                                <input type="text" name="title" placeholder="Search" class="title-input" ng-model="title" ng-minlength="1" required/>
                            </span>
                            <span class="reg-error-message search-title">${searchByTitle}: </span>
                            <input type="submit" class="btn btn-primary button alt search-btn" ng-disabled="searchForm.$invalid" value="${search}">
                        </form>
                    </div>
                </c:otherwise>
            </c:choose>


            <c:if test="${pageContext.request.requestURI eq '/jsp/main.jsp'}">
                <div class="lang">
                    <c:choose>
                        <c:when test="${sessionScope.locale eq 'ru'}">
                            <img src="img/ru.png" class="lang-img" alt="ru" />
                        </c:when>
                        <c:otherwise>
                            <form action="controller" method="post" class="lang-form">
                                <input type="hidden" name="command" value="change_locale"/>
                                <input type="hidden" name="locale" value="ru"/>
                                <button type="submit" class="lang-btn">
                                    <img src="img/ru.png" class="lang-img" alt="ru" />
                                </button>
                            </form>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${sessionScope.locale eq 'en'}">
                            <img src="img/usa.png" class="lang-img" alt="en" />
                        </c:when>
                        <c:otherwise>
                            <form action="controller" method="post" class="lang-form">
                                <input type="hidden" name="command" value="change_locale"/>
                                <input type="hidden" name="locale" value="en"/>
                                <button type="submit" class="lang-btn">
                                    <img src="img/usa.png" class="lang-img" alt="en" />
                                </button>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:if>
        </ul>
    </nav>
</div>