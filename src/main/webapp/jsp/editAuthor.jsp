<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>

    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.fullName" var="fullName"/>
    <fmt:message bundle="${loc}" key="locale.birthYear" var="birthYear"/>
    <fmt:message bundle="${loc}" key="locale.biography" var="biography"/>
    <fmt:message bundle="${loc}" key="locale.birthCountry" var="birthCountry"/>
    <fmt:message bundle="${loc}" key="locale.image" var="image"/>
    <fmt:message bundle="${loc}" key="locale.imgText" var="imgText"/>
    <fmt:message bundle="${loc}" key="locale.invalidAuthorData" var="invalidAuthorData"/>
    <fmt:message bundle="${loc}" key="locale.editAuthorTitle" var="editAuthorTitle"/>
    <fmt:message bundle="${loc}" key="locale.save" var="save"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.fullNameText" var="fullNameText"/>
    <fmt:message bundle="${loc}" key="locale.biographyText" var="biographyText"/>
    <fmt:message bundle="${loc}" key="locale.birthCountryText" var="birthCountryText"/>
    <fmt:message bundle="${loc}" key="locale.birthYearText" var="birthYearText"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.authorExists" var="authorExistsText"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="../js/angular.min.js"></script>
    <script src="../js/controller.js"></script>
    <script src="../js/validation.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>
<div class="container-fluid">
    <div class="row" >

        <section id="tm-section-4" class="tm-section">
            <div class="tm-container">

                <h2 class="blue-text text-center">${editAuthorTitle}</h2>

                <c:if test="${not empty authorExists}">
                    <div class="row text-center error-text">${authorExistsText}.</div>
                </c:if>

                <c:if test="${not empty invalidData}">
                    <div class="row text-center error-text">${invalidAuthorData}</div>
                </c:if>
                <form name="editAuthorForm" action="controller" method="POST" enctype="multipart/form-data" class="tm-contact-form">
                    <input type="hidden" name="command" value="edit_author"/>
                    <input type="hidden" name="oldName" value="${author.fullName}"/>
                    <input type="hidden" name="authorId" value="${author.authorId}"/>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="author">${fullName}: </label></div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="author" name="fullName" value="${author.fullName}" required min="5" max="100" onkeyup="validateEditAuthorForm()"/>
                            <div class="reg-error-message">${fullNameText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="title">${birthYear}: </label></div>
                        <div class="col-md-6">
                            <input type="number" class="form-control" id="title" name="birthYear" required value="${author.birthYear}" min="1000"  onkeyup="validateEditAuthorForm()" />
                            <div class="reg-error-message">${birthYearText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"> <label for="description">${biography}: </label></div>
                        <div class="col-md-6">
                            <textarea class="form-control description-area" rows="3" id="description" name ="biography"  required maxlength="1500"  onkeyup="validateEditAuthorForm()">${author.biography}</textarea>
                            <div class="reg-error-message">${biographyText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="publishingYear">${birthCountry}: </label></div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="publishingYear" name="birthCountry" required value="${author.birthCountry}" min="3" max="60"  onkeyup="validateEditAuthorForm()"/>
                            <div class="reg-error-message">${birthCountryText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="newImage">${image}:</label></div>
                        <div class="col-md-6">
                            <input type="file" id="newImage" name="newImage" accept="image/*">
                            <p class="help-block">${imgText}</p>
                            <input type="hidden" name="oldImage" value="${author.imageUrl}">
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2"><input type="submit" class="btn button tm-green-btn add-author-btn" id="submit_edit_author" value="${save}"/></div>
                        <div class="col-md-5"></div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>
