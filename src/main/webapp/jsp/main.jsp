<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html lang="en">
    <head>
        <fmt:setLocale value="${sessionScope.locale}"/>
        <fmt:setBundle basename="locale" var="loc"/>
        <fmt:message bundle="${loc}" key="locale.popularBooks" var="popularBooksTitle"/>
        <fmt:message bundle="${loc}" key="locale.more" var="more"/>
        <fmt:message bundle="${loc}" key="locale.next" var="next"/>
        <fmt:message bundle="${loc}" key="locale.previous" var="previous"/>
        <fmt:message bundle="${loc}" key="locale.login" var="login"/>
        <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
        <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
        <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
        <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
        <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
        <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
        <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
        <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
        <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
        <fmt:message bundle="${loc}" key="locale.search" var="search"/>
        <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
        <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
        <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>${bookrating}</title>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <script src="../js/angular.min.js"></script>
        <script src="../js/controller.js"></script>
    </head>
    <body ng-app="validationApp" ng-controller="mainController">
    <%@include file="header.jsp"%>

    <div class="container-fluid book-list">
            <c:if test="${popularBooks.size!=0 and not empty popularBooks}">
                <div class="row popular">
                    <div class="blue-text text-center">${popularBooksTitle} : </div>
                    <c:forEach items="${popularBooks}" var="popularBook">
                        <div class="col-md-4 margin-bottom-sm-3">
                            <div class="tm-3-col-box gray-bg book-item">
                                <c:choose>
                                    <c:when test="${empty popularBook.key.imageUrl}">
                                        <img src="../img/book.png" alt="" class="book-img">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${popularBook.key.imageUrl}" alt="Image" class="book-img">
                                    </c:otherwise>
                                </c:choose>
                                <div class="book-title">${popularBook.key.title}</div>
                                <div class="book-item-title-author">
                                    <c:forEach items="${popularBook.key.authors}" var="author" varStatus="authorLoop">
                                        <a href="controller?command=view_author&authorId=${author.authorId}">${author.fullName}</a> ${!authorLoop.last ? ', ' : ''}
                                    </c:forEach>
                                </div>
                                <p class="tm-description-text desc-short-text">${bookItem.description}</p>
                                <div class="book-item-btn-more">
                                    <a href="controller?command=view_single&bookId=${popularBook.key.bookId}" class="btn btn-default tm-normal-btn tm-gray-btn add-author-btn">${more}</a>
                                <span class="popular-star">
                                    <img src="img/star.png" alt="" class="popular-star-image">
                                    <span class="popular-star-number">${popularBook.value}</span>
                                </span>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </c:if>

        <div class="row other-books">
                <c:forEach items="${books}" var="bookItem">
                    <div class="col-md-4 margin-bottom-sm-3 ">
                        <div class="tm-3-col-box gray-bg book-item">
                            <c:choose>
                                <c:when test="${empty bookItem.imageUrl}">
                                    <img src="../img/book.png" alt="" class="book-img">
                                </c:when>
                                <c:otherwise>
                                    <img src="${bookItem.imageUrl}" alt="Image" class="book-img">
                                </c:otherwise>
                            </c:choose>
                            <div class="book-title">
                                    ${bookItem.title}
                            </div>
                            <div class="book-item-title-author">
                                <c:forEach items="${bookItem.authors}" var="author" varStatus="authorLoop">
                                    <a href="controller?command=view_author&authorId=${author.authorId}">${author.fullName}</a> ${!authorLoop.last ? ', ' : ''}
                                </c:forEach>
                            </div>
                            <p class="tm-description-text desc-short-text">${bookItem.description}</p>
                            <div class="book-item-btn-more">
                                <a href="controller?command=view_single&bookId=${bookItem.bookId}"  class="btn btn-default tm-normal-btn tm-gray-btn add-author-btn">${more}</a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
        </div>

        <div class="pagination">
            <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="2" action="controller?command=view_all_books"/>
        </div>

    </div>
    <%@include file="footer.jsp"%>
    </body>
</html>