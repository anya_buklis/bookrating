<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.haveAnAccount" var="accountText"/>
    <fmt:message bundle="${loc}" key="locale.name" var="name"/>
    <fmt:message bundle="${loc}" key="locale.password" var="password"/>
    <fmt:message bundle="${loc}" key="locale.repeatPassword" var="repeatPassword"/>
    <fmt:message bundle="${loc}" key="locale.userExists" var="userExistsText"/>
    <fmt:message bundle="${loc}" key="locale.invalidRegData" var="invalidRegData"/>
    <fmt:message bundle="${loc}" key="locale.loginText" var="loginText"/>
    <fmt:message bundle="${loc}" key="locale.nameRegText" var="nameRegText"/>
    <fmt:message bundle="${loc}" key="locale.loginRegText" var="loginRegText"/>
    <fmt:message bundle="${loc}" key="locale.passwordRegText" var="passwordRegText"/>
    <fmt:message bundle="${loc}" key="locale.passMismatch" var="passMismatch"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="../js/angular.min.js"></script>
    <script src="../js/controller.js"></script>
    <script src="../js/validation.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">

<%@include file="header.jsp"%>

<div class="container-fluid">
    <div class="row">
        <section id="tm-section-4" class="tm-section">
            <div class="tm-container">
                <h2 class="blue-text text-center">${registration}</h2>
                <c:if test="${not empty userExists}">
                    <div class="row text-center error-text">${userExistsText}</div>
                </c:if>
                <c:if test="${not empty invalidRegistrationData}">
                    <div class="row text-center error-text">${invalidRegData}</div>
                </c:if>
                <div class="col-md-3 col-lg-3 col-xl-3"></div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <form name="registrationForm" action="controller" method="POST" class="tm-contact-form">
                        <input type="hidden" name="command" value="registration"/>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control reg-input" placeholder="${name}" min="2"  max="30" required onkeyup="validateRegistrationForm()"/>
                            <div class="reg-error-message">${nameRegText}</div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="login" class="form-control reg-input" placeholder="${loginText}"  min="5"  max="10" required onkeyup="validateRegistrationForm()"/>
                            <div class="reg-error-message">${loginRegText}</div>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control reg-input" placeholder="${password}"  min="6"  max="10" required onkeyup="validateRegistrationForm()"/>
                            <div class="reg-error-message">${passwordRegText}</div>
                        </div>
                        <div class="form-group">
                            <input type="password" name="passwordConfirm" class="form-control reg-input" placeholder="${repeatPassword}"  min="6"  max="10" required  onkeyup="validateRegistrationForm()"/>
                            <div class="reg-error-message">${passMismatch}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="submit" class="btn tm-green-btn pull-xs-right" id="submit_button_register">${registration}</button>
                            </div>
                            <div class="col-md-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="register-link-text">
                                    ${accountText} <a href="controller?command=login_page" class="register-link">${login}</a>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-3"></div>
            </div>
        </section>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>