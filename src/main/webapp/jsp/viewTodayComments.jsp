<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.noTodayComments" var="noTodayComments"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">

<%@include file="header.jsp"%>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 comments-section">
            <c:choose>
                <c:when test="${comments.size()==0}">
                    <div class="text-center">
                        ${noTodayComments}.
                    </div>
                </c:when>
            <c:otherwise>
                <c:forEach items="${comments}" var="commentItem">
                <article class="box comment-box">
                    <a href="controller?command=view_profile&userId=${commentItem.key.userId}" class="comment-user-link"> ${commentItem.value}</a>
                    <span class="user-comment-date"> ${commentItem.key.commentDate}</span>

                    <a href="controller?command=delete_comment&commentId=${commentItem.key.commentId}&bookId=${commentItem.key.bookId}&previousPage=${pageContext.request.requestURI}" class="delete-comment-cross">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>
                    <p>${commentItem.key.commentText}</p>
                </article>
            </c:forEach>
            </c:otherwise>
            </c:choose>
        </div>
        <div class="col-md-2"></div>
    </div>
    </div>
<%@include file="footer.jsp"%>
</body>
</html>
