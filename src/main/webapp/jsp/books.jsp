<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.fullName" var="fullName"/>
    <fmt:message bundle="${loc}" key="locale.birthYear" var="birthYear"/>
    <fmt:message bundle="${loc}" key="locale.birthCountry" var="birthCountry"/>
    <fmt:message bundle="${loc}" key="locale.more" var="more"/>
    <fmt:message bundle="${loc}" key="locale.edit" var="edit"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.loginText" var="loginText"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.publishingYear" var="pubYear"/>
    <fmt:message bundle="${loc}" key="locale.authors" var="authorsTitle"/>
    <fmt:message bundle="${loc}" key="locale.title" var="title"/>
    <fmt:message bundle="${loc}" key="locale.allBooksTitle" var="allBooksTitle"/>
    <fmt:message bundle="${loc}" key="locale.genres" var="genresTitle"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.delete" var="delete"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>

<div class="container-fluid book-list">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h3 class="blue-text text-center">${allBooksTitle}: </h3>
            <table class="table">
                <thead>
                <tr>
                    <th>${title}</th>
                    <th>${authorsTitle}</th>
                    <th>${pubYear}</th>
                    <th>${genresTitle}</th>
                    <th>${more}</th>
                    <th>${edit}</th>
                    <th>${delete}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${books}" var="book">
                    <tr>
                        <td>${book.title}</td>
                        <td>
                            <c:forEach items="${book.authors}" var="author" varStatus="authorLoop">
                                <a href="controller?command=view_author&authorId=${author.authorId}">${author.fullName}</a> ${!authorLoop.last ? ', ' : ''}
                            </c:forEach>
                        </td>
                        <td>${book.publishingYear}</td>
                        <td>
                            <c:forEach items="${book.genres}" var="genre" varStatus="genreLoop">
                                ${genre.genreName}</a> ${!genreLoop.last ? ', ' : ''}
                            </c:forEach>
                        </td>
                        <td><a href="controller?command=view_single&bookId=${book.bookId}" class="authors-statistics-link">${more}</a></td>
                        <td><a href="controller?command=edit_book_page&bookId=${book.bookId}" class="authors-statistics-link">${edit}</a></td>
                        <td><a href="controller?command=delete_entity&entity=book&bookId=${book.bookId}" class="authors-statistics-link">${delete}</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-2"></div>

        <div class="pagination">
            <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="1" action="controller?command=view_book_statistics"/>
        </div>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>