<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html lang="en">
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>

    <fmt:message bundle="${loc}" key="locale.userInfoTitle" var="userInfoTitle"/>
    <fmt:message bundle="${loc}" key="locale.name" var="name"/>
    <fmt:message bundle="${loc}" key="locale.age" var="age"/>
    <fmt:message bundle="${loc}" key="locale.banned" var="banned"/>
    <fmt:message bundle="${loc}" key="locale.active" var="active"/>
    <fmt:message bundle="${loc}" key="locale.commentsNumber" var="commentsNumber"/>
    <fmt:message bundle="${loc}" key="locale.marksNumber" var="marksNumber"/>
    <fmt:message bundle="${loc}" key="locale.status" var="status"/>
    <fmt:message bundle="${loc}" key="locale.ban" var="ban"/>
    <fmt:message bundle="${loc}" key="locale.unlock" var="unlock"/>
    <fmt:message bundle="${loc}" key="locale.loginText" var="loginText"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>

<div class="container-fluid book-list">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h3 class="blue-text text-center">${userInfoTitle}: </h3>
            <table class="table">
                <thead> <tr> <th>${loginText}</th> <th>${name}</th> <th>${age}</th> <th>${commentsNumber}</th>
                    <th>${marksNumber}</th> <th>${status}<th><th></th> </tr></thead>
                <tbody>
                <c:forEach items="${statisticsList}" var="statics">
                    <tr>
                        <th scope="row"><a href="controller?command=view_profile&userId=${statics.userId}">${statics.login}</a></th>
                        <td>${statics.name}</td> <td>${statics.age}</td>
                        <td>${statics.commentsAmount}</td>
                        <td>${statics.ratingsAmount}</td>
                        <td>
                            <c:choose>
                                <c:when test="${statics.role==banned}">
                                    <span class="status banned">${banned}</span></td>
                                    <td>
                                        <a href="controller?command=ban_user&userId=${statics.userId}&action=unlock" class="button alt">${unlock}</a>
                                    </td>
                                </c:when>
                                <c:when test="${statics.role==userRole}">
                                    <span class="status active">${active}</span></td>
                                    <td>
                                        <a href="controller?command=ban_user&userId=${statics.userId}&action=ban" class="button alt">${ban}</a>
                                    </td>
                                </c:when>
                            </c:choose>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="pagination">
        <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="1" action="controller?command=view_user_statistics"/>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>