<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tag.tld"%>
<html>
<head>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.addAuthor" var="addAuthor"/>
    <fmt:message bundle="${loc}" key="locale.addBook" var="addBook"/>
    <fmt:message bundle="${loc}" key="locale.viewTodayComments" var="todayComments"/>
    <fmt:message bundle="${loc}" key="locale.viewUserInfo" var="userInfo"/>
    <fmt:message bundle="${loc}" key="locale.backToProfile" var="backToProfile"/>
    <fmt:message bundle="${loc}" key="locale.noUserComments" var="noUserComments"/>
    <fmt:message bundle="${loc}" key="locale.more" var="more"/>
    <fmt:message bundle="${loc}" key="locale.comments" var="commentsTitle"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>
<div class="container-fluid book-list">
    <div class="row">
        <div class="col-md-3">
            <a href="controller?command=view_profile&userId=${userId}" class="back-to-profile-link">${backToProfile}</a>
        </div>
        <div class="col-md-6">
            <h2 class="blue-text text-center"> ${commentsTitle} : </h2>
        </div>
        <div class="col-md-3"></div>
    </div>

    <c:choose>
        <c:when test="${comments.size()!=0}">
            <c:forEach var="comment" items="${comments}">
                <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <article class="box comment-box">
                                <span class="user-comment-date">${comment.key.commentDate}</span>

                                <a href="controller?command=delete_comment&commentId=${comment.key.commentId}&bookId=${comment.value.bookId}&previousPage=${pageContext.request.requestURI}&&userId=${sessionScope.userId}" class="delete-comment-cross">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </a>
                                <p>${comment.key.commentText}</p>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <div class="tm-3-col-box gray-bg book-item">
                                <c:choose>
                                    <c:when test="${empty comment.value.imageUrl}">
                                        <img src="../img/book.png" alt="image" class="book-img">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${comment.value.imageUrl}" alt="Image" class="book-img">
                                    </c:otherwise>
                                </c:choose>
                                <div class="book-item-title-author">
                                    <c:forEach items="${comment.value.authors}" var="author" varStatus="authorLoop">
                                        <a href="#">${author.fullName}</a> ${!authorLoop.last ? ', ' : ''}
                                    </c:forEach> - ${comment.value.title}
                                </div>

                                <div class="book-item-btn-more">
                                    <a href="controller?command=view_single&bookId=${comment.key.bookId}"  class="btn btn-default tm-normal-btn tm-gray-btn add-author-btn">${more}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="text-center">
            ${noUserComments}
            </div>
        </c:otherwise>
    </c:choose>
    <div class="pagination">
        <pagination:nav totalPageAmount="${pagesAmount}" viewPageCount="1" action="controller?command=view_user_comments&userId=${userId}"/>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>
