<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale" var="loc"/>
    <fmt:message bundle="${loc}" key="locale.login" var="login"/>
    <fmt:message bundle="${loc}" key="locale.logout" var="logout"/>
    <fmt:message bundle="${loc}" key="locale.registration" var="registration"/>
    <fmt:message bundle="${loc}" key="locale.welcomeText" var="welcomeText"/>
    <fmt:message bundle="${loc}" key="locale.bookrating" var="bookrating"/>
    <fmt:message bundle="${loc}" key="locale.image" var="image"/>
    <fmt:message bundle="${loc}" key="locale.name" var="name"/>
    <fmt:message bundle="${loc}" key="locale.age" var="age"/>
    <fmt:message bundle="${loc}" key="locale.aboutMyself" var="aboutMyself"/>
    <fmt:message bundle="${loc}" key="locale.imgText" var="imgText"/>
    <fmt:message bundle="${loc}" key="locale.invalidUserData" var="invalidUserData"/>
    <fmt:message bundle="${loc}" key="locale.editProfileTitle" var="editProfileTitle"/>
    <fmt:message bundle="${loc}" key="locale.ageText" var="ageText"/>
    <fmt:message bundle="${loc}" key="locale.save" var="save"/>
    <fmt:message bundle="${loc}" key="locale.nameRegText" var="nameRegText"/>
    <fmt:message bundle="${loc}" key="locale.rights" var="footerText"/>
    <fmt:message bundle="${loc}" key="locale.search" var="search"/>
    <fmt:message bundle="${loc}" key="locale.aboutText" var="aboutText"/>
    <fmt:message bundle="${loc}" key="locale.viewAllAuthors" var="allAuthors"/>
    <fmt:message bundle="${loc}" key="locale.allBooks" var="allBooks"/>
    <fmt:message bundle="${loc}" key="locale.searchByTitle" var="searchByTitle"/>
    <title>${bookrating}</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="../js/validation.js"></script>
</head>
<body ng-app="validationApp" ng-controller="mainController">
<%@include file="header.jsp"%>
<div class="container-fluid">
    <div class="row gray-bg">
        <section id="tm-section-4" class="tm-section">
            <div class="tm-container">
                <h2 class="blue-text text-center">${editProfileTitle}:</h2>
                <c:if test="${not empty invalidData}">
                    <div class="row text-center error-text">${invalidUserData}</div>
                </c:if>
                <form name="editProfile" action="controller" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="userId" value="${user.userId}"/>
                    <input type="hidden" name="command" value="edit_profile"/>
                    <input type="hidden" name="oldImage" value="${user.avatar}"/>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="name">${name}: </label></div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="name" name="name" value="${user.name}" required min="2" max="30" onkeyup="validateEditProfileForm()"/>
                            <div class="reg-error-message">${nameRegText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="age">${age}: </label></div>
                        <div class="col-md-6">
                            <input type="number" class="form-control" id="age" name="age" value="${user.age}" required min="12" max="100" onkeyup="validateEditProfileForm()"/>
                            <div class="reg-error-message">${ageText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"> <label for="info">${aboutMyself}:</label></div>
                        <div class="col-md-6">
                                <textarea class="form-control description-area" rows="3" id="info" name="info" required maxlength="300" onkeyup="validateEditProfileForm()">
                                    ${user.info}</textarea>
                            <div class="reg-error-message">${aboutText}</div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2 single_view"><label for="newImage">${image}:</label></div>

                        <c:if test="${not empty user.avatar}">
                            <img src="${user.avatar}" alt="" class="user-image left-image"/>
                        </c:if>

                        <div class="col-md-6">
                            <input type="file" name="newImage" id="newImage" accept="image/*"/>
                            <p class="help-block">${imgText}</p>
                        </div>
                        <div class="col-md-3"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-primary button alt login-button edit-profile" value="${save}" id="submit-edit-profile"/></div>
                        <div class="col-md-5"></div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>