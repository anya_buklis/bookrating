
var validationApp = angular.module('validationApp', []);

validationApp.controller('mainController', function($scope) {
	$scope.minAuthorAge = 18;
	$scope.currentYear = new Date().getFullYear();
	$scope.maxBirthYear = $scope.currentYear - 18;
});