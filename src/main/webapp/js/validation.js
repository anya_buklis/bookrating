var tagRegexp = /^[^<>]*$/;
var lettersRegexp = /^[^0123456789!@#$%^&*()':}{>?<.,`~]*$/;

function validateForm(){
    var form = document.forms["registrationForm"];
    var name = form["name"];
    var login = form["login"];
    var password = form["password"];
    var passwordConfirm = form["passwordConfirm"];

    var validationCheck = new Array();
    validationCheck.push(validateUserName(name));
    validationCheck.push(validateWithRegExp(login, loginRegExp));
    validationCheck.push(validateWithRegExp(password, passwordRegEx));
    validationCheck.push(validatePasswordConfirmation(passwordConfirm, password));
    return !validationCheck.includes(false);
}

function validateAge(age){
    if (age.value<=100 && age.value>=12){
        makeValid(age);
        return true;
    } else {
        makeInvalid(age);
        return false;
    }
}

function makeValid(element){
    element.className = "form-control";
}

function makeInvalid(element){
    element.className = "form-control invalid";
}

function validateWithRegExp(element, regExp){
    if(element.value.match(regExp) == null){
        makeInvalid(element);
        return false;
    } else{
        makeValid(element);
        return true;
    }
}

function validatePasswordConfirmation(passwordConfirm, password){
    if(passwordConfirm.value != password.value){
        makeInvalid(passwordConfirm);
        return false;
    } else {
        makeValid(passwordConfirm);
        return true;
    }
}

/*======= Functions for validating author's form =====*/

function validateEditAuthorForm(){
    var form = document.forms["editAuthorForm"];
    var name = form["fullName"];
    var birthYear = form["birthYear"];
    var biography = form["biography"];
    var birthCountry = form["birthCountry"];

    var validationAuthorCheck = new Array();
    validationAuthorCheck.push(validateAuthorName(name));
    validationAuthorCheck.push(validateBiography(biography));
    validationAuthorCheck.push(validateBirthCountry(birthCountry));
    validationAuthorCheck.push(validateBirthYear(birthYear));
    if(validationAuthorCheck.includes(false)){
        document.getElementById('submit_edit_author').disabled = true;
    } else {
        document.getElementById('submit_edit_author').disabled = false;
    }
}

function validateAuthorName(name){
    var trimAuthor = name.value.trim();
    if (trimAuthor.length <= 100 && trimAuthor.length >=5
        && validateWithRegExp(name, tagRegexp)
        && validateWithRegExp(name, lettersRegexp)){
        makeValid(name);
        return true;
    } else {
        makeInvalid(name);
        return false;
    }
}

function validateBiography(biography){
    var trimBiography = biography.value.trim();
    if (trimBiography.length <= 1500 && trimBiography.length >= 60
        && validateWithRegExp(biography, tagRegexp)){
        makeValid(biography);
        return true;
    } else {
        makeInvalid(biography);
        return false;
    }
}

function validateBirthCountry(birthCountry){
    var trimCountry = birthCountry.value.trim();
    if (trimCountry.length <= 60 && trimCountry.length >= 3
        && validateWithRegExp(birthCountry, tagRegexp)
        && validateWithRegExp(birthCountry, lettersRegexp)){
        makeValid(birthCountry);
        return true;
    } else {
        makeInvalid(birthCountry);
        return false;
    }
}

function validateBirthYear(birthYear){
    var maxBirthYear = new Date().getFullYear() - 18;
    if (birthYear.value <= maxBirthYear && birthYear.value >= 1000){
        makeValid(birthYear);
        return true;
    } else {
        makeInvalid(birthYear);
        return false;
    }
}
/*============================================*/

var passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,10}$/;
var loginRegExp = /^[\w\-]{5,10}$/;

function validateRegistrationForm(){
    var registerForm = document.forms["registrationForm"];
    var registerUsername = registerForm["name"];
    var registerLogin = registerForm["login"];
    var registerPassword = registerForm["password"];
    var registerRepeatPassword = registerForm["passwordConfirm"];
    var registerValidationArray = new Array();
    registerValidationArray.push(validateUserName(registerUsername));
    registerValidationArray.push(validateLogin(registerLogin));
    registerValidationArray.push(validateWithRegExp(registerPassword, passwordRegEx));
    registerValidationArray.push(validatePasswordConfirmation(registerRepeatPassword, registerPassword));
    if(registerValidationArray.includes(false)){
        document.getElementById('submit_button_register').disabled = true;
    } else {
        document.getElementById('submit_button_register').disabled = false;
    }
}

function validateUserName(userName){
    if(userName.value.length <= 30 && userName.value.length >=2
        && validateWithRegExp(userName, lettersRegexp)){
        makeValid(userName);
        return true;
    } else {
        makeInvalid(userName);
        return false;
    }
}

function validateLogin(login){
    if(validateWithRegExp(login, loginRegExp)){
        makeValid(login);
        return true;
    } else {
        makeInvalid(login);
        return false;
    }
}


/*========================================*/

var currentYear = new Date().getFullYear();

function validateEditBookForm(){
    var form = document.forms["editBookForm"];
    var authors = document.getElementsByName("author");
    var title = form["title"];
    var description = form["description"];
    var publishYear = form["publishingYear"];
    var array = new Array();
    array.push(validateTitle(title));
    array.push(validateDesc(description));
    array.push(validatePublishYear(publishYear));
    array.push(validateAuthors(authors));
    if(array.includes(false)){
        document.getElementById('submit-edit-book').disabled = true;
    } else {
        document.getElementById('submit-edit-book').disabled = false;
    }
}

function validateDesc(description){
    var trimDesc = description.value.trim();
    if (trimDesc.length <= 1500 && trimDesc.length >= 30
        && validateWithRegExp(description, tagRegexp)){
        makeValid(description);
        return true;
    } else {
        makeInvalid(description);
        return false;
    }
}

function validateAuthors(authors){
    var array = new Array();
    for (var i = 0; i < authors.length; i++) {
        array.push(validateAuthor(authors[i]));
    }
    if(array.includes(false)){
        return false;
    } else{
        true;
    }
}

function validateAuthor(author){
    var trimAuthor = author.value.trim();
    if (trimAuthor.length <= 100 && trimAuthor.length >= 5
        && validateWithRegExp(author, tagRegexp)
         && validateWithRegExp(author, lettersRegexp)){
        makeValid(author);
        return true;
    } else {
        makeInvalid(author);
        return false;
    }
}

function validateTitle(title){
    var trimTitle = title.value.trim();
    if (trimTitle.length <= 200 && trimTitle.length >= 1
        && validateWithRegExp(title, tagRegexp)){
        makeValid(title);
        return true;
    } else {
        makeInvalid(title);
        return false;
    }
}

function validatePublishYear(publishYear){
    if (publishYear.value <= currentYear && publishYear.value >= 1400){
        makeValid(publishYear);
        return true;
    } else {
        makeInvalid(publishYear);
        return false;
    }
}

/*==========================================*/

function validateAddBookForm(){
    var form = document.forms["addBookForm"];
    var authors = document.getElementsByName("author");
    var title = form["title"];
    var description = form["description"];
    var publishYear = form["publishingYear"];
    var array = new Array();
    array.push(validateTitle(title));
    array.push(validateDesc(description));
    array.push(validatePublishYear(publishYear));
    array.push(validateAuthors(authors));
    if(array.includes(false)){
        document.getElementById('submit-add-book-form').disabled = true;
    } else {
        document.getElementById('submit-add-book-form').disabled = false;
    }
}


/*==========================================*/
function validateEditProfileForm(){
    var form = document.forms["editProfile"];
    var name = form["name"];
    var age = form["age"];
    var aboutMyself = form["info"];

    var validationProfileCheck = new Array();
    validationProfileCheck.push(validateUserName(name));
    validationProfileCheck.push(validateAge(age));
    validationProfileCheck.push(validateAboutMyself(aboutMyself));
    if(validationProfileCheck.includes(false)){
        document.getElementById('submit-edit-profile').disabled = true;
    } else {
        document.getElementById('submit-edit-profile').disabled = false;
    }
}

function validateAboutMyself(aboutMyself){
    var trimAbout = aboutMyself.value.trim();
    if (trimAbout.length <= 300 && validateWithRegExp(aboutMyself, tagRegexp)){
        makeValid(aboutMyself);
        return true;
    } else {
        makeInvalid(aboutMyself);
        return false;
    }
}


/*===============================================*/

function validateAddAuthorForm(){
    var form = document.forms["addAuthorForm"];
    var name = form["fullName"];
    var birthYear = form["birthYear"];
    var biography = form["biography"];
    var birthCountry = form["birthCountry"];

    var validationAuthorCheck = new Array();
    validationAuthorCheck.push(validateAuthorName(name));
    validationAuthorCheck.push(validateBiography(biography));
    validationAuthorCheck.push(validateBirthCountry(birthCountry));
    validationAuthorCheck.push(validateBirthYear(birthYear));
    if(validationAuthorCheck.includes(false)){
        document.getElementById('submit-add-author').disabled = true;
    } else {
        document.getElementById('submit-add-author').disabled = false;
    }
}