$(document).ready(function(){
    var counter = $('div[id^=author-field]').length;
    $(".addAuthor").click(function(){
        if (counter < 10){
            $(".addAuthor").show();
            $("#author-field").append("<div class='col-md-12 new-author' id='author-field'><input type='text' class='form-control new-author-input' id='author1' name='author' required min='5' max='100' onkeyup='validateAddBookForm()'/> <a href='javascript:void(0);' class='remAuthor'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a></div>");
            counter++;
        } else{
            $(".addAuthor").hide();
        }
    });
    $("#author-field").on('click','.remAuthor',function(){
        if (counter <= 10){
            $(".addAuthor").show();
            $(this).parent().remove();
            counter--;
        }
    });
});